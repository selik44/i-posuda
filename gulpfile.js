(function() {
    'use strict';
    var autoprefixer, babel, concat, cooked, cssnano, del, es, fs, gulp, gutil, iconfont, imagemin, livereload, path, pngquant, raw, realFavicon, rename, rigger, sass, slim, uglify;

    raw = 'catalog/view/theme/default/assets/';
    cooked = 'catalog/view/theme/default/build/';
    gulp = require('gulp');
    rigger = require('gulp-rigger');
    slim = require('gulp-slim');
    sass = require('gulp-sass');
    autoprefixer = require('gulp-autoprefixer');
    concat = require('gulp-concat');
    es = require('event-stream');
    gutil = require('gulp-util');
    uglify = require('gulp-uglify');
    cssnano = require('gulp-cssnano');
    babel = require('gulp-babel');
    livereload = require('gulp-livereload');
    del = require('del');
    iconfont = require('gulp-iconfont');
    imagemin = require('gulp-imagemin');
    pngquant = require('imagemin-pngquant');
    rename = require('gulp-rename');
    fs = require('fs');
    realFavicon = require('gulp-real-favicon');

    path = {
        src: {
            styles: [raw + 'stylesheets/@*.{scss,sass}', raw + 'stylesheets/**/@*.{scss,sass}'],
            js: [
                raw+'javascripts/@*.js'
            ],
            libs: [
                'node_modules/babel-polyfill/dist/polyfill.js',
                'bower_components/noty/lib/noty.min.js',
                'bower_components/selectize/dist/js/standalone/selectize.min.js'
            ],
            fonts: [raw + 'fonts/**/*.{eot,ttf,woff,woff2,svg}', '!' + raw + 'fonts/@*/*.svg'],
            iconfont: raw + 'fonts/@*/*.{eot,ttf,woff,woff2,svg}',
            img: [raw + 'img/**/*.{jpg,png,gif,svg}', '!' + raw + 'img/favicon.{jpg,png,gif,svg}'],
            images: [raw + 'images/**/*.{jpg,png,gif,svg}', '!' + raw + 'images/favicon.{jpg,png,gif,svg}'],
            favicon: raw + 'img/favicon.svg'
        },
        watch: {
            styles: raw + 'stylesheets/**/*.{sass,scss,css}',
            js: raw + 'javascripts/**/*.js',
            fonts: raw + 'fonts/**/*.{eot,ttf,woff,woff2,svg}',
            iconfont: raw + 'fonts/@*/*.{eot,ttf,woff,woff2,svg}',
            img: [raw + 'img/**/*.{jpg,png,gif,svg}', '!' + raw + 'img/favicon.{jpg,png,gif,svg}']
        },
        build: {
            styles: cooked,
            js: cooked,
            libs: cooked+'libs',
            fonts: cooked+'fonts',
            img: cooked+'img',
            images: cooked+'images',
            favicon: './'
        },
        clean: cooked
    };

    gulp.task('styles:build', function() {
        return gulp.src(path.src.styles).pipe(sass().on('error', gutil.log)).pipe(autoprefixer()).pipe(cssnano()).pipe(rename(function(path) {
            if (path.basename[0] === '@') {
                return path.basename = path.basename.slice(1);
            }
        })).pipe(gulp.dest(path.build.styles)).pipe(livereload());
    });

    gulp.task('js:build', function() {
        return gulp
            .src(path.src.js)
            .pipe(rigger())
            .pipe(babel({"presets": ["latest"]}))
            .pipe(uglify())
            .pipe(rename(function(path) {
                if (path.basename[0] === '@') {
                    return path.basename = path.basename.slice(1);
                }
            })).pipe(gulp.dest(path.build.js))
            .pipe(livereload());
    });

    gulp.task('js:libs', function() {
        return gulp
            .src(path.src.libs)
            .pipe(concat('libs.js'))
            .pipe(uglify())
            .pipe(gulp.dest(path.build.js))
            .pipe(livereload());
    });

    gulp.task('getIconfontName', function() {
        return gulp.src(path.src.iconfont).pipe(rename(function(file) {
            return path.iconfontname = file.dirname.slice(1);
        }));
    });

    gulp.task('iconfont:build', ['getIconfontName'], function() {
        return gulp.src(path.src.iconfont).pipe(iconfont({
            fontName: path.iconfontname,
            prependUnicode: true,
            normalize: true,
            fontHeight: 1001,
            formats: ['ttf', 'eot', 'woff', 'woff2', 'svg']
        })).on('glyphs', function(glyphs) {
            return console.log(glyphs);
        }).pipe(gulp.dest(path.build.fonts + '/' + path.iconfontname)).pipe(livereload());
    });

    gulp.task('fonts:build', function() {
        return gulp.src(path.src.fonts).pipe(gulp.dest(path.build.fonts)).pipe(livereload());
    });

    gulp.task('img:build', function() {
        return gulp.src(path.src.img).pipe(imagemin({
            progressive: true,
            svgoPlugins: [
                {
                    removeViewBox: false
                }
            ],
            use: [pngquant()]
        })).pipe(gulp.dest(path.build.img)).pipe(livereload());
    });

    gulp.task('images:build', function() {
        return gulp.src(path.src.images).pipe(imagemin({
            progressive: true,
            svgoPlugins: [
                {
                    removeViewBox: false
                }
            ],
            use: [pngquant()]
        })).pipe(gulp.dest(path.build.images)).pipe(livereload());
    });

    gulp.task('clean', function() {
        return del(path.clean);
    });

    gulp.task('lr:listen', function() {
        return livereload.listen();
    });

    gulp.task('watch', function() {
        gulp.watch(path.watch.styles, ['styles:build']);
        gulp.watch(path.watch.js, ['js:build']);
        gulp.watch(path.watch.js, ['js:libs']);
        gulp.watch(path.watch.fonts, ['fonts:build']);
        gulp.watch(path.watch.img, ['img:build']);
        return gulp.watch(path.watch.images, ['images:build']);
    });

    gulp.task('build', ['styles:build', 'js:build', 'js:libs', 'fonts:build', 'img:build', 'images:build']);

    gulp.task('refresh', ['clean', 'build']);

    gulp.task('default', ['lr:listen', 'build', 'watch']);

}).call(this);
