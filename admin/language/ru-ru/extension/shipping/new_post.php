<?php
// Heading
$_['heading_title']    = 'Новая почта';

// Text
$_['text_extension']   = 'Доставка';
$_['text_success']     = 'Модуль успешно обновлен!';
$_['text_edit']        = 'Редактирование модуля';

// Entry
$_['entry_geo_zone']   = 'Географическая зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Порядок сортировки:';

// Error
$_['error_permission'] = 'У вас нет прав для управления этим модулем!';
