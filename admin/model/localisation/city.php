<?php
class ModelLocalisationCity extends Model {
	public function addCity($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "city SET country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
		$city_id = $this->db->getLastId();

		foreach ($data['city_name'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "city_name SET city_id = '" . (int)$city_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}

		$this->cache->delete('city');
		
		return $this->db->getLastId();
	}

	public function editCity($city_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "city SET country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "' WHERE city_id = '" . (int)$city_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "city_name WHERE city_id = '" . (int)$city_id . "'");

		foreach ($data['city_name'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "city_name SET city_id = '" . (int)$city_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}
		$this->cache->delete('city');
	}

	public function deleteCity($city_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$city_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "city_name WHERE city_id = '" . (int)$city_id . "'");
		$this->cache->delete('city');
	}

	public function getCity($city_id) {
		$query = $this->db->query("SELECT c.*, cn.name AS name FROM " . DB_PREFIX . "city c LEFT JOIN " . DB_PREFIX . "city_name cn ON (c.city_id = cn.city_id) WHERE c.city_id = '" . (int)$city_id . "' AND cn.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		return $query->row;
	}

	public function getCities($data = array()) {
		if ($data) {
		$sql = "SELECT c.*, cn.name, co.name AS country, z.name AS zone FROM " . DB_PREFIX . "city c LEFT JOIN " . DB_PREFIX . "country co ON (c.country_id = co.country_id) LEFT JOIN " . DB_PREFIX . "zone z ON (c.zone_id = z.zone_id) LEFT JOIN " . DB_PREFIX . "city_name cn ON (c.city_id = cn.city_id) WHERE cn.language_id = '" . (int)$this->config->get('config_language_id') . "'";
			$sort_data = array(
				'cn.name',
				'co.name',
				'z.name',
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY city_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$city_data = $this->cache->get('city.admin');

			if (!$city_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city ORDER BY name ASC");

				$city_data = $query->rows;

				$this->cache->set('city.admin', $city_data);
			}

			return $city_data;
		}
	}

	public function getTotalCities() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "city");

		return $query->row['total'];
	}
	public function getCityNames($city_id) {
		$city_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city_name WHERE city_id = '" . (int)$city_id . "'");

		foreach ($query->rows as $result) {
			$city_data[$result['language_id']] = array('name' => $result['name']);
		}	
		return $city_data;
	}	
	public function getZonesByCountry($country_id) {
		$query = $this->db->query("SELECT name, zone_id FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "'");
		return $query->rows;
	}
}
