<?php
class ModelCatalogExport extends Model {
	public function getDownload($file) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "errors WHERE file_id = '".$file."'");
		return $query->rows;
	}
	public function getDownloadArt() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "art_error");
		return $query->rows;
	}
	public function getMeasure($measure_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "measure WHERE id = '" . (int)$measure_id . "'");
		return $query->row['measure_name'];
	}
	public function getDownloadProducts($startRow,$datefrom,$dateto ) {
	if ($datefrom) {
	$query = $this->db->query("SELECT DISTINCT p.product_id, p.measure_id, pd.name, pd.description_prom, pd.description_second, p.sku, p.model, p.minimum, p.manufacturer_id, p.price, p.days_to_stock, p.stock_status_id, m.PromUA_name  FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id>".$startRow." and DATE(p.date_modified) BETWEEN '".$datefrom."' AND '".$dateto."' ORDER BY p.`product_id` DESC LIMIT 20000");
	} else {
	$query = $this->db->query("SELECT DISTINCT p.product_id, p.measure_id, pd.name, pd.description_prom, pd.description_second, p.sku, p.model, p.minimum, p.manufacturer_id, p.price, p.days_to_stock, p.stock_status_id, m.PromUA_name  FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id>".$startRow." ORDER BY p.`product_id` DESC LIMIT 20000 ");}
		return $query->rows;
	}
	public function addFile($file) {
	$search = $this->db->query("SELECT id  FROM " . DB_PREFIX . "files where `filename` = '". $this->db->escape($file['name']) ."'");
if ($search->num_rows >0) {
$this->db->query("UPDATE " . DB_PREFIX . "files SET filename = '" . $this->db->escape($file['name']) . "', cols = '" . $this->db->escape($file['string']) . "' WHERE `filename` = '". $this->db->escape($file['name']) ."'");
//$this->db->query("DELETE FROM " . DB_PREFIX . "errors WHERE `file_id` = '".$search->row['id']."'");
return $search->row['id'];
} else {
$this->db->query("INSERT INTO " . DB_PREFIX . "files SET filename = '" . $this->db->escape($file['name']) . "', cols = '" . $this->db->escape($file['string']) . "'");
return $this->db->getLastId();
}
}
	public function deleteFile($file) {
	$search = $this->db->query("SELECT id  FROM " . DB_PREFIX . "files where `filename` = '". $file ."'");
if ($search->num_rows >0) {
$this->db->query("DELETE FROM " . DB_PREFIX . "files WHERE filename = '".$file."'");
//$this->db->query("DELETE FROM " . DB_PREFIX . "errors WHERE `file_id` = '".$search->row['id']."'");
}
}
	public function updateFile($file) {
$this->db->query("UPDATE " . DB_PREFIX . "files SET rows = '" . (int)$file['rows'] . "', `insert` = '" . (int)$file['insert'] . "', `update` = '" . (int)$file['update'] . "', `errors` = '" . (int)$file['errors'] . "' WHERE `filename` = '". $this->db->escape($file['name']) ."'");
}
	public function validateProduct($data) {
	$search = $this->db->query("SELECT sku  FROM " . DB_PREFIX . "product where `model` = '". $this->db->escape($data['model']) ."'");
if ($search->num_rows >0) {
$data['update']++;} else {
$data['insert']++;}
	return array($data['insert'],$data['update']);
}
	public function selectFile($file) {
	$search = $this->db->query("SELECT cols, id  FROM " . DB_PREFIX . "files where `filename` = '". $this->db->escape($file['name']) ."'");
if ($search->num_rows >0) {
return $search->row;
}
}
public function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
	'і' => 'i',   'ї' => 'i',   'є' => 'ye',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
	'І' => 'I',   'Ї' => 'I',   'Є' => 'Ye',
    );
    return strtr($string, $converter);
}
public function str2url($str) {
    // переводим в транслит
    $str = $this->model_catalog_export->rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    return $str;
}

	public function searchMan($data, $searchCol) {
	$search = $this->db->query("SELECT manufacturer_id, db_brand  FROM " . DB_PREFIX . "manufacturer where `". $searchCol ."` = '". $this->db->escape($data['manufacturer']) ."'  LIMIT 1");
if ($search->num_rows >0) {
	return array($search->row['manufacturer_id'],$search->row['db_brand']);}
	}
	public function updateMan($data) {
	$this->db->query("UPDATE  " . DB_PREFIX . "manufacturer SET `col_1`='".$this->db->escape($data['col_1'])."',`PromUA_name`='".$this->db->escape($data['prom_name'])."',`db_brand`='".$this->db->escape($data['db_brand'])."',`col_4`='".$this->db->escape($data['col_4'])."',`col_5`='".$this->db->escape($data['col_5'])."',`col_6`='".$this->db->escape($data['col_6'])."',`brand`='".$this->db->escape($data['offic'])."' WHERE name='".$data['manufacturer']."'");
}
	public function addMan($data) {
	$this->db->query("INSERT INTO  " . DB_PREFIX . "manufacturer (`name`, `col_1`,`PromUA_name`,`db_brand`,`col_4`,`col_5`,`col_6`,`brand`) VALUES('".$this->db->escape($data['manufacturer'])."','".$this->db->escape($data['col_1'])."','".$this->db->escape($data['prom_name'])."','".$this->db->escape($data['db_brand'])."','".$this->db->escape($data['col_4'])."','".$this->db->escape($data['col_5'])."','".$this->db->escape($data['col_6'])."','".$this->db->escape($data['offic'])."')");

	$manufacturer_id = $this->db->getLastId();

	$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_description SET manufacturer_id = '" . (int)$manufacturer_id . "', language_id = '" . (int)$data['language_id'] . "', name = '" . $this->db->escape($data['manufacturer']) . "', description = '" . $this->db->escape($data['desc']) . "', meta_title = '" . $this->db->escape($data['manufacturer']) . "', meta_h1 = '" . $this->db->escape($data['manufacturer']) . "', meta_description = '" . $this->db->escape($data['manufacturer']) . "', meta_keyword = '" . $this->db->escape($data['manufacturer']) . "'");
	
	$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '".(int)$data['store']."'");
$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manufacturer_id=" . $manufacturer_id . "', keyword = '" . $data['url'] . "'");
	return $manufacturer_id;
	}

private function setDefaultCurrency($price = 0, $currency = 0) {
		if($price > 0) {
			$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "currency WHERE currency_id = '" .(int)$currency. "'  LIMIT 1");
			return $final_price = ($query->num_rows)? ((float)$price / $query->row['value']):0;
		}
		return 0;
}
	public function addProduct($data) {
	$search = $this->db->query("select product_id from " . DB_PREFIX . "product where `model` = '". $data['model'] ."'  LIMIT 1");
	$search_id = $search->num_rows;

        $data['price'] = (!empty($data['price'])) ? ceil($data['price']) : ""; // ceil price
        $data['price_1'] = (!empty($data['price']) && !empty($data['quant_u'])) ?  ceil($data['price']/$data['quant_u']) : "";
        $data['price'] = !empty($data['price_1']) ? ceil($data['price_1']*$data['quant_u']) : "";
        $data['price_u'] = $data['price'];
        $data['price_y'] = !empty($data['price_1']) ? ceil($data['price_1']*$data['quant_y']) : "";

        if ($search_id==0)
		{
		$this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', price = '" . (float)$data['price_1'] . "', status = '0', date_added = NOW(), date_modified = NOW()");
		$product_id = $this->db->getLastId();
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$data['language_id'] . "', name = '" . $this->db->escape($data['name']) . "', meta_title = '" . $this->db->escape($data['name']) . "', meta_h1 = '" . $this->db->escape($data['name'])  . "', meta_description = '" . $this->db->escape($data['name'])  . "', meta_keyword = '" . $this->db->escape($data['name']). "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$data['store_id'] . "'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . $product_id . "', keyword = '" . $data['url'] . "'");
		if ($data['price']) {
$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '1', required = '1'");
		$product_u_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', see = '" . (int)$data['see_1'] . "', price = '" . (float)$data['price'] . "', status = '" . (int)$data['status_1'] . "', quant = '1', option_value_id = '32', quantity = 1000000");
		}
		if ($data['price_u']) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', option_value_id = '31', price = '" . (float)$data['price_u'] . "', quant = '". (int)$data['quant_u'] ."', see = '" . (int)$data['see_u'] . "', status = '" . (int)$data['status_u'] . "', price_prefix = '. $prefix .', quantity = 1000000");
		}
		if ($data['price_y']) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', quant = '". (int)$data['quant_y'] ."', option_value_id = '43', price = '" . (float)$data['price_y'] . "', see = '" . (int)$data['see_y'] . "', status = '" . (int)$data['status_y'] . "', price_prefix = '. $prefix .', quantity = 1000000");
		}
		$data['insert']++;
		}
		else {
		$product_id = $search->row['product_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . (float)$data['price_1'] . "' WHERE product_id = '" . (int)$product_id . "'");
		//$this->db->query("UPDATE " . DB_PREFIX . "product_description SET name = '" . $this->db->escape($data['name']) . "', meta_title = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description'])."' WHERE product_id = '" . (int)$product_id . "' AND  language_id = '" . (int)$data['language_id'] . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` LIKE '%product_id=".$product_id."%'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . $product_id . "', keyword = '" . $data['url'] . "'");
		if ($data['price']) {
$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '1', required = '1'");
		$product_u_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', see = '" . (int)$data['see_1'] . "', price = '" . (float)$data['price_1'] . "', status = '" . (int)$data['status_1'] . "', quant = '1', option_value_id = '32', quantity = 1000000");
		}
		if ($data['price_u']) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', option_value_id = '31', price = '" . (float)$data['price_u'] . "', quant = '". (int)$data['quant_u'] ."', see = '" . (int)$data['see_u'] . "', status = '" . (int)$data['status_u'] . "', quantity = 1000000");
		}
		if ($data['price_y']) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', quant = '". (int)$data['quant_y'] ."', option_value_id = '43', price = '" . (float)$data['price_y'] . "', see = '" . (int)$data['see_y'] . "', status = '" . (int)$data['status_y'] . "', quantity = 1000000");
		}

		$data['update']++;

		}
	return array($data['insert'],$data['update']);
	}
	public function add2Product($data) {
        $data['price_1'] = ceil($data['price_1']); // ceil price
        $data['price'] = ceil($data['price']); // ceil price
        $data['price_u'] = $data['price'] * (int)$data['quant_u']; // ceil price
        $data['price_y'] = $data['price'] * (int)$data['quant_y']; // ceil price

	$search = $this->db->query("select product_id, status_update from " . DB_PREFIX . "product where `sku` = '". $this->db->escape($data['sku']) ."'  LIMIT 1");
	$search_id = $search->num_rows;
		if ($search_id==0)
		{
		$this->db->query("INSERT INTO " . DB_PREFIX . "product SET sku = '". $this->db->escape($data['sku']) ."', model = '" . $this->db->escape($data['model']) . "', price = '" . (float)$data['price_1'] . "', status = '0', date_added = NOW(), date_modified = NOW()");
		$product_id = $this->db->getLastId();
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$data['language_id'] . "', name = '" . $this->db->escape($data['name']) . "', meta_title = '" . $this->db->escape($data['name']) . "', meta_h1 = '" . $this->db->escape($data['name'])  . "', meta_description = '" . $this->db->escape($data['name'])  . "', meta_keyword = '" . $this->db->escape($data['name']). "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$data['store_id'] . "'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . $product_id . "', keyword = '" . $data['url'] . "'");
$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '1', required = '1'");
		$product_u_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', price = '".(float)$data['price']."', see = '" . (int)$data['see_1'] . "', status = '" . (int)$data['status_1'] . "', quant = '1', option_value_id = '32', quantity = 1000000");
		if ($data['price_u']) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', option_value_id = '31', price = '" . (float)$data['price_u'] . "', quant = '". (int)$data['quant_u'] ."', see = '" . (int)$data['see_u'] . "', status = '" . (int)$data['status_u'] . "', quantity = 1000000");
		}
		if ($data['price_y']) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', quant = '". (int)$data['quant_y'] ."', option_value_id = '43', price = '" . (float)$data['price_y'] . "', see = '" . (int)$data['see_y'] . "', status = '" . (int)$data['status_y'] . "', quantity = 1000000");
		}
		$data['insert']++;
		}
		else {
		$product_id = $search->row['product_id'];
		$status_update = $search->row['status_update'];
$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` LIKE '%product_id=".$product_id."%'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . $product_id . "', keyword = '" . $data['url'] . "'");
		$select_query = $this->db->query("SELECT product_option_id FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "' AND option_id = '1'");

		if ($select_query->num_rows) {
		$product_u_id = $select_query->row['product_option_id'];

if ($status_update==0) {
		$this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . (float)$data['price_1'] . "' WHERE product_id = '" . (int)$product_id . "'");

		if ($data['price']) {

		$value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "' AND option_value_id = '32' AND product_option_id = '" . (int)$product_u_id . "'");
		if ($value_query->num_rows) {
		$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET price = '" . (float)$data['price'] . "', quantity = 1000000 WHERE product_option_id = '" . (int)$product_u_id . "' AND option_value_id = '32' AND product_id = '" . (int)$product_id . "'");
		} else {
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', price = '".(float)$data['price']."', see = '" . (int)$data['see_1'] . "', status = '" . (int)$data['status_1'] . "', quant = '1', option_value_id = '32', quantity = 1000000");
}
		}

		$value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "' AND option_value_id = '31' AND product_option_id = '" . (int)$product_u_id . "'");
		if ($value_query->num_rows) {

		$price_u = $data['price']*$value_query->row['quant'];
		$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET price = '" . (float)$price_u . "', quantity = 1000000 WHERE product_option_id = '" . (int)$product_u_id . "' AND option_value_id = '31' AND product_id = '" . (int)$product_id . "'");
		} else 	if ($data['price_u'])  {

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', option_value_id = '31', price = '" . (float)$data['price_u'] . "', quant = '". (int)$data['quant_u'] ."', see = '" . (int)$data['see_u'] . "', status = '" . (int)$data['status_u'] . "', quantity = 1000000");
		}

		$value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "' AND option_value_id = '43' AND product_option_id = '" . (int)$product_u_id . "'");

		if ($value_query->num_rows) {

		$price_y = $data['price']*$value_query->row['quant'];
		$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET price = '" . (float)$data['price_y'] . "', quantity = 1000000 WHERE product_option_id = '" . (int)$product_u_id . "' AND option_value_id = '43' AND product_id = '" . (int)$product_id . "'");
		} else 	if ($data['price_y'])  {

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_u_id . "', product_id = '" . (int)$product_id . "', option_id = '1', quant = '". (int)$data['quant_y'] ."', option_value_id = '43', price = '" . (float)$data['price_y'] . "', see = '" . (int)$data['see_y'] . "', status = '" . (int)$data['status_y'] . "', quantity = 1000000");
		}
} elseif ($status_update==1) {
		if ($data['price']) {

		$value_query_u = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "' AND option_value_id = '31' AND product_option_id = '" . (int)$product_u_id . "'");
		$value_query_y = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "' AND option_value_id = '43' AND product_option_id = '" . (int)$product_u_id . "'");
		if ($value_query_u->num_rows) {
		$price_1 = $data['price']/$value_query_u->row['quant'];

$price_1 = ceil($price_1); // ceil price

		$this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . (float)$price_1 . "' WHERE product_id = '" . (int)$product_id . "'");

$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET price = '" . (float)$price_1 . "', quantity = 1000000 WHERE product_option_id = '" . (int)$product_u_id . "' AND option_value_id = '32' AND product_id = '" . (int)$product_id . "'");
$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET price = '" . (float)$data['price'] . "', quantity = 1000000 WHERE product_option_id = '" . (int)$product_u_id . "' AND option_value_id = '31' AND product_id = '" . (int)$product_id . "'");

		if ($value_query_y->num_rows) {
		$price_y = $price_1*($value_query_y->row['quant']);
$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET price = '" . (float)$price_y . "', quantity = 1000000 WHERE product_option_id = '" . (int)$product_u_id . "' AND option_value_id = '43' AND product_id = '" . (int)$product_id . "'");
}
}
}
}
}
		$data['update']++;

		}
	return array($data['insert'],$data['update']);
	}


	public function updateProduct($data) {
	$search = $this->db->query("select product_id from " . DB_PREFIX . "product where `product_id` = '". $data['product_id'] ."'  LIMIT 1");
if ($search->row['product_id']) {
$product_id = $search->row['product_id'];
	if (!empty($data['currency'])) {
		$curr = $this->db->query("SELECT currency_id from " . DB_PREFIX . "currency WHERE `code` = '".$data['currency']."' LIMIT 1");
		if ($curr->num_rows) {
		$measure_id = $this->db->query("SELECT id from " . DB_PREFIX . "measure WHERE `measure_name` = '".$data['measure']."' LIMIT 1");
			$currency_id = $curr->row['currency_id'];
$this->db->query("UPDATE " . DB_PREFIX . "product SET product_id = '".(int)$data['product_id']."', model = '" .$this->db->escape($data['model']) . "',stock_status_id = '".(int)$data['stock_status']."',days_to_stock = '".(int)$data['days_to_stock']."', sku = '" . $this->db->escape($data['sku']) . "', minimum = '" . (int)$data['minimum'] . "', quantity = '" . (int)$data['quantity'] . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', price = '" . $this->setDefaultCurrency($data['price'], $currency_id) . "', status = '" . (int)$data['status'] . "', currency_id = '" . (int)$currency_id . "', base_price = '" . (float)$data['price'] . "', measure_id = '" . (int)$measure_id->row['id'] . "' WHERE product_id = '" . (int)$product_id . "'");
$this->db->query("UPDATE " . DB_PREFIX . "product_description SET name = '" . $this->db->escape($data['name']) . "', name_prom = '" . $this->db->escape($data['name_prom']) . "', meta_title = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description'])."', description_prom = '" . $this->db->escape($data['description_prom']) ."', description_second = '" . $this->db->escape($data['description_second']) ."' WHERE product_id = '" . (int)$product_id . "' AND  language_id = '" . (int)$data['language_id'] . "'");
$data['insert']++;
}
}
}
return $data['insert'];
}
	public function maxProduct() {
	$query = $this->db->query("SELECT product_id FROM  " . DB_PREFIX . "product WHERE image IS NULL ORDER BY `product_id` DESC LIMIT 1");
	return $query->row['product_id'];
	}
	public function maxProduct2($datefrom, $dateto) {
	if ($datefrom) {
	$query = $this->db->query("SELECT product_id FROM  " . DB_PREFIX . "product WHERE DATE(`date_modified`) BETWEEN  '".$datefrom."' AND '".$dateto."' ORDER BY `product_id` DESC LIMIT 1");
	} else {
	$query = $this->db->query("SELECT product_id FROM  " . DB_PREFIX . "product ORDER BY `product_id` DESC LIMIT 1");
	}
	return $query->row['product_id'];
	}
	public function getProductforimage($last) {
	$query = $this->db->query("SELECT sku, product_id FROM  " . DB_PREFIX . "product WHERE  image IS NULL AND product_id > '".$last."'  ORDER BY `product_id` LIMIT 700");
	return $query->rows;
	}

public function addImage($image, $product_id, $id) {
if ($id==0)
$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '".$image."' WHERE product_id = '".$product_id."'");
$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int)$id . "'");
	}

	public function addError($data, $error) {
$this->db->query("INSERT INTO " . DB_PREFIX . "errors SET file_id = '".(int)$data['file_id']."', col_num = '".(int)$data['int']."', product_id = '".$data['product_id']."', model = '" . $this->db->escape($data['model'])  . "', sku = '" . $this->db->escape($data['sku']) . "',manufacturer = '" . $this->db->escape($data['manufacturer']) . "', name = '".$this->db->escape($data['name'])."', currency = '".$this->db->escape($data['currency'])."', error ='".$this->db->escape($error)."'");

	}

	public function search_attr($attribute) {
$query = $this->db->query("SELECT `attribute_sub_id` FROM " . DB_PREFIX . "attribute_sub WHERE `description` = '".$attribute."' LIMIT 1" );
if ($query->num_rows >0) {
return $query->row['attribute_sub_id'];
}
	}

	public function add_product_attr($data) {
$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id='" . (int)$data['product_id'] . "', language_id = '1', attribute_id = '5552', text= '" . $data['korpus'] . "'");
$query = $this->db->query("SELECT mfilter_value_id FROM " . DB_PREFIX . "mfilter_values WHERE value= '" . md5($data['korpus'])."'");
if ($query->num_rows >0) {
$mfilter_id = $query->row['mfilter_value_id'];
} else {
$this->db->query("INSERT INTO " . DB_PREFIX . "mfilter_values SET type = 'attribute', value_id = '5552', value= '" . md5($data['korpus']) . "'");
$mfilter_id = $this->db->getLastId();
}

$this->db->query("UPDATE " . DB_PREFIX . "product SET mfilter_values = '".$mfilter_id."' WHERE product_id = '".(int)$data['product_id']."'");
$this->db->query("UPDATE " . DB_PREFIX . "product_description SET name = '".$this->db->escape($data['name'])."', meta_title = '".$this->db->escape($data['name'])."', description = '".$this->db->escape($data['description'])."' WHERE product_id = '".(int)$data['product_id']."'");
	}
	public function add_attr($data) {
$this->db->query("INSERT INTO " . DB_PREFIX . "attribute_sub SET attribute_id = '5552', description = '".$this->db->escape($data["attribute"]) . "'");
	}

	public function add_attr2($data, $attr_id) {
$this->db->query("INSERT INTO " . DB_PREFIX . "attribute_sub SET attribute_id = '5552', description = '".$this->db->escape($data["attribute2"]) . "', korn_id = '" . (int)$attr_id. "'");
		}

public function get_names($last) {
	$search = $this->db->query("SELECT DISTINCT p.product_id, pd.name_prom, p.sku, p.manufacturer_id, m.db_brand  FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id>".$last." AND p.mfilter_values=''");
	return $search->rows;
	}
public function get_file($file) {
	$query = $this->db->query("SELECT rows, `insert`, `update`, `errors`,id FROM " . DB_PREFIX . "files WHERE filename = '".$file."'");
if ($query->num_rows >0) {
	return $query->row;
}
}
public function get_artikules() {
$artikules_data = array();
	$query = $this->db->query("SELECT attribute_id, name FROM " . DB_PREFIX . "attribute_description");
foreach ($query->rows as $result) {
$attribute = $result['attribute_id'];
			$artikules_data[$attribute] = $result['name'];
		}
	return $artikules_data;	
}

 public function art_error($analyze2,$analyze) {
$this->db->query("INSERT INTO  " . DB_PREFIX . "art_error SET find='".$this->db->escape($analyze)."', nofind='".$this->db->escape($analyze2)."'");
}


	public function clean() {
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_description");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_image");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_to_store");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_to_category");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_discount");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_attribute");
$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` LIKE '%product_id=%'");
$this->db->query("DELETE FROM " . DB_PREFIX . "mfilter_values WHERE `type` = 'attribute'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value");

	}
	public function cleanman() {
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "manufacturer");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "manufacturer_description");
$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "manufacturer_to_store");
$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` LIKE '%manufacturer_id=%'");
}

}
