<?php
class ModelCatalogCategory extends Model {
	public function addCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$category_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
		}

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$level = 0;

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

		foreach ($query->rows as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

			$level++;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

		if (isset($data['category_filter'])) {
			foreach ($data['category_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		if (isset($data['category_store'])) {
			foreach ($data['category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		// Set which layout to use with this category
		if (isset($data['category_layout'])) {
			foreach ($data['category_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('category');

		return $category_id;
	}

	public function editCategory($category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

		if ($query->rows) {
			foreach ($query->rows as $category_path) {
				// Delete the path below the current one
				$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

				$path = array();

				// Get the nodes new parents
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Get whats left of the nodes current path
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Combine the paths with a new level
				$level = 0;

				foreach ($path as $path_id) {
					$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

					$level++;
				}
			}
		} else {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_id . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_filter'])) {
			foreach ($data['category_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_store'])) {
			foreach ($data['category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_layout'])) {
			foreach ($data['category_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('category');
	}

	public function deleteCategory($category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE path_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$this->deleteCategory($result['category_id']);
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category WHERE category_id = '" . (int)$category_id . "'");

		$this->cache->delete('category');
	}

	public function repairCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_id . "'");

		foreach ($query->rows as $category) {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category['category_id'] . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$parent_id . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$category['category_id'] . "', level = '" . (int)$level . "'");

			$this->repairCategories($category['category_id']);
		}
	}

	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getCategoriesByParentId($parent_id = 0) {
		$query = $this->db->query("SELECT *, (SELECT COUNT(parent_id) FROM " . DB_PREFIX . "category WHERE parent_id = c.category_id) AS children FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name");

		return $query->rows;
	}

	public function getCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order, c1.status,(select count(product_id) as product_count from " . DB_PREFIX . "product_to_category pc where pc.category_id = c1.category_id) as product_count FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'product_count',
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCategoryDescriptions($category_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_h1'          => $result['meta_h1'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}
	
	public function getCategoryPath($category_id) {
		$query = $this->db->query("SELECT category_id, path_id, level FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

		return $query->rows;
	}
	
	public function getCategoryFilters($category_id) {
		$category_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_filter_data[] = $result['filter_id'];
		}

		return $category_filter_data;
	}

	public function getCategoryStores($category_id) {
		$category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}

		return $category_store_data;
	}

	public function getCategoryLayouts($category_id) {
		$category_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $category_layout_data;
	}

	public function getTotalCategories() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category");

		return $query->row['total'];
	}

	public function getAllCategories() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  ORDER BY c.parent_id, c.sort_order, cd.name");

		$category_data = array();
		foreach ($query->rows as $row) {
			$category_data[$row['parent_id']][$row['category_id']] = $row;
		}

		return $category_data;
	}
	
	public function getTotalCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

    // for make table oc_mfilter_url_alias
    public function modelMakeMfilterUrlAlias()
    {
        $query_category_url = $this->db->query("SELECT query, keyword FROM " . DB_PREFIX . "url_alias WHERE query LIKE 'category_id%'");
        $category_name_url = [];
        for ($i = 0; $i < $query_category_url->num_rows; $i++) {
            $id = explode("=", $query_category_url->rows[$i]["query"]);
            $category_name_url[$id[1]] = $query_category_url->rows[$i]["keyword"];
        }

        $query_category_name = $this->db->query("SELECT category_id, name FROM " . DB_PREFIX . "category_description ORDER BY category_id ");
        $category_name = [];
        for ($i = 0; $i < $query_category_name->num_rows; $i++) {
            $category_name[$query_category_name->rows[$i]["category_id"] . "_"] = $query_category_name->rows[$i]["name"];
            $category_name[$query_category_name->rows[$i]["category_id"]] = $category_name_url[$query_category_name->rows[$i]["category_id"]];
        }

        $query_category = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path cp ORDER BY category_id");
        $all_category = $query_category->rows;
        $number_categories = count($all_category);
        $all_category_name = $all_category;
        for ($i = 0; $i < $number_categories; $i++) {
            $all_category_name[$i]["category_id"] = $all_category[$i]["category_id"];
            $all_category_name[$i]["category"] = $category_name[$all_category[$i]["category_id"]];
            $all_category_name[$i]["category_ru"] = $category_name[$all_category[$i]["category_id"] . "_"];
            $all_category_name[$i]["path_id"] = $all_category[$i]["path_id"];
            $all_category_name[$i]["path"] = $category_name[$all_category[$i]["path_id"]];
            $all_category_name[$i]["path_ru"] = $category_name[$all_category[$i]["path_id"] . "_"];
        }
        $number_all_categories = count($all_category_name);
        $all_category_name_2 = $all_category_name;
        $all_category_name_3 = $all_category_name;
        $all_category_name_4 = $all_category_name;

        $cities = $this->db->query("SELECT * FROM " . DB_PREFIX . "cities WHERE status = 1");
        $manufacturer = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer ORDER BY manufacturer_id");

        $this->db->query("DELETE FROM  " . DB_PREFIX . "mfilter_url_alias  WHERE description=''");
        $mfilter_url_alias_old = $this->db->query("SELECT path FROM " . DB_PREFIX . "mfilter_url_alias ORDER BY path");

        for ($i = 0; $i <= $cities->num_rows; $i++) { // города
            $city = isset($cities->rows[$i]["city_url"]) ? $cities->rows[$i]["city_url"] . "/" : "";
            $city_name = isset($cities->rows[$i]["city_name"]) ? $cities->rows[$i]["city_name"] : "Киев";
            switch ($city_name) {
                case "Одесса":
                    $city_name = "в Одессе";
                    break;
                case "Харьков":
                    $city_name = "в Харькове";
                    break;
                case "Днепр":
                    $city_name = "в Днепре";
                    break;
                case "Киев":
                    $city_name = "в Киеве";
                    break;
                case 'Опт':
                    $city_name = 'оптом';
                    break;
            }
            for ($j = 0; $j < $manufacturer->num_rows; $j++) { // производители
                $mfp = "manufacturers[" . mb_strtolower($manufacturer->rows[$j]["name"]) . "]";
                $alias = mb_strtolower($manufacturer->rows[$j]["name"]);
                for ($c = 0; $c < $number_all_categories; $c++) { // категории


                    if (($all_category_name[$c]["category_id"] == $all_category_name[$c]["path_id"]) && ($all_category_name[$c]["level"] == 0)) {
                        $path = $city . $all_category_name[$c]["category"];
                        foreach ($mfilter_url_alias_old as $url) {  // есть уже такой url ?
                            $is_url = ($url == $path) ? true : false;
                        }
                        if (!$is_url) { // если в базе нет такого урла, то записывает в базу
                            //  {Название категории} {Название бренда} {Город} (Киев где нет городов): купить в интернет магазине iPosuda по выгодным ценам!
                            $meta_title = $all_category_name[$c]["category_ru"] . ' ' . $manufacturer->rows[$j]["name"] . ' ' . $city_name . ': купить в интернет магазине iPosuda по выгодным ценам!';

                            //❶В НАЛИЧИИ БОКАЛЫ ДЛЯ ПИВА LUMINARC В ОДЕССЕ ➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55
                            $meta_description = '❶В НАЛИЧИИ ' . mb_strtoupper($all_category_name[$c]["category_ru"]) . ' ' . mb_strtoupper($manufacturer->rows[$j]["name"]) . ' ' . $city_name . '➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55';
                            $description = '';

                            // H1: {Название категории} {Название бренда} {Город} (Киев где нет городов)
                            $h1 = $all_category_name[$c]["category_ru"] . " " . $manufacturer->rows[$j]["name"] . ' ' . $city_name;
                            $this->addToDatabase ($path, $mfp, $alias, $meta_title, $meta_description, $description, $h1 );
                        }
                    } elseif (($all_category_name[$c]["category_id"] == $all_category_name[$c]["path_id"]) && ($all_category_name[$c]["level"] == 1)) {
                        for ($cc = 0; $cc < $number_all_categories; $cc++) {
                            if($all_category_name_2[$cc]["category_id"] == $all_category_name[$c]["category_id"] && $all_category_name_2[$cc]["level"] == 0)

                                $path = $city . $all_category_name_2[$cc]["path"]."/".$all_category_name[$c]["category"];

                        }
                        foreach ($mfilter_url_alias_old as $url) {  // есть уже такой url ?
                            $is_url = ($url == $path) ? true : false;
                        }
                        if (!$is_url) { // если в базе нет такого урла, то записывает в базу
                            //  {Название категории} {Название бренда} {Город} (Киев где нет городов): купить в интернет магазине iPosuda по выгодным ценам!
                            $meta_title = $all_category_name[$c]["category_ru"] . ' ' . $manufacturer->rows[$j]["name"] . ' ' . $city_name . ': купить в интернет магазине iPosuda по выгодным ценам!';

                            //❶В НАЛИЧИИ БОКАЛЫ ДЛЯ ПИВА LUMINARC В ОДЕССЕ ➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55
                            $meta_description = '❶В НАЛИЧИИ ' . mb_strtoupper($all_category_name[$c]["category_ru"]) . ' ' . mb_strtoupper($manufacturer->rows[$j]["name"]) . ' ' . $city_name . '➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55';
                            $description = '';

                            // H1: {Название категории} {Название бренда} {Город} (Киев где нет городов)
                            $h1 = $all_category_name[$c]["category_ru"] . " " . $manufacturer->rows[$j]["name"] . ' ' . $city_name;
                            $this->addToDatabase ($path, $mfp, $alias, $meta_title, $meta_description, $description, $h1 );
                        }
                    }
                    elseif (($all_category_name[$c]["category_id"] == $all_category_name[$c]["path_id"]) && ($all_category_name[$c]["level"] == 2)) {
                        for ($cc = 0; $cc < $number_all_categories; $cc++) {
                            if($all_category_name_2[$cc]["category_id"] == $all_category_name[$c]["category_id"] && $all_category_name_2[$cc]["level"] == 1) {
                                $middle_path = $all_category_name_2[$cc]["path"];
                                for ($ccc = 0; $ccc < $number_all_categories; $ccc++) {
                                    if($all_category_name_3[$ccc]["category_id"] == $all_category_name[$c]["category_id"] && $all_category_name_3[$ccc]["level"] == 0)

                                        $path = $city . $all_category_name_3[$ccc]["path"]."/". $middle_path ."/".$all_category_name[$c]["category"];
                                }
                            }

                        }
                        foreach ($mfilter_url_alias_old as $url) {  // есть уже такой url ?
                            $is_url = ($url == $path) ? true : false;
                        }
                        if (!$is_url) { // если в базе нет такого урла, то записывает в базу
                            //  {Название категории} {Название бренда} {Город} (Киев где нет городов): купить в интернет магазине iPosuda по выгодным ценам!
                            $meta_title = $all_category_name[$c]["category_ru"] . ' ' . $manufacturer->rows[$j]["name"] . ' ' . $city_name . ': купить в интернет магазине iPosuda по выгодным ценам!';

                            //❶В НАЛИЧИИ БОКАЛЫ ДЛЯ ПИВА LUMINARC В ОДЕССЕ ➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55
                            $meta_description = '❶В НАЛИЧИИ ' . mb_strtoupper($all_category_name[$c]["category_ru"]) . ' ' . mb_strtoupper($manufacturer->rows[$j]["name"]) . ' ' . $city_name . '➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55';
                            $description = '';

                            // H1: {Название категории} {Название бренда} {Город} (Киев где нет городов)
                            $h1 = $all_category_name[$c]["category_ru"] . " " . $manufacturer->rows[$j]["name"] . ' ' . $city_name;
                            $this->addToDatabase ($path, $mfp, $alias, $meta_title, $meta_description, $description, $h1 );
                        }
                    }
                    elseif (($all_category_name[$c]["category_id"] == $all_category_name[$c]["path_id"]) && ($all_category_name[$c]["level"] == 3)) {
                        for ($cc = 0; $cc < $number_all_categories; $cc++) {
                            if($all_category_name_2[$cc]["category_id"] == $all_category_name[$c]["category_id"] && $all_category_name_2[$cc]["level"] == 2) {
                                $middle_path = $all_category_name_2[$cc]["path"];
                                for ($ccc = 0; $ccc < $number_all_categories; $ccc++) {
                                    if($all_category_name_3[$ccc]["category_id"] == $all_category_name[$c]["category_id"] && $all_category_name_3[$ccc]["level"] == 1)
                                        $middle_path_2 = $all_category_name_3[$ccc]["path"];
                                    for ($cccc = 0; $cccc < $number_all_categories; $cccc++) {
                                        if($all_category_name_4[$cccc]["category_id"] == $all_category_name[$c]["category_id"] && $all_category_name_4[$cccc]["level"] == 0)

                                            $path = $city . $all_category_name_4[$cccc]["path"]."/". $middle_path_2 ."/". $middle_path ."/".$all_category_name[$c]["category"];
                                    }
                                }
                            }
                        }
                        foreach ($mfilter_url_alias_old as $url) {  // есть уже такой url ?
                            $is_url = ($url == $path) ? true : false;
                        }
                        if (!$is_url) { // если в базе нет такого урла, то записывает в базу
                            //  {Название категории} {Название бренда} {Город} (Киев где нет городов): купить в интернет магазине iPosuda по выгодным ценам!
                            $meta_title = $all_category_name[$c]["category_ru"] . ' ' . $manufacturer->rows[$j]["name"] . ' ' . $city_name . ': купить в интернет магазине iPosuda по выгодным ценам!';

                            //❶В НАЛИЧИИ БОКАЛЫ ДЛЯ ПИВА LUMINARC В ОДЕССЕ ➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55
                            $meta_description = '❶В НАЛИЧИИ ' . mb_strtoupper($all_category_name[$c]["category_ru"]) . ' ' . mb_strtoupper($manufacturer->rows[$j]["name"]) . ' ' . $city_name . '➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55';
                            $description = '';

                            // H1: {Название категории} {Название бренда} {Город} (Киев где нет городов)
                            $h1 = $all_category_name[$c]["category_ru"] . " " . $manufacturer->rows[$j]["name"] . ' ' . $city_name;
                            $this->addToDatabase ($path, $mfp, $alias, $meta_title, $meta_description, $description, $h1 );
                        }
                    }

                }
            }
        }
    }

    private function addToDatabase ($path, $mfp, $alias, $meta_title, $meta_description, $description, $h1 ) {
        $this->db->query("
										INSERT INTO `" . DB_PREFIX . "mfilter_url_alias` SET
											`path` = '" . $path . "',
											`mfp` = '" . $mfp . "',
											`alias` = '" . $alias . "',
											`language_id` = '1',
											`store_id` = '0',
											`meta_title` = '" . $meta_title . "',
											`meta_description` = '" . $meta_description . "',
											`meta_keyword` = '',
											`description` = '" . $description . "',
											`h1` = '" . $h1 . "'
									");
    }
}
