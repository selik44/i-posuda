<?php
class ModelToolRedirect extends Model {
	
	public function addRedirect($data)
	{
		$this->db->query( "INSERT INTO " . DB_PREFIX . "url_redirect "
        . "SET `url_from` = '" .$this->db->escape($data['url_from']). "', "
        . "`url_to` = '" .$this->db->escape($data['url_to']). "', "
        . "`url_code` = '" . (int)$data['url_code'] . "'");
		
		$this->cache->delete('customRedirects');
		
		return $this->db->getLastId();
	}
	
	public function editRedirect($redirect_id, $data)
	{
		$this->db->query("UPDATE " . DB_PREFIX . "url_redirect "
			. "SET `url_from` = '" .$this->db->escape($data['url_from']). "', "
			. "`url_to` = '" .$this->db->escape($data['url_to']). "', "
			. "`url_code` = '" . (int)$data['url_code'] . "' "
			. "WHERE `redirect_id` = '" . (int)$redirect_id . "'");
			
		$this->cache->delete('customRedirects');
	}
	
	public function deleteRedirect($redirect_id)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_redirect "
			. "WHERE `redirect_id` = '" . $redirect_id . "'");
			
		$this->cache->delete('customRedirects');
	}
	
	public function getRedirect($redirect_id)
	{
		$query = $this->db->query("SELEcT * FROM " . DB_PREFIX . "url_redirect "
			. "WHERE `redirect_id` = '" . $redirect_id . "'");
			
		return $query->row;
	}
	
	public function getRedirects($data)
	{
		
		$sql = "SELECT * FROM " . DB_PREFIX . "url_redirect ";

		$implode = Array();

		if($data['filter_from']):
			$implode[] = " `url_from` LIKE '%" .$this->db->escape($data['filter_from']). "%' ";
		endif;

		if($data['filter_to']):
			$implode[] = " `url_to` LIKE '%" .$this->db->escape($data['filter_to']). "%' ";
		endif;

		if($data['filter_code']):
			$implode[] = " `url_code` = '" . (int)$data['filter_code'] . "' ";
		endif;

		if(!empty($implode)):
			$sql .= ' WHERE '.implode(' AND ',$implode);
		endif;
		
		$sql .= ' ORDER BY redirect_id DESC ';
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 50;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTotalRedirects($data)
	{
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "url_redirect ";

		$implode = Array();

		if($data['filter_from']):
			$implode[] = " `url_from` LIKE '%" .$this->db->escape($data['filter_from']). "%' ";
		endif;

		if($data['filter_to']):
			$implode[] = " `url_to` LIKE '%" .$this->db->escape($data['filter_to']). "%' ";
		endif;

		if($data['filter_code']):
			$implode[] = " `url_code` = '" . (int)$data['filter_code'] . "' ";
		endif;

		if(!empty($implode)):
			$sql .= ' WHERE '.implode(' AND ',$implode);
		endif;

		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
}