<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
<div class="pull-right">
		<button id="enable_cities" type="button" data-toggle="tooltip" class="btn btn-default">Включить</button>
		<button id="disable_cities" type="button" data-toggle="tooltip" class="btn btn-default">Отключить</button>
		
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-city').submit() : false;"><i class="fa fa-trash-o"></i></button>
</div>
    <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
        <form style="margin-bottom: 10px;" action="<?php echo $add; ?>" method="post" enctype="multipart/form-data" id="form-city-add">
<input type="text" name="city_name" class="form-control" style="width: 180px; margin-right: 10px; display: inline-block;" placeholder="Название города *">
<input type="text" name="city_url" class="form-control" style="width: 180px; margin-right: 10px; display: inline-block;" placeholder="url *">
<button type="button" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary" onclick="$('#form-city-add').submit();"><i class="fa fa-plus"></i></button>
	</form>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-city">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left">
<?php echo $column_name; ?></td>
                  <td class="text-left">URL</td>
				  <td style="width: 100px;" class="text-left">Статус</td>
                </tr>
              </thead>
              <tbody>
                <?php if ($cities) { ?>
                <?php foreach ($cities as $city) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($city['id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $city['id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $city['id']; ?>" />
                    <?php } ?></td>
                    <td class="text-left"><?php echo $city['city_name']; ?></td>
					<td class="text-left"><?php echo $city['city_url']; ?></td>
				   <td class="text-left">
						<?php if($city['status']): ?>
							<b>Активен</b>
						<?php else:?>
							<b>НЕ активен</b>
						<?php endif; ?>
				   </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
		  <input type="hidden" id="cities_action" name="cities_action" value="delete">
	  </form>


  </div>
</div>      
<script>
$(document).on('click','#enable_cities', function(){
	var cities_form = $('form#form-city');
	$(cities_form).find('input#cities_action').val('enable');
	$(cities_form).trigger('submit');
});

$(document).on('click','#disable_cities', function(){
	var cities_form = $('form#form-city');
	$(cities_form).find('input#cities_action').val('disable');
	$(cities_form).trigger('submit');
});
</script>
<?php echo $footer; ?>
