<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-backup" data-toggle="tooltip" title="<?php echo $button_export; ?>" class="btn btn-default"><i class="fa fa-download"></i></button>
        <button type="submit" form="form-restore" data-toggle="tooltip" title="<?php echo $button_import; ?>" class="btn btn-default"><i class="fa fa-upload"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-exchange"></i> <?php echo $heading_title; ?></h3>
		</div>
		<div class="panel-body">
			<div class='well'>
				<div class='row'>
					<div class='col-sm-5'>
						<div class="form-group">
							<label class="control-label">URL откуда</label>
							<input class="form-control" name='filter_from' placeholder="URL откуда" value="<?php echo $filter_from; ?>">
						</div>
					</div>
					<div class='col-sm-5'>
						<div class="form-group">
							<label class="control-label">URL куда</label>
							<input class="form-control" name='filter_to' placeholder="URL куда" value="<?php echo $filter_to; ?>">
						</div>
					</div>
					<div class='col-sm-2'>
						<div class="form-group">
							<label class="control-label">Код</label>
							<select name="filter_code" class="form-control">
								<option selected value="">Код</option>
								<option <?php echo ($filter_code == 301) ? 'selected' : '' ?> value="301">301</option>
								<option <?php echo ($filter_code == 302) ? 'selected' : '' ?> value="302">302</option>
							</select>
						</div>
						<button id="filter-redirects" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> Фильтр</button>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<td style="width: 35%;" class="text-left">Откуда</td>
							<td style="width: 35%;"class="text-left">Куда</td>
							<td style="width: 10%;" class="text-left">Код</td>
							<td style="width: 20%;" ></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="3"></td>
							<td class='text-right'><button id="add-redirect" data-toggle="tooltip" data-original-title="Добавить" class="btn btn-success"><i class="fa fa-plus"></i></button></td>
						</tr>
						<?php if($redirects): ?>
							<?php foreach($redirects as $redirect): ?>
								<tr id="<?php echo 'redirect-'.$redirect['redirect_id']; ?>">
									<td>
										<input class="form-control" name="url_from" value="<?php echo $redirect['url_from']; ?>" placeholder="Откуда">
									</td>
									<td>
										<input class="form-control" name="url_to" value="<?php echo $redirect['url_to']; ?>" placeholder="Куда">
									</td>
									<td>
										<select name="url_code" class="form-control">
											<option selected value="301">301</option>
											<option <?php echo ($redirect['url_code'] == 302) ? 'selected' : '' ?> value="302">302</option>
										</select>
									</td>
									<td class="text-right">
										<button id="save-redirect" data-toggle="tooltip" data-original-title="Сохранить" class="btn btn-primary"><i class="fa fa-save"></i></button>
										<button id="delete-redirect" data-toggle="tooltip" data-original-title="Удалить" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
										<input type="hidden" name="redirect_id" value="<?php echo $redirect['redirect_id']; ?>">
										<div class="message"></div>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php else: ?>
							<tr>
								<td colspan='4'>Нет добавленных редиректов</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
				</div>
			<div class="row">
				<div class="col-sm-12 text-left"><?php echo $pagination; ?></div>
			</div>
		</div>
	 </div> 
  </div>
</div>
<script>
//фильтрация редиректов
$(document).on('click','#filter-redirects',function(){
	
	var url = 'index.php?route=tool/redirect&token=<?php echo $token; ?>';
	
	var filter_from = $('input[name="filter_from"]').val();
	
	if (filter_from) {
		url += '&filter_from=' + encodeURIComponent(filter_from);
	}
	
	var filter_to = $('input[name="filter_to"]').val();
	
	if (filter_to) {
		url += '&filter_to=' + encodeURIComponent(filter_to);
	}
	
	var filter_code = $('input[name="filter_code"]').val();
	
	if (filter_code) {
		url += '&filter_code=' + encodeURIComponent(filter_code);
	}
	
	location = url;
});

//удаление редиректа
$(document).on('click','#delete-redirect',function(){
	var this_tr = $(this).closest('tr');
	
	var redirect_id = $(this_tr).find('input[name="redirect_id"]').val();
	if(redirect_id == 'new')
	{
		$(this_tr).fadeOut();
	}
	else
	{
		if(confirm('Уверенны, что хотите удалить?'))
		{
			$.ajax({
				url: 'index.php?route=tool/redirect/remove&token=<?php echo $token; ?>',
				type: 'post',
				dataType: 'html',
				data: {
					redirect_id : redirect_id
				},
				success: function(data)
				{
					$(this_tr).fadeOut();
				}
			});
		}
	}
});

//сохранение данных редиректа
$(document).on('click','#save-redirect',function(){
	var this_tr = $(this).closest('tr');
	
	var redirect_id = $(this_tr).find('input[name="redirect_id"]').val();
	
	var url_from = $.trim($(this_tr).find('input[name="url_from"]').val());
	var url_to = $.trim($(this_tr).find('input[name="url_to"]').val());
	var url_code = $(this_tr).find('select[name="url_code"]').val();
	
	var has_error = false;
	if(url_from.length == 0)
	{
		$(this_tr).find('input[name="url_from"]').css('border-color','red');
		has_error = true;
	}
	
	if(url_to.length == 0)
	{
		$(this_tr).find('input[name="url_to"]').css('border-color','red');
		has_error = true;
	}
	
	if(!has_error)
	{
		$.ajax({
			url: 'index.php?route=tool/redirect/edit&token=<?php echo $token; ?>',
			type: 'post',
			dataType: 'json',
			data: {
				redirect_id : redirect_id,
				url_from: url_from,
				url_to: url_to,
				url_code: url_code
			},
			beforeSend: function()
			{
				$(this_tr).find('input').css('border-color','#ccc');
				$(this_tr).find('.message').text('');
			},
			success: function(data)
			{
				
				if(data.redirect_id)
				{
					$(this_tr).attr('id','redirect-'+data.redirect_id);
					$(this_tr).find('input[name="redirect_id"]').val(data.redirect_id);
				}
			
				$(this_tr).find('.message').text(data.message);
			}
		});
	}
	//alert(redirect_id);
});

//добавление формы
$(document).on('click','#add-redirect',function(){
	var this_tr = $(this).closest('tr');
	
	var html = '';
	html += '<tr>';
	html += '	<td>';
	html += '		<input class="form-control" name="url_from" value="" placeholder="Откуда">';
	html += '	</td>';
	
	html += '	<td>';
	html += '		<input class="form-control" name="url_to" value="" placeholder="Куда">';
	html += '	</td>';
	
	html += '	<td>';
	html += '		<select name="url_code" class="form-control">';
	html += '			<option selected value="301">301</option>';
	html += '			<option value="302">302</option>';
	html += '		</select>';
	html += '	</td>';
	
	html += '	<td class="text-right">';
	html += '		<button id="save-redirect" data-toggle="tooltip" data-original-title="Сохранить" class="btn btn-success"><i class="fa fa-save"></i></button>';
	html += '		<button id="delete-redirect" data-toggle="tooltip" data-original-title="Удалить" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
	html += '		<input type="hidden" name="redirect_id" value="new">';
	html += '		<div class="message">Обязательно сохранить!</div>';
	html += '	</td>';

	html += '</tr>';
	
	$(this_tr).after(html);
});
</script>
<?php echo $footer; ?>