<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
		  <div class="pull-right">
			<button type="submit" form="form-backup" data-toggle="tooltip" title="<?php echo $button_export; ?>" class="btn btn-default"><i class="fa fa-download"></i></button>
			<button type="submit" form="form-restore" data-toggle="tooltip" title="<?php echo $button_import; ?>" class="btn btn-default"><i class="fa fa-upload"></i></button>
		  </div>
		  <h1><?php echo $heading_title; ?></h1>
		  <ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		  </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-exchange"></i> <?php echo $heading_title; ?></h3>
			</div>
			<div class="panel-body">

				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td style="width: 35%;" class="text-left">URL</td>
								<td style="width: 45%;"class="text-left">Код</td>
								<td style="width: 20%;" ></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2"></td>
								<td class='text-right'><button id="add-custom-code" data-toggle="tooltip" data-original-title="Добавить" class="btn btn-success"><i class="fa fa-plus"></i></button></td>
							</tr>
							<?php if($custom_codes): ?>
								<?php foreach($custom_codes as $custom_code): ?>
									<tr id="<?php echo 'custom-code-'.$custom_code['custom_code_id']; ?>">
										<td>
											<input class="form-control" name="site_url" value="<?php echo $custom_code['site_url']; ?>" placeholder="Полная ссылка страницы">
										</td>
										<td>
											<textarea class="form-control" name="custom_code" placeholder="Произвольный код"><?php echo $custom_code['custom_code']; ?></textarea>
										</td>
										<td class="text-right">
											<button id="save-custom-code" data-toggle="tooltip" data-original-title="Сохранить" class="btn btn-success"><i class="fa fa-save"></i></button>
											<button id="delete-custom-code" data-toggle="tooltip" data-original-title="Удалить" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
											<input type="hidden" name="custom_code_id" value="<?php echo $custom_code['custom_code_id']; ?>">
											<div class="message"></div>
										</td>
								<?php endforeach; ?>
							<?php else: ?>
								<tr>
									<td colspan='3'>Нет добавленных произвольных кодов</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).on('click','#save-custom-code',function(){
	var this_tr = $(this).closest('tr');
	
	var custom_code_id = $(this_tr).find('input[name="custom_code_id"]').val();
	var site_url = $.trim($(this_tr).find('input[name="site_url"]').val());
	var custom_code = $.trim($(this_tr).find('textarea[name="custom_code"]').val());
	
	var has_error = false;
	
	if(site_url.length == 0)
	{
		$(this_tr).find('input[name="site_url"]').css('border-color','red');
		has_error = true;
	}
	
	if(custom_code.length == 0)
	{
		$(this_tr).find('textarea[name="custom_code"]').css('border-color','red');
		has_error = true;
	}
	
	if(!has_error)
	{
		$.ajax({
			url: 'index.php?route=tool/custom_code/edit&token=<?php echo $token; ?>',
			type: 'post',
			dataType: 'json',
			data: {
				custom_code_id : custom_code_id,
				site_url: site_url,
				custom_code: custom_code
			},
			beforeSend: function()
			{
				$(this_tr).find('input,textarea').css('border-color','#ccc');
				$(this_tr).find('.message').text('');
			},
			success: function(data)
			{
				if(data.custom_code_id)
				{
					$(this_tr).attr('id','custom-code-'+data.custom_code_id);
					$(this_tr).find('input[name="custom_code_id"]').val(data.custom_code_id);
				}
			
				$(this_tr).find('.message').text(data.message);
			}
		});
	}
});
//удаление
$(document).on('click','#delete-custom-code',function(){
	var this_tr = $(this).closest('tr');
		
	var custom_code_id = $(this_tr).find('input[name="custom_code_id"]').val();
	if(custom_code_id == 'new')
	{
		$(this_tr).fadeOut();
	}
	else
	{
		if(confirm('Уверенны, что хотите удалить?'))
		{
			$.ajax({
				url: 'index.php?route=tool/custom_code/remove&token=<?php echo $token; ?>',
				type: 'post',
				dataType: 'html',
				data: {
					custom_code_id : custom_code_id
				},
				success: function(data)
				{
					alert(data)
					$(this_tr).fadeOut();
				}
			});
		}
	}
});

//добавление формы
$(document).on('click','#add-custom-code',function(){
	var this_tr = $(this).closest('tr');
	
	var html = '';
	html += '<tr>';
	html += '	<td>';
	html += '		<input class="form-control" name="site_url" value="" placeholder="Полная ссылка страницы">';
	html += '	</td>';
	
	html += '	<td>';
	html += '		<textarea class="form-control" name="custom_code" placeholder="Произвольный код"></textarea>';
	html += '	</td>';

	html += '	<td class="text-right">';
	html += '		<button id="save-custom-code" data-toggle="tooltip" data-original-title="Сохранить" class="btn btn-success"><i class="fa fa-save"></i></button>';
	html += '		<button id="delete-custom-code" data-toggle="tooltip" data-original-title="Удалить" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
	html += '		<input type="hidden" name="custom_code_id" value="new">';
	html += '		<div class="message">Обязательно сохранить!</div>';
	html += '	</td>';

	html += '</tr>';
	
	$(this_tr).after(html);
});
</script>
<?php echo $footer; ?>