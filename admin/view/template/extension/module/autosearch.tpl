<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
		<span style="padding-right:20px;">
		<a href="https://opencartforum.com/index.php?app=core&module=search&do=user_activity&search_app=downloads&mid=688391" target="_blank" data-toggle="tooltip" title="Другие дополнения" class="btn btn-info"><i class="fa fa-download"></i> Другие дополнения</a></span>
        <button type="submit" form="form-latest" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1>AutoSearch 2x 1.10</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-latest" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-4">
              <select name="autosearch_status" id="input-status" class="form-control">
                <?php if ($autosearch_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
			</div>
			</div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-limit"><?php echo $entry_limit; ?></label>
            <div class="col-sm-4">
              <input type="text" name="autosearch_limit" value="<?php echo (isset($autosearch_limit) ? $autosearch_limit: '5') ; ?>" id="input-limit" class="form-control" />
            </div>
            <label class="col-sm-2 control-label" for="input-symbol"><?php echo $entry_symbol; ?></label>
            <div class="col-sm-4">
              <input type="text" name="autosearch_symbol" value="<?php echo (isset($autosearch_symbol) ? $autosearch_symbol: '3') ; ?>" id="input-symbol" class="form-control" />
            </div>
          </div>

          <fieldset>
          <legend><?php echo $text_search; ?></legend>
          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-model"><?php echo $entry_model; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_model" id="input-model" class="form-control">
                <?php if ($autosearch_model) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-tag"><?php echo $entry_tag; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_tag" id="input-tag" class="form-control">
                <?php if ($autosearch_tag) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
		  
          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-sku"><?php echo $entry_sku; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_sku" id="input-sku" class="form-control">
                <?php if ($autosearch_sku) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-upc"><?php echo $entry_upc; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_upc" id="input-upc" class="form-control">
                <?php if ($autosearch_upc) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-ean"><?php echo $entry_ean; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_ean" id="input-ean" class="form-control">
                <?php if ($autosearch_ean) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-jan"><?php echo $entry_jan; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_jan" id="input-jan" class="form-control">
                <?php if ($autosearch_jan) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-isbn"><?php echo $entry_isbn; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_isbn" id="input-isbn" class="form-control">
                <?php if ($autosearch_isbn) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-mpn"><?php echo $entry_mpn; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_mpn" id="input-mpn" class="form-control">
                <?php if ($autosearch_mpn) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-location"><?php echo $entry_location; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_location" id="input-location" class="form-control">
                <?php if ($autosearch_location) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-attr"><?php echo $entry_attr; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_attr" id="input-attr" class="form-control">
                <?php if ($autosearch_attr) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          </fieldset>

          <fieldset>
          <legend><?php echo $text_show; ?></legend>
          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-show"><?php echo $entry_show; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_show" id="input-show" class="form-control">
                <?php if ($autosearch_show) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
            <label class="col-sm-2 control-label" for="input-asr_image"><?php echo $entry_asr_image; ?></label>
            <div class="col-sm-4">
              <input type="text" name="autosearch_asr_image" value="<?php echo (isset($autosearch_asr_image) ? $autosearch_asr_image: '45') ; ?>" id="input-asr_image" class="form-control" />
            </div>
          </div>
          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-show_model"><?php echo $entry_show_model; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_show_model" id="input-show_model" class="form-control">
                <?php if ($autosearch_show_model) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-show_quantity"><?php echo $entry_show_quantity; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_show_quantity" id="input-show_quantity" class="form-control">
                <?php if ($autosearch_show_quantity) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-show_price"><?php echo $entry_show_price; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_show_price" id="input-show_price" class="form-control">
                <?php if ($autosearch_show_price) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-viewall"><?php echo $entry_viewall; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_viewall" id="input-viewall" class="form-control">
                <?php if ($autosearch_viewall != 'no') { ?>
                <option value="<?php echo $text_viewall; ?>" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="no"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="<?php echo $text_viewall; ?>"><?php echo $text_enabled; ?></option>
                <option value="no" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-sort"><?php echo $entry_sort; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_sort" id="input-sort" class="form-control">
                <?php if ($autosearch_sort) { ?>
                <option value="1" selected="selected"><?php echo $text_sort_name; ?></option>
                <option value="0"><?php echo $text_sort_date; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_sort_name; ?></option>
                <option value="0" selected="selected"><?php echo $text_sort_date; ?></option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label" for="input-codepage"><?php echo $entry_codepage; ?></label>
			<div class="col-sm-4">
              <select name="autosearch_codepage" id="input-codepage" class="form-control">
                <?php if ($autosearch_codepage == 0) { ?>
                 <option value="0" selected="selected"><?php echo $text_code_variant1; ?></option>
                 <option value="1"><?php echo $text_code_variant2; ?></option>
				 <option value="2"><?php echo $text_code_variant3; ?></option>
                <?php } else if ($autosearch_codepage == 1) { ?>
                 <option value="0"><?php echo $text_code_variant1; ?></option>
                 <option value="1" selected="selected"><?php echo $text_code_variant2; ?></option>
				 <option value="2"><?php echo $text_code_variant3; ?></option>
                <?php } else if ($autosearch_codepage == 2) { ?>
                 <option value="0"><?php echo $text_code_variant1; ?></option>
                 <option value="1"><?php echo $text_code_variant2; ?></option>
				 <option value="2" selected="selected"><?php echo $text_code_variant3; ?></option>
				<?php } ?>
              </select>
            </div>
          </div>
          </fieldset>

        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>