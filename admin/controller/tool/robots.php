<?php
class ControllerToolRobots extends Controller {
	private $file_name = 'robots.txt';
	
	public function index() {
		
		$data = Array();
		
		$this->document->setTitle('Robots');
		
		$data['heading_title'] = 'Robots';
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Robots',
			'href' => $this->url->link('tool/robots', 'token=' . $this->session->data['token'], true)
		);
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$data['token'] = $this->session->data['token'];
		
		$data['file_content'] = '';
		$data['textarea_rows'] = 25;
		
		$file_name = $_SERVER['DOCUMENT_ROOT'].'/'.$this->file_name;

		if(file_exists($file_name)):
			$data['file_content'] = trim(file_get_contents($file_name));
		
			 // рассмотрим файл как массив
			$file_arr = file($file_name); 
	 
			// подсчитываем количество строк в массиве 
			$data['textarea_rows'] = count($file_arr); 
		endif;

		$this->response->setOutput($this->load->view('tool/robots', $data));
	}
	
	public function saveFile()
	{

		$json = Array();
		
		$json['error'] = '';
		$json['success'] = '';
		
		if(isset($this->request->post['file_content'])):
			$file_content = trim($this->request->post['file_content']);
			
			//открываем файл для записи
			$file_name = $_SERVER['DOCUMENT_ROOT'].'/'.$this->file_name;
			
			$handle = fopen($file_name, "w");
			if($handle):
				if(fwrite($handle, $file_content)):
					$json['success'] = 'robots.txt успешно сохранен';
				else:
					$json['error'] = 'Ошибка записи файла - '.$this->file_name;
				endif;
				
				fclose($handle);
			else:
				$json['error'] = 'Ошибка открытия файла - '.$this->file_name;
			endif;
		endif;
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
		return;
	}
}	
?>