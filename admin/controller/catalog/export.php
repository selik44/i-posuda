<?php
class ControllerCatalogExport extends Controller {
  private $error = array();

  public function index() {
    $this->language->load('catalog/export');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('catalog/export');

    $this->getList();
  }

  public function products() {
    $url = "";
    $this->language->load('catalog/export');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('catalog/export');

    $data['heading_title'] = $this->language->get('heading_title');

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL')
    );
    $data['token'] = $this->session->data['token'];
    $data['info'] = $this->uploadFile();
    $data['dir']    =  $_SERVER['DOCUMENT_ROOT'] . '/xls';
    $files1 = scandir($data['dir']);
    foreach ($files1 as $file) {
      if((preg_match('/\.(xlsx)/', $file)||preg_match('/\.(xls)/', $file))){
        $data['files'][] = array (
          'name' => $file
        );
      }
    }

    $data['mans']  = $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['errors'] = $this->url->link('catalog/export/errors', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['clean'] = $this->url->link('catalog/export/clean', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['options'] = $this->url->link('catalog/export/options', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['button_options'] = $this->language->get('button_options');
    $data['products'] = $this->url->link('catalog/export/products', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['button_products'] = $this->language->get('button_products');

    $data['button_mans'] = $this->language->get('button_mans');
    $data['button_errors'] = $this->language->get('button_errors');
    $data['button_clean'] = $this->language->get('button_clean');
    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('catalog/export_products.tpl', $data));


  }

  protected function uploadFile() {
    if (isset($this->request->post['submit-_files'])) {
      $allowedExts = array("xls", "xlsx", "XLSX");
      $exten = explode(".", $_FILES["files"]["name"]);
      $extension = end($exten);
      if (($_FILES["files"]["size"] < 1300000000)
        && in_array($extension, $allowedExts))
      {
        if ($_FILES["files"]["error"] > 0)
        {
          return "Return Code: " . $_FILES["files"]["error"] . "<br>";
        }
        else
        {
          if (file_exists($_SERVER['DOCUMENT_ROOT'] . "xls/" . $_FILES["files"]["name"]))
          {
            return $_FILES["files"]["name"] . " already exists. ";
          }
          else
          {
            move_uploaded_file($_FILES["files"]["tmp_name"],
              $_SERVER['DOCUMENT_ROOT'] . "/xls/" . $_FILES["files"]["name"]);
            return "Stored in: " . "xls/" . $_FILES["files"]["name"];
          }
        }
      }
      else
      {
        $str = "Invalid file<br />";

        return $str;
      }

    }

  }

  public function options() {
    $url = "";
    $this->language->load('catalog/export');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('catalog/export');

    $data['heading_title'] = $this->language->get('heading_title');

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL')
    );
    if (isset($this->request->post['optproduct'])) {
      $names = $this->model_catalog_export->get_names();
      $artikules = $this->model_catalog_export->get_artikules();
      $tt=0;
      foreach ($names as $row) {
        $key="";
        $name = $row['name'];
        $pieces = explode($row['sku'], $name);
        if (isset($pieces[1])) {
          $analyze = str_replace("-","",str_replace(" ","",$pieces[1]));
          $key = array_search($analyze,$artikules);
          if (!empty($key)) {
            $tt++;
          } else {
            $analyze2 = substr($analyze, 0, -1);
            if (strlen($analyze2)>1)
              $key = array_search($analyze2,$artikules);
            if (!empty($key)) {
              $this->model_catalog_export->art_error($name,$analyze);
              $tt++;
            } else {
              $analyze2 = substr($analyze, 0, -2);
              if (strlen($analyze2)>1)
                $key = array_search($analyze2,$artikules);
              if (!empty($key)) {
                $this->model_catalog_export->art_error($name,$analyze);
                $tt++;
              } else {
                $analyze2 = substr($analyze, 0, -3);
                if (strlen($analyze2)>1)
                  $key = array_search($analyze2,$artikules);
                if (!empty($key)) {
                  $this->model_catalog_export->art_error($name,$analyze);
                  $tt++;
                }
              }

            }
          }
        }
      }
      echo $tt;
    }



    if (isset($this->request->post['add_opt'])) {
      require_once $_SERVER['DOCUMENT_ROOT'] . "/Classes/PHPExcel.php";/* подключаем класс */
      if (isset($this->request->post['file'])) {
        $file = $this->request->post['file'];
        $xls = $_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file;

        $startRow = 2;

        $chunkSize = 20;
        $objReader = PHPExcel_IOFactory::createReaderForFile($xls);

        $chunkFilter = new chunkReadFilter();
        $insert=0;
        $update=0;
        $double=0;
        while ($startRow <= 65000) {

          $chunkFilter->setRows($startRow,$chunkSize);
          $objReader->setReadFilter($chunkFilter);
          $objReader->setReadDataOnly(false);
          $objPHPExcel = $objReader->load($xls);		//открываем файл
          $objPHPExcel->setActiveSheetIndex(0);
          $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
          if ($startRow > $highestRow) break;
          $sheet = $objPHPExcel->getActiveSheet();
          $data['store']=0;
          $data['language_id'] = 1;
          for ($int = $startRow; $int < $startRow + $chunkSize; $int++) 	//внутренний цикл по строкам
          {
            if ($int > $highestRow) break;
            $attr_id = "";
            $data['attribute'] = $sheet->getCellByColumnAndRow(0, $int)->getValue();
            if(strstr($data['attribute'],'=')==true)
            {
              $data['attribute'] = $sheet->getCellByColumnAndRow(0, $int)->getOldCalculatedValue();
            }
            $data['attribute'] = str_replace("-","",$data['attribute']);
            $data['attribute2'] = $sheet->getCellByColumnAndRow(1, $int)->getValue();
            if(strstr($data['attribute2'],'=')==true)
            {
              $data['attribute2'] = $sheet->getCellByColumnAndRow(1, $int)->getOldCalculatedValue();
            }
            $data['attribute2'] = str_replace("-","",$data['attribute2']);
            $attr_id = $this->model_catalog_export->search_attr($data['attribute']);
            if (empty($attr_id))
              $this->model_catalog_export->add_attr($data);
            $attr_id2 = $this->model_catalog_export->search_attr($data['attribute2']);
            if (empty($attr_id2))
              $this->model_catalog_export->add_attr2($data,$attr_id);
          }//for

          $startRow += $chunkSize;

        }
        unset($objReader);
        unset($objPHPExcel);
      }
    }
    $data['token'] = $this->session->data['token'];
    $data['mans']  = $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['errors'] = $this->url->link('catalog/export/errors', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['clean'] = $this->url->link('catalog/export/clean', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['products'] = $this->url->link('catalog/export/products', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['button_products'] = $this->language->get('button_products');
    $data['dir']    =  $_SERVER['DOCUMENT_ROOT'] . '/xls';
    $data['button_mans'] = $this->language->get('button_mans');
    $data['button_errors'] = $this->language->get('button_errors');
    $data['button_clean'] = $this->language->get('button_clean');
    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('catalog/export_options.tpl', $data));


  }

  public function clean() {
    $url = "";
    $this->language->load('catalog/export');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('catalog/export');

    $data['heading_title'] = $this->language->get('heading_title');

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL')
    );

    if (isset($this->request->post['clean'])) {
      $this->model_catalog_export->clean();
    }
    if (isset($this->request->post['cleanman'])) {
      $this->model_catalog_export->cleanman();
    }
    $data['mans']  = $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['errors'] = $this->url->link('catalog/export/errors', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['clean'] = $this->url->link('catalog/export/clean', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['products'] = $this->url->link('catalog/export/products', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['options'] = $this->url->link('catalog/export/options', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['button_options'] = $this->language->get('button_options');
    $data['button_mans'] = $this->language->get('button_mans');
    $data['button_products'] = $this->language->get('button_products');
    $data['button_errors'] = $this->language->get('button_errors');
    $data['button_clean'] = $this->language->get('button_clean');
    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('catalog/export_clean.tpl', $data));
  }

  public function errors() {
    $url = "";
    $this->language->load('catalog/export');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('catalog/export');

    $data['heading_title'] = $this->language->get('heading_title');

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL')
    );

//		$results2 = $this->model_catalog_export->getDownload();

//		foreach ($results2 as $result2) {
//			$data['downloads'][] = array(
//				'col_num' => $result2['col_num'],
//				'product_id'        => $result2['product_id']
//			);
//		}
    $data['token'] = $this->session->data['token'];
    $data['mans']  = $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['errors'] = $this->url->link('catalog/export/errors', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['clean'] = $this->url->link('catalog/export/clean', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['products'] = $this->url->link('catalog/export/products', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['options'] = $this->url->link('catalog/export/options', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['button_options'] = $this->language->get('button_options');
    $data['button_mans'] = $this->language->get('button_mans');
    $data['button_products'] = $this->language->get('button_products');
    $data['button_errors'] = $this->language->get('button_errors');
    $data['button_clean'] = $this->language->get('button_clean');

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('catalog/export_errors.tpl', $data));
  }

  protected function getList() {
    $url = '';

    $data['dir']    =  $_SERVER['DOCUMENT_ROOT'] . '/xls';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('catalog/export', 'token=' . $this->session->data['token'] . $url, 'SSL')
    );
    $this->uploadFile();
    $data['heading_title'] = $this->language->get('heading_title');
    $data['errors'] = $this->url->link('catalog/export/errors', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['products'] = $this->url->link('catalog/export/products', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['clean'] = $this->url->link('catalog/export/clean', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['ajaxgetman'] = $this->url->link('catalog/export/ajaxGetMan', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['token'] = $this->session->data['token'];
    $data['options'] = $this->url->link('catalog/export/options', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['button_options'] = $this->language->get('button_options');
    $data['button_products'] = $this->language->get('button_products');
    $data['button_errors'] = $this->language->get('button_errors');
    $data['button_clean'] = $this->language->get('button_clean');
    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('catalog/export.tpl', $data));
  }

  public function images() {
    $json = array();
    $data = array();
//		if (isset($this->request->get['addimage'])) {
//echo "test";
    $this->load->model('catalog/export');
    if (isset($_SESSION['startRow'])) $last = $_SESSION['startRow']; else $last = 0;
    $artikules = array();
    if (isset($_SESSION['insert'])) $json['insert'] = $_SESSION['insert']; else
      $json['insert'] = 0;

    $max = $this->model_catalog_export->maxProduct();
    $artikules = $this->model_catalog_export->getProductforimage($last);
    foreach ($artikules as $artikule) {
      $string_to_search="/var/www/vhosts/allchips.com.ua/httpdocs/image/catalog/allchips/*/*".$artikule['sku']."*";
      $search = glob($string_to_search);
      if (!empty($search)) {
        $i=0;
        foreach ($search as $filename) {
          $filename = substr($filename,47);
          $this->model_catalog_export->addImage($filename, $artikule['product_id'], $i);
          $i++;
        }
        $json['insert'] = $json['insert']+1;
      }

      if ($max == $artikule['product_id']) {
        $json['success'] = 'The End';
        unset($_SESSION['startRow']);
        unset($_SESSION['insert']);
      }
      else
      {
        $json['success'] = $json['insert'];
        $_SESSION['startRow'] = $artikule['product_id'];
        $_SESSION['insert'] = $json['insert'];
      }
    }

    $this->response->setOutput(json_encode($json));

//}
  }

  public function korpusa() {
    $json = array();
    $data = array();
//		if (isset($this->request->get['addimage'])) {
//echo "test";
    $this->load->model('catalog/export');
    if (isset($_SESSION['startRow'])) $last = $_SESSION['startRow']; else $last = 0;
    $artikules = array();
    if (isset($_SESSION['insert'])) $json['insert'] = $_SESSION['insert']; else
      $json['insert'] = 0;
    $i=0;
    $max = $this->model_catalog_export->maxProduct();
    $rows = $this->model_catalog_export->get_names($last);
    foreach ($rows as $row) {
      $key2 = 0;
      $pieces = explode(" ", $row['name_prom']);
      $key2 = array_search($row['sku'] , $pieces)+1;
      if (($key2>=0) && (isset($pieces[$key2]))) {
        if ($pieces[$key2]) $data['korpus'] = $pieces[$key2];
        $data['korpus_id'] = $this->model_catalog_export->search_attr($data['korpus']);
        if ($data['korpus_id']>0) {
          $data['product_id'] = $row['product_id'];
          $data['name'] = $row['sku']." ".$data['korpus']." ".$row['db_brand'];
          $data['description'] = "<p>Микросхема ".$row['db_brand']." ".$data['korpus']." ".$row['sku']." всегда можете приобрести у нас по самой выгодной цене. Оформить заказ на ".$row['sku']." ".$row['db_brand']." очень просто, для этого нужно только заполнить форму заказа, регистрация не требуется. В наличии большой ассортимент сопутствующих электронных компонентов. ".$row['sku']." ".$row['db_brand']." ".$data['korpus']." можно приобрести как по 100% предоплате так и наложенным платежом, без предоплаты.</p><p>На новые электронные компоненты предоставляется гарантия 60 дней, на б/у – гарантия на включение.</p>";

          $this->model_catalog_export->add_product_attr($data);
          $i++;
          $json['insert'] = $json['insert']+1;
        }
      }
      if ($max == $row['product_id']) {
        $json['success'] = 'The End';
        unset($_SESSION['startRow']);
        unset($_SESSION['insert']);
      }
      else
      {
        $json['success'] = $json['insert'];
        $_SESSION['startRow'] = $row['product_id'];
        $_SESSION['insert'] = $json['insert'];
      }
    }

    $this->response->setOutput(json_encode($json));

//}
  }

  public function korpusa_action() {
    $json = array();
    $data = array();
    if (isset($_SESSION['insert'])) $json['insert'] = $_SESSION['insert']; else
      $json['insert'] = 0;
    require_once $_SERVER['DOCUMENT_ROOT'] . "/Classes/PHPExcel.php";/* подключаем класс */
    $dir = $_SERVER['DOCUMENT_ROOT'] . "/11/";
    $files = scandir($dir);
    if (isset($_SESSION['startRow'])) $last = $_SESSION['startRow']; else $last = 0;
    $this->load->model('catalog/export');
    $max = $this->model_catalog_export->maxProduct();
    $products = $this->model_catalog_export->get_names($last);
    $i=0;
    foreach ($products as $product) {
      $i++;
      $last = $product['product_id'];
      if ($i< 100) {
        foreach ($files as $file) {
          if(preg_match('/\.(xlsx)/', $file)){
            $objPHPExcel = PHPExcel_IOFactory::load($dir.$file);
            $objPHPExcel->setActiveSheetIndex(0);
            $worksheet = $objPHPExcel->getActiveSheet();

            $row = 1;
            $lastColumn = $worksheet->getHighestColumn();
            $lastColumn++;
            for ($column = 'A'; $column != $lastColumn; $column++) {
              $cell = $worksheet->getCell($column.$row);
              if ($cell == "Идентификатор_товара") {
                $lastRow = $worksheet->getHighestRow();
                for ($row = 1; $row <= $lastRow; $row++) {
                  $cell = $worksheet->getCell($column.$row);
                  if ($cell == $product['sku']) {
                    for ($column = 'A'; $column != $lastColumn; $column++) {
                      $cell = $worksheet->getCell($column."1");
                      if ($cell == "Количество") {
                        $data['korpus'] = $worksheet->getCell($column.$row);
                        $data['korpus_id'] = $this->model_catalog_export->search_attr($data['korpus']);
                        if ($data['korpus_id']>0) {
                          $data['product_id'] = $product['product_id'];
                          $data['name'] = $product['sku']." ".$data['korpus']." ".$product['db_brand'];
                          $data['description'] = "<p>Микросхема ".$product['db_brand']." ".$data['korpus']." ".$product['sku']." всегда можете приобрести у нас по самой выгодной цене. Оформить заказ на ".$product['sku']." ".$product['db_brand']." очень просто, для этого нужно только заполнить форму заказа, регистрация не требуется. В наличии большой ассортимент сопутствующих электронных компонентов. ".$product['sku']." ".$product['db_brand']." ".$data['korpus']." можно приобрести как по 100% предоплате так и наложенным платежом, без предоплаты.</p><p>На новые электронные компоненты предоставляется гарантия 60 дней, на б/у – гарантия на включение.</p>";
                          $json['insert']++;
                          $this->model_catalog_export->add_product_attr($data);
                        } unset($objPHPExcel);
                        break 4;
                      }
                    }
                  }
                }
              }
            }
            unset($objPHPExcel);
          }
        }
      } else {
        break;

      }
    }
    if ($max == $last) {
      $json['success'] = 'The End';
      unset($_SESSION['startRow']);
      unset($_SESSION['insert']);
    }
    else
    {
      $json['success'] = $last;
      $_SESSION['startRow'] = $last;
      $_SESSION['insert'] = $json['insert'];
    }
    $this->response->setOutput(json_encode($json));
  }

  public function ajaxdownload() {
    $json = array();
    $data = array();
    $results = array();

    ini_set('memory_limit', '512M');
//ini_set('max_execution_time', 1200);
//require_once $_SERVER['DOCUMENT_ROOT'] . "/shop/Classes/PHPExcel.php";/* подключаем класс */
    $this->load->model('catalog/export');
    $this->load->model('catalog/product');
    if (isset($_SESSION['zet'])) {
      $zet = $_SESSION['zet'];} else {$zet = 0;}

    $xls = $_SERVER['DOCUMENT_ROOT'] . '/load.csv';

    if (!isset($_SESSION['row'])) {
      if (file_exists($xls)) unlink($xls);
      if (isset($_SESSION['startRow'])) {
        $startRow = $_SESSION['startRow'];}
      else {$startRow =0;}
//$objPHPExcel = new PHPExcel();
      $row = 1;
    } else {
      $startRow = $_SESSION['startRow'];
      $row = $_SESSION['row'];
//    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
//    $objPHPExcel = $objReader->load($xls);

    }

    $last = 0;
    $output = fopen($xls, "a");
    /* CSV Header Starts Here */

    $datefrom = "";
    $dateto = "";
//$objPHPExcel->setActiveSheetIndex(0);
//$sheet = $objPHPExcel->getActiveSheet();
//$row2 = $sheet->getHighestRow();
//$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_memcache;
//$cacheSettings = array( 'memcacheServer' => 'localhost', 'memcachePort' => 11211, 'cacheTime' => 600,' memoryCacheSize ' => '64MB' );
//PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    if ($this->request->get['error_type']=="options") {
      $results = $this->model_catalog_export->getDownloadArt();
      foreach ($results as $result) {
        $sheet->setCellValue('A'.($row+1),$result['nofind']);
        $sheet->setCellValue('B'.($row+1),$result['find']);
        $row=$row+1;
//$json['insert'] = $json['insert']+1;
      }
    } elseif ($this->request->get['error_type']=="products_prom") {

      if ($this->request->get['date_modified_from']) {
        $datefrom = $this->request->get['date_modified_from'];
      }
      if ($this->request->get['date_modified_to']) {
        $dateto = $this->request->get['date_modified_to'];
      }
      if ($row == 1) {
        $titles = array('Код_товара','Название_позиции','Ключевые_слова','Описание', 'Тип_товара','Цена','Валюта','Скидка','Единица_измерения','Минимальный_объем_заказа','Оптовая_цена','Минимальный_заказ_опт',
          'Наличие','Производитель','Идентификатор_товара','Уникальный_идентификатор');
        fputcsv($output, $titles, ';');
      }
      $results = $this->model_catalog_export->getDownloadProducts($startRow,$datefrom,$dateto);

      $data = array();

// We don't want to export all the information to be exported so maintain a separate array for the information to be exported
      foreach($results as $result)
      {
        $opt_price = "";
        $opt = "";
        $discounts = $this->model_catalog_product->getProductDiscounts($result['product_id']);
        if (!empty($discounts)) {
          foreach($discounts as $discount) {
            $opt_price .= $discount['price'].";";
            $opt .=$discount['quantity']. ";";
          }
        }
        if ((!empty($result['measure_id']))||($result['measure_id'] >1)) {
          $measure = $this->model_catalog_export->getMeasure($result['measure_id']);
        } else {
          $measure = "шт.";
        }
        if ($result['minimum']>1) {
          $minimum = $result['minimum'];
          $type = "w"; } elseif (empty($opt)) {
          $type = "r";
          $minimum = "";
        } else {
          $type = "u";
          $minimum = "";
        }
        if ($result['days_to_stock']>0) {
          $days_to_stock = $result['days_to_stock'];
        } else if ($result['stock_status_id']==7) {
          $days_to_stock = "+";
        } else {
          $days_to_stock = "-";
        }
        if ($result['price']==0) $result['price'] = "";

        $data[] = array(
          'model' =>$result['model'],
          'name' =>$result['name'],
          'keys' => "",
          'description' => $result['description_prom'].$result['description_second'],
          'type' => $type,
          'price' => $result['price'],
          'valuta' => "USD",
          'skidka' => "",
          'quant' => $measure,
          'minimum' => $minimum,
          'opt_price' => $opt_price,
          'opt' => $opt,
          'days' => $days_to_stock,
          'brand' => $result['PromUA_name'],
          'sku' => $result['sku'],
          'product_id' =>$result['product_id'],
        );

        $row++;
        $last= $result['product_id'];
      }
    } elseif ($this->request->get['error_type']=="products_mans") {
      $results = $this->model_catalog_export->getDownload();
      foreach ($results as $result) {
        $sheet->setCellValue('F'.($i+1),$result['col_num']);
        $sheet->setCellValue('A'.($i+1),$result['product_id']);
        $sheet->setCellValue('C'.($i+1),$result['model']);
        $sheet->setCellValue('D'.($i+1),$result['sku']);
        $sheet->setCellValue('E'.($i+1),$result['manufacturer']);
        $sheet->setCellValue('B'.($i+1),$result['name']);
        $sheet->setCellValue('G'.($i+1),$result['currency']);
        $sheet->setCellValue('H'.($i+1),$result['error']);
        $i=$i+1;
      }
    }

    $json['insert'] = $row;
    $json['zet'] = $zet;

    $_SESSION['row'] = $row;
//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save($xls);
//   $objPHPExcel->disconnectWorksheets();
//    unset($objReader);
// unset($objPHPExcel);
// Exporting the CSV
//header('Content-Type: application/download');
//header("Content-Disposition: attachment; filename=data.csv");
// Disable caching
//header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
//header("Pragma: no-cache"); // HTTP 1.
//header("Expires: 0"); // Proxies

    /* CSV Header Ends Here */

    foreach($data as $row)
    {
      fputcsv($output, $row, ";"); // here you can change delimiter/enclosure
    }

    fclose($output); // Closing the File
    header('Content-Type: application/json');
    $max = $this->model_catalog_export->maxProduct2($datefrom, $dateto);

    if ($max == $last) {
      $json['success'] = 'The End';
      unset($_SESSION['startRow']);
      unset($_SESSION['row']);
      unset($_SESSION['zet']);
    }
    else
    {
      $json['success'] = memory_get_usage();
      $_SESSION['startRow'] = $last;
      $_SESSION['zet'] = $zet;
    }
// unset($objPHPExcel);

    $this->response->setOutput(json_encode($json));

  }

  public function ajaxGetProducts() {
    $json = array();
    $data = array();
    $files = array();
//ini_set('memory_limit', '128M');
//ini_set('max_execution_time', 200);
    //	if (isset($this->request->post['import'])) {
    require_once $_SERVER['DOCUMENT_ROOT'] . "/system/PHPExcel/Classes/PHPExcel.php";/* подключаем класс */
    if (isset($this->request->get['file'])) {

      $file = $this->request->get['file']; }
    if (isset($this->request->get['funct'])) {
      $funct = $this->request->get['funct'];

    }
    $xls = $_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file;
    $this->load->model('catalog/export');
    $files['name'] = $file;
    if ($funct == "delete") {
      $this->model_catalog_export->deleteFile($file);
      if (file_exists($xls)) unlink($xls);
      $json['success'] = 'The End';
      $this->response->setOutput(json_encode($json));
    } else {
      if (isset($_SESSION['startRow'])) $startRow = $_SESSION['startRow'];
      else $startRow = 1;
      if (isset($_SESSION['insert'])) $data['insert'] = $_SESSION['insert'];
      else $data['insert']=0;
      if (isset($_SESSION['update'])) $data['update'] = $_SESSION['update'];
      else $data['update']=0;
      if (isset($_SESSION['errors'])) $errors = $_SESSION['errors'];
      else $errors=0;
      $chunkSize = 500;

      $objReader = PHPExcel_IOFactory::createReader('Excel5');
      $chunkFilter = new chunkReadFilter();

      $update=0;
      $double=0;
      $error_file = "";
//while ($startRow <= 165000) {
      $chunkFilter->setRows($startRow,$chunkSize);
      $objReader->setReadFilter($chunkFilter);
      $objReader->setReadDataOnly(false);
      $objPHPExcel = $objReader->load($xls);		//открываем файл
      //   $objPHPExcel->setActiveSheetIndex(0);
      $sheet = $objPHPExcel->getSheet(0);
      $highestRow = $sheet->getHighestRow();

      if ($startRow == 1) {
        $startRow = 2;
        $value = array();
        $files['string'] = "";
        $nColumn = PHPExcel_Cell::columnIndexFromString(
          $sheet->getHighestColumn());
        for ($j = 0; $j < $nColumn; $j++) {
          $value[$j] = $sheet->getCellByColumnAndRow($j, 1)->getValue();
        }
        $search = array('Артикул', 'О П И С А Н И Е', 'Кол-во в наб.', 'Наб. в ящ.', 'Цена клиента' );
        for ($i = 0; $i < count($search); $i++) {
          $key = array_search($search[$i], $value);
          if ($key===FALSE) { $key = "-1";
            $error_file .= " ".$search[$i];}
          $files['string'] .= $key . ";";
        }
        $data['file_id'] = $this->model_catalog_export->addFile($files);
      } else {
        $string = $this->model_catalog_export->selectFile($files);
        $data['file_id'] = $string['id'];
        $files['string'] = $string['cols'];
      }
      $cols = explode(";", $files['string']);
      if (($cols[0]==-1)||($cols[1]==-1)||($cols[2]==-1)||($cols[3]==-1)||($cols[4]==-1)) {
        $json["error_file"] = "Нет колонки ".$error_file;
      }
      $data['store_id']=0;
      $data['language_id'] = 1;
      //получим итератор строки и пройдемся по нему циклом
      if (!isset($json["error_file"])) {
        for ($int = $startRow; $int < $startRow + $chunkSize; $int++) 	//внутренний цикл по строкам
        {
          if ($int > $highestRow) break;
          $result = "";
          $data['price_1'] = "";
          $data['price_u'] = "";
          $data['price_y'] = "";
          $data['int'] = $int;
          $data['quant_u'] = "";

          /*if(strstr($data['name'],'=')==true)
          {
            $data['name'] = $sheet->getCellByColumnAndRow(1, $int)->getOldCalculatedValue();
          }		*/
          if ($cols[0]>-1) {
            $data['model'] = $sheet->getCellByColumnAndRow($cols[0], $int)->getValue();
          }
          if ($cols[1]>-1)
            $data['name'] = $sheet->getCellByColumnAndRow($cols[1], $int)->getValue();
          if ($cols[2]>-1)
            $data['quant_u'] = $sheet->getCellByColumnAndRow($cols[2]+1, $int)->getValue();
          if ($cols[3]>-1) {
            $data['quant_y'] = $sheet->getCellByColumnAndRow($cols[3], $int)->getValue();
            if (!is_numeric($data['quant_y'])) {
              $data['quant_y'] = $sheet->getCellByColumnAndRow($cols[3]+1, $int)->getValue();
            }

          }

          $data['price'] = $sheet->getCellByColumnAndRow($cols[4], $int)->getValue();
          if(strstr($data['price'],'=')==true)
          {
            $data['price'] = $sheet->getCellByColumnAndRow($cols[4], $int)->getOldCalculatedValue();
          }

          if ($data['quant_u']!=0) {
            if ($data['quant_u']==1) {
              $data['price_1'] =  $data['price'];
              $data['see_1'] = 1;
              $data['status_1'] = 1;
              $data['price_y'] = $data['price'] * $data['quant_y'];
              $data['see_y'] = 1;
              $data['status_y'] = 1;
            } elseif ($data['quant_u']>1) {
              $data['price_1'] = $data['price'] / $data['quant_u'];
              $data['see_1'] = 1;
              $data['status_1'] = 0;
              $data['price_u'] = $data['price'];
              $data['see_u'] = 1;
              $data['status_u'] = 1;
              $data['price_y'] = $data['price_u'] * $data['quant_y'];
              $data['see_y'] = 1;
              $data['status_y'] = 1;
              $data['quant_y'] = $data['quant_y'] * $data['quant_u'];
            }

            $error = "";
            if (empty($data['model'])) {
              $error .= "нет кода товара, ";
            }
            if (empty($data['name'])) {
              $error .= "нет названия, ";
            }

            $data['url'] = $this->model_catalog_export->str2url($data['name']);

            if (empty($error)) {
              if (stripos($data['model'], 'sl') == false)
                list($data['insert'],$data['update']) = $this->model_catalog_export->addProduct($data);

            } else {
              $errors++;
              //$this->model_catalog_export->addError($data,$error);
              file_put_contents("errors.txt",$file.": Строка:".$data['int']." - ".$error."\n",  FILE_APPEND );
            }
          }//конец цикла
        }
        $_SESSION['startRow'] = $int;
        $_SESSION['insert'] = $data['insert'];
        $_SESSION['update'] = $data['update'];
        $_SESSION['errors'] = $errors;
        $json['insert'] = $data['insert'];
        $json['update'] = $data['update'];
        $json['file'] = $file;
        $json['int'] = $int;

        if ($int >= $highestRow) {
          $json['success'] = 'The End';
          unset($objReader);
          unset($objPHPExcel);
          unset($_SESSION['startRow'],$_SESSION['insert'],$_SESSION['update'],$_SESSION['errors']);
          $this->model_catalog_export->deleteFile($file);
          if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file)) unlink($_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file);
        }
      } else {
        $json['success'] = 'The End';
        unset($objReader);
        unset($objPHPExcel);
        unset($_SESSION['startRow'],$_SESSION['insert'],$_SESSION['update'],$_SESSION['errors']);
      }

      $this->cleanSeoUrl();
      $this->response->setOutput(json_encode($json));
    }
  }

  private function cleanSeoUrl() {
    $sql = "SELECT *, count(`keyword`) FROM `oc_url_alias` WHERE keyword !='' GROUP BY  `keyword` HAVING COUNT(`keyword`) > 1 ";
    $query = $this->db->query($sql);
    if (!empty($query->rows) && $query->num_rows > 0) {
      foreach ($query->rows as $result) {
        $sql_keyword = "SELECT * FROM `oc_url_alias` WHERE keyword ='" . $result["keyword"] . "' ORDER BY `url_alias_id` ASC";
        $query_keyword = $this->db->query($sql_keyword);
        if (!empty($query_keyword) && $query_keyword->num_rows > 0) {
          $n=1;
          foreach ($query_keyword->rows as $res) {
            if($n>1) {
              $new_keyword =  $res["keyword"]."-".$n;
              $this->db->query("UPDATE `oc_url_alias` SET keyword ='" . $new_keyword . "' WHERE url_alias_id='". $res['url_alias_id'] ."'");
            }
            $n++;
          }
        }
      }
    }

  }

  public function ajaxGetMan() {
    $json = array();
    $data = array();
    //	if (isset($this->request->post['import'])) {
    require_once $_SERVER['DOCUMENT_ROOT'] . "/Classes/PHPExcel.php";/* подключаем класс */
    if (isset($this->request->get['file'])) {
      $file = $this->request->get['file'];
      $xls = $_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file;
      $this->load->model('catalog/export');
      if (isset($this->request->get['funct'])) {
        $funct = $this->request->get['funct'];
      }
      if (isset($_SESSION['startRow'])) $startRow = $_SESSION['startRow'];
      else $startRow = 2;

      $chunkSize = 20;
      $objReader = PHPExcel_IOFactory::createReaderForFile($xls);

      $chunkFilter = new chunkReadFilter();
      $insert=0;
      $update=0;
      $double=0;
      while ($startRow <= 65000) {

        $chunkFilter->setRows($startRow,$chunkSize);
        $objReader->setReadFilter($chunkFilter);
        $objReader->setReadDataOnly(false);
        $objPHPExcel = $objReader->load($xls);		//открываем файл
        $objPHPExcel->setActiveSheetIndex(1);
        $highestRow = $objPHPExcel->setActiveSheetIndex(1)->getHighestRow();
        if ($startRow > $highestRow) break;
        $sheet = $objPHPExcel->getActiveSheet();
        $data['store']=0;
        $data['language_id'] = 1;
        for ($int = $startRow; $int < $startRow + $chunkSize; $int++) 	//внутренний цикл по строкам
        {
          if ($int > $highestRow) break;
          $desc="";
          $offic="";
          $description="";
          $data['manufacturer'] = $sheet->getCellByColumnAndRow(2, $int)->getValue();
          if(strstr($data['manufacturer'],'=')==true)
          {
            $data['manufacturer'] = $sheet->getCellByColumnAndRow(2, $int)->getOldCalculatedValue();
          }
          $data['db_brand'] = $sheet->getCellByColumnAndRow(1, $int)->getValue();
          $description = $sheet->getCellByColumnAndRow(5, $int)->getValue();
          $data['prom_name'] = $sheet->getCellByColumnAndRow(3, $int)->getValue();
          $data['offic'] = $sheet->getCellByColumnAndRow(4, $int)->getValue();
          if(strstr($data['offic'],'=')==true)
          {
            $data['offic'] = $sheet->getCellByColumnAndRow(4, $int)->getOldCalculatedValue();
          }
          $data['col_1'] = $sheet->getCellByColumnAndRow(6, $int)->getValue();
          $data['col_2'] = $sheet->getCellByColumnAndRow(7, $int)->getValue();
          $data['col_3'] = $sheet->getCellByColumnAndRow(8, $int)->getValue();
          $data['col_4'] = $sheet->getCellByColumnAndRow(9, $int)->getValue();
          $data['col_5'] = $sheet->getCellByColumnAndRow(10, $int)->getValue();
          $data['col_6'] = $sheet->getCellByColumnAndRow(11, $int)->getValue();
          $data['desc'] = $data['offic'] . "<br />" . $description;
          $data['url'] = $this->model_catalog_export->str2url($data['manufacturer']);
          $search = "";
          if (!empty($data['manufacturer'])) {
            $searchCol = "name";
            $search =  $this->model_catalog_export->searchMan($data,$searchCol);
            if (!isset($search)) {
              $this->model_catalog_export->addMan($data);
              $insert=$insert+1;
            } else {
              if (isset($funct) && ($funct=="update")) {
                $this->model_catalog_export->updateMan($data);
              }
              $json['htmltext'][] =   $int . ": " . $data['manufacturer'] . " - exist<br>";
              $double++;
            }

          }
          else {
            $json['htmltext'][] =  $int . ": - name not exist<br>";
          }
        } // for
        $startRow += $chunkSize;
        $_SESSION['startRow'] = $startRow;


      } //while

      $json['double'] = $double;
      $json['insert'] = $insert;
      $json['file'] = $file;
      $json['success'] = 'The End';

      $this->response->setOutput(json_encode($json));
      unset($objReader);
      unset($objPHPExcel);
      unset($_SESSION['startRow']);

//}

    }
  }

  public function ajaxChProducts() {
    $json = array();
    $data = array();
    $files = array();
//ini_set('memory_limit', '128M');
//ini_set('max_execution_time', 200);
    //	if (isset($this->request->post['import'])) {
    require_once $_SERVER['DOCUMENT_ROOT'] . "/system/PHPExcel/Classes/PHPExcel.php";/* подключаем класс */
    if (isset($this->request->get['file'])) {

      $file = $this->request->get['file']; }
    if (isset($this->request->get['funct'])) {
      $funct = $this->request->get['funct'];

    }
    $xls = $_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file;
    $this->load->model('catalog/export');
    $files['name'] = $file;
    if ($funct == "delete") {
      $this->model_catalog_export->deleteFile($file);
      if (file_exists($xls)) unlink($xls);
      $json['success'] = 'The End';
      $this->response->setOutput(json_encode($json));
    } else {
      if (isset($_SESSION['startRow'])) $startRow = $_SESSION['startRow'];
      else $startRow = 1;
      if (isset($_SESSION['insert'])) $data['insert'] = $_SESSION['insert'];
      else $data['insert']=0;
      if (isset($_SESSION['update'])) $data['update'] = $_SESSION['update'];
      else $data['update']=0;
      if (isset($_SESSION['errors'])) $errors = $_SESSION['errors'];
      else $errors=0;
      $chunkSize = 500;

      $objReader = PHPExcel_IOFactory::createReader('Excel5');
      $chunkFilter = new chunkReadFilter();

      $update=0;
      $double=0;
      $error_file = "";
//while ($startRow <= 165000) {
      $chunkFilter->setRows($startRow,$chunkSize);
      $objReader->setReadFilter($chunkFilter);
      $objReader->setReadDataOnly(false);
      $objPHPExcel = $objReader->load($xls);		//открываем файл
      //   $objPHPExcel->setActiveSheetIndex(0);
      $sheet = $objPHPExcel->getSheet(0);
      $highestRow = $sheet->getHighestRow();

      if ($startRow == 1) {
        $startRow = 3;
        $value = array();
        $files['string'] = "";
        $nColumn = PHPExcel_Cell::columnIndexFromString(
          $sheet->getHighestColumn());
        for ($j = 0; $j < $nColumn; $j++) {
          $value[$j] = $sheet->getCellByColumnAndRow($j, 2)->getValue();
        }
        $search = array('Код', 'Артикул', 'Штрихкод', 'Номенклатура', 'В спайке', 'В картоне', 'Цена базового прайса, грн', 'Доступно картонов' );
        for ($i = 0; $i < count($search); $i++) {
          $key = array_search($search[$i], $value);
          if ($key===FALSE) { $key = "-1";
            $error_file .= " ".$search[$i];}
          $files['string'] .= $key . ";";
        }
        $data['file_id'] = $this->model_catalog_export->addFile($files);
      } else {
        $string = $this->model_catalog_export->selectFile($files);
        $data['file_id'] = $string['id'];
        $files['string'] = $string['cols'];
      }
      $cols = explode(";", $files['string']);
      if (($cols[0]==-1)||($cols[1]==-1)||($cols[2]==-1)||($cols[3]==-1)||($cols[4]==-1)||($cols[5]==-1)) {
        $json["error_file"] = "Нет колонки ".$error_file;
      }
      $data['store_id']=0;
      $data['language_id'] = 1;
      //получим итератор строки и пройдемся по нему циклом


      if (!isset($json["error_file"])) {
        for ($int = $startRow; $int < $startRow + $chunkSize; $int++) 	//внутренний цикл по строкам
        {
          if ($int > $highestRow) break;
          $result = "";
          $data['price_1'] = "";
          $data['price_u'] = "";
          $data['price_y'] = "";
          $data['int'] = $int;
          $data['see_1'] = 0;
          $data['status_1'] = 0;
          $data['see_u'] = 0;
          $data['status_u'] = 0;
          $data['see_y'] = 0;
          $data['status_y'] = 0;

          /*if(strstr($data['name'],'=')==true)
          {
            $data['name'] = $sheet->getCellByColumnAndRow(1, $int)->getOldCalculatedValue();
          }		*/


          $data['sku'] = $sheet->getCellByColumnAndRow($cols[0], $int)->getValue();
          $data['model'] = $sheet->getCellByColumnAndRow($cols[1], $int)->getValue();
          $data['barcode'] = $sheet->getCellByColumnAndRow($cols[2], $int)->getValue();
          $data['name'] = $sheet->getCellByColumnAndRow($cols[3], $int)->getValue();
          $data['quant_u'] = $sheet->getCellByColumnAndRow($cols[4], $int)->getValue();
          $data['quant_y'] = $sheet->getCellByColumnAndRow($cols[5], $int)->getValue();
          $data['price'] = $sheet->getCellByColumnAndRow($cols[6], $int)->getValue();


          if ((empty($data['quant_u']))&&(!empty($data['quant_y']))) {
            $data['price_1'] =  $data['price'];
            $data['see_1'] = 1;
            $data['status_1'] = 1;
            $data['price_y'] = $data['price'] * $data['quant_y'];
            $data['see_y'] = 1;
            $data['status_y'] = 1;
          } elseif ($data['quant_u']==$data['quant_y']) {
            $data['price_1'] =  $data['price'];
            $data['see_1'] = 1;
            $data['status_1'] = 0;
            $data['price_u'] = $data['price'] * $data['quant_u'];
            $data['see_u'] = 1;
            $data['status_u'] = 1;
          } elseif ($data['quant_u']!=$data['quant_y']) {
            $data['price_1'] = $data['price'];
            $data['see_1'] = 1;
            $data['status_1'] = 0;
            $data['price_u'] = $data['price'] * $data['quant_u'];
            $data['see_u'] = 1;
            $data['status_u'] = 1;
            $data['price_y'] = $data['price'] * $data['quant_y'];
            $data['see_y'] = 1;
            $data['status_y'] = 1;
          }

          $error = "";
          if (empty($data['model'])) {
            $error .= "нет кода товара, ";
          }
          if (empty($data['name'])) {
            $error .= "нет названия, ";
          }

          $data['url'] = $this->model_catalog_export->str2url($data['name']);

          if (empty($error)) {
            if (stripos($data['model'], 'sl') == false) {
              list($data['insert'],$data['update']) = $this->model_catalog_export->add2Product($data);
            }
          } else {
            $errors++;
            //$this->model_catalog_export->addError($data,$error);
            file_put_contents("errors.txt", $file.": Строка:".$data['int']." - ".$error."\n");
          }
        }//конец цикла

        $_SESSION['startRow'] = $int;
        $_SESSION['insert'] = $data['insert'];
        $_SESSION['update'] = $data['update'];
        $_SESSION['errors'] = $errors;
        $json['insert'] = $data['insert'];
        $json['update'] = $data['update'];
        $json['file'] = $file;
        $json['int'] = $int;

        if ($int >= $highestRow) {
          $json['success'] = 'The End';
          unset($objReader);
          unset($objPHPExcel);
          unset($_SESSION['startRow'],$_SESSION['insert'],$_SESSION['update'],$_SESSION['errors']);
          $this->model_catalog_export->deleteFile($file);
          if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file)) unlink($_SERVER['DOCUMENT_ROOT'] . '/xls/' . $file);
        }
      } else {
        $json['success'] = 'The End';
        unset($objReader);
        unset($objPHPExcel);
        unset($_SESSION['startRow'],$_SESSION['insert'],$_SESSION['update'],$_SESSION['errors']);
      }

      $this->cleanSeoUrl();
      $this->response->setOutput(json_encode($json));
    }
  }

  public function rus2translit($string) {
    $converter = array(
      'а' => 'a',   'б' => 'b',   'в' => 'v',
      'г' => 'g',   'д' => 'd',   'е' => 'e',
      'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
      'и' => 'i',   'й' => 'y',   'к' => 'k',
      'л' => 'l',   'м' => 'm',   'н' => 'n',
      'о' => 'o',   'п' => 'p',   'р' => 'r',
      'с' => 's',   'т' => 't',   'у' => 'u',
      'ф' => 'f',   'х' => 'h',   'ц' => 'c',
      'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
      'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
      'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

      'А' => 'A',   'Б' => 'B',   'В' => 'V',
      'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
      'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
      'И' => 'I',   'Й' => 'Y',   'К' => 'K',
      'Л' => 'L',   'М' => 'M',   'Н' => 'N',
      'О' => 'O',   'П' => 'P',   'Р' => 'R',
      'С' => 'S',   'Т' => 'T',   'У' => 'U',
      'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
      'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
      'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
      'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
  }

  public function str2url($str) {
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    return $str;
  }
}
