<?php

class City
{

    private $city_id;

    public function __construct($registry)
    {

        $this->db = $registry->get('db');
        $this->session = $registry->get('session');
        $this->cache = $registry->get('cache');

        $this->city_id = '';

        if (isset($this->session->data['city_id'])):
            $this->city_id = $this->session->data['city_id'];
        endif;
    }

    public function getCityId()
    {
        return $this->city_id;
    }

    //данные одного города
    public function getCityData($city_id = '')
    {


//city in $breadCityName
        $rout_path = $_GET['_route_'];
        $rout_array = (!empty($rout_path)) ? explode("/", $rout_path) : "";
        $city_from_url = (!empty($rout_array[0])) ? $rout_array[0] : "";
        $query_city = $this->db->query("SELECT * FROM " . DB_PREFIX . "cities "
            . "WHERE city_url = '" . $this->db->escape($city_from_url) . "' AND `status` = '1' LIMIT 1");
        $city_id = $query_city->row['id'] ? $query_city->row['id'] : "";
//city in $breadCityName

        if (empty($city_id)):
            $city_id = $this->city_id;
        endif;

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cities "
                . "WHERE id = '" . (int) $city_id . "' AND `status` = '1'");

        return $query->row;
    }

    //массив урлов всех городов
    public function citiesQueries()
    {
        $data = $this->cache->get('citiesQueries');

        if (empty($data)):
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cities WHERE `status` = '1'");

            $data = Array();

            foreach ($query->rows as $row):
                $data[$row['id']] = $row['city_url'];
            endforeach;

            $this->cache->set('citiesQueries', $data);

        endif;

        return $data;
    }

}

?>