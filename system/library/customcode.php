<?php
class CustomCode {
	
	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->cache = $registry->get('cache');
	}
	
	public function getCustomCodes()
	{
		$data = $this->cache->get('customHeadCodes');
		$data = '';
		if(empty($data)):
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_head_custom_code ");
			
			$data = Array();
			
			foreach($query->rows as $row):
				$hash_key = md5($row['site_url']);
				$data[$hash_key] = $row;
			endforeach;
			
			$this->cache->set('customHeadCodes',$data);
			
		endif;
		
		return $data;
	}
}