<?php
class CustomRedirect {
	
	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->session = $registry->get('session');
		$this->cache = $registry->get('cache');
	}
	
	public function getRedirects()
	{
		$data = $this->cache->get('customRedirects');
		$data = '';
		if(empty($data)):
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_redirect ");
			
			$data = Array();
			
			foreach($query->rows as $row):
				$hash_key = md5($row['url_from']);
				$data[$hash_key] = $row;
			endforeach;
			
			$this->cache->set('customRedirects',$data);
			
		endif;
		
		return $data;
	}
}