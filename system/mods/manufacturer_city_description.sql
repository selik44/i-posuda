CREATE TABLE `oc_manufacturer_city_description` (
  `city_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


ALTER TABLE `oc_manufacturer_city_description`
  ADD PRIMARY KEY (`city_id`,`manufacturer_id`,`language_id`);