<?php
class ModelCheckoutOneclick extends Model {
	public function add($product_id, $phone) {
$this->db->query("INSERT INTO `" . DB_PREFIX . "oneclick` SET product_id = '" . $this->db->escape($product_id) . "', phone = '" . $this->db->escape($phone) . "'"); 
	return $this->db->getLastId();
	}

    public function addOrder($data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '1', 
               firstname = 'Быстрый', "
            . "lastname = ' заказ', "
            . "surname = '', "
            . "order_status_id = '2', "
            . "email = 'oneclick@test.net', telephone = '+" . $this->db->escape($data['telephone']) . "', fax = '', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "', payment_firstname = '', payment_lastname = '', payment_company = '', payment_address_1 = '', payment_address_2 = '', payment_city = '', payment_postcode = '', payment_country = '', payment_country_id = '" . $this->db->escape($data['payment_country_id']) . "', payment_zone = '', payment_zone_id = '', payment_address_format = '', payment_custom_field = '" . $this->db->escape(isset($data['payment_custom_field']) ? json_encode($data['payment_custom_field']) : '') . "', payment_method = '', payment_code = '', shipping_firstname = '', shipping_lastname = '', shipping_company = '', shipping_address_1 = '', shipping_address_2 = '', 
            shipping_city = '', shipping_postcode = '', 
            shipping_country = '" . $this->db->escape($data['shipping_country']) . "', 
            shipping_country_id = '" . (int)$data['shipping_country_id'] . "', 
            shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', 
            shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', 
            shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', 
            shipping_custom_field = '" . $this->db->escape(isset($data['shipping_custom_field']) ? json_encode($data['shipping_custom_field']) : '') . "', 
            shipping_method = '" . $this->db->escape($data['shipping_method']) . "', 
            shipping_code = '" . $this->db->escape($data['shipping_code']) . "', 
            comment = '" . $this->db->escape($data['comment']) . "', 
            total = '" . (float)$data['total'] . "', 
            affiliate_id = '" . (int)$data['affiliate_id'] . "', 
            commission = '" . (float)$data['commission'] . "', 
            marketing_id = '" . (int)$data['marketing_id'] . "', 
            tracking = '" . $this->db->escape($data['tracking']) . "', 
            language_id = '" . (int)$data['language_id'] . "', 
            currency_id = '" . (int)$data['currency_id'] . "', 
            currency_code = '" . $this->db->escape($data['currency_code']) . "', 
            currency_value = '" . (float)$data['currency_value'] . "', 
            ip = '" . $this->db->escape($data['ip']) . "', 
            forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', 
            user_agent = '" . $this->db->escape($data['user_agent']) . "', 
            accept_language = '" . $this->db->escape($data['accept_language']) . "', 
            date_added = NOW(), date_modified = NOW()");

        $order_id = $this->db->getLastId();

        // Products
        if (isset($data['products'])) {
            foreach ($data['products'] as $product) {
                $product['price'] = ceil($product['price']);
                $product['total'] = $product['price'] * $product['quantity'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");

                $order_product_id = $this->db->getLastId();
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '0', product_option_value_id = '" . $this->db->escape($data['product_option_value_id']) . "', name = '" . $this->db->escape($data['quant']) . "', `value` = '" . $this->db->escape($data['value']) . "', `type` = 'radio'");
            }
        }

        // Gift Voucher
        $this->load->model('extension/total/voucher');

        // Vouchers
/*        if (isset($data['vouchers'])) {
            foreach ($data['vouchers'] as $voucher) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");

                $order_voucher_id = $this->db->getLastId();

                $voucher_id = $this->model_extension_total_voucher->addVoucher($order_id, $voucher);

                $this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
            }
        }*/

        // Totals

$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = 'sub_total', title = 'Общая сумма', `value` = '" . (float)$data['total'] . "', sort_order = '1'");
$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = 'total', title = 'Итого', `value` = '" . (float)$data['total'] . "', sort_order = '9'");

$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '1', firstname = 'Быстрый', lastname = ' заказ', email = 'oneclick@oneclick.net', telephone = '+ " . $this->db->escape($data['telephone']) . "', fax = '', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "', newsletter = '0', salt = '', password = '', status = '1', approved = '1', safe = '0', date_added = NOW()");

        return $order_id;
    }


}
