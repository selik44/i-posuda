<?php
class ModelAccountCustomer2 extends Model {
	public function update2Customer($data, $customer_id) {
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = token(9)) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "' WHERE customer_id = '". $customer_id ."'");

		return $customer_id;
	}
	public function update2Address($data) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address  WHERE address_id = '".(int)$data['address_id']."'");
        $newpost_type = !empty($query->row['newpost_type']) ? $query->row['newpost_type'] : $data['input-newpost-type'] ;

        $query_shipping_code = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer  WHERE customer_id = '".(int)$query->row['customer_id']."'");
        $shipping = !empty($query_shipping_code->row['shipping_code']) ? $query_shipping_code->row['shipping_code'] : "" ;
        $shipping_array = empty($shipping) ? explode(".", $data['shipping_method']) : ""; //new_post.new_post
        $shipping_ship = (!empty($shipping_array)  && !empty($shipping_array[0])) ? $shipping_array[0] : "";
        $shipping_code = !empty($shipping) ? $shipping : $shipping_ship;


        if (!empty($data['shipping_method']) && $data['shipping_method'] == "citylink.citylink") { $data['city'] = "Одесса"; }
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET shipping_code = '" . $shipping_code . "' WHERE customer_id = '". (int)$query->row['customer_id'] ."'");
        $this->db->query("UPDATE " . DB_PREFIX . "address SET newpost_type = '" . $newpost_type . "', address_order = '" . $this->db->escape($data['address_1']) . "', city_order = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "' WHERE address_id = '".(int)$data['address_id']."'");
	}
}
?>
