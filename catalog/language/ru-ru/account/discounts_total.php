<?php
// Heading
$_['heading_title']         = 'Накопительные скидки';

$_['text_account']      	= 'Личный кабинет';

$_['text_empty']            = 'Накопительных скидкок нет!';
$_['text_from']            = 'От';
$_['column_name']         = 'Тип';
$_['column_total']       = 'Сумма покупок';
$_['column_discount']       = 'Скидка';

$_['entry_discount']       = 'Ваша скидка:';
$_['entry_orders_total']       = 'Сумма заказов:';
$_['entry_next_level']       = 'До следующей скидки осталось:';
$_['entry_discount_table']       = 'Таблица скидок:';

