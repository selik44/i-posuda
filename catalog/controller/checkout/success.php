<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {
			$this->cart->clear();

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				if ($this->customer->isLogged()) {
					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
						'order_id'    => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);
				} else {
					$activity_data = array(
						'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
						'order_id' => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_guest', $activity_data);
				}
			}
				if ($this->customer->isLogged()) {
		$data['name'] = $this->customer->getFirstName(); } else {
		$data['name'] = $this->session->data['guest']['firstname'];
}
		$data['order_id'] = $this->session->data['order_id'];
        // get analytics info
            /*$this->load->model('checkout/order');
            $this->load->model('account/order');
            $this->load->model('catalog/product');
            $order_info = $this->model_checkout_order->getOrder($data['order_id']);

            if(!empty($order_info)):
                $transactionData = Array(
                    'id' => $order_info['order_id'].'-'.time(),
                    'affiliation' => 'iPosuda',
                    'revenue' => round($order_info['total'], 2),
                    'tax' => '0',
                    'shipping' => '0',
                    'currency' => 'UAH',
                    'products' => Array()
                );

                $produts = $this->model_account_order->getOrderProducts($data['order_id']);

                foreach ($produts as $product):
                    $product_category = 'Посуда';
                    $main_category = $this->model_catalog_product->getMainCategory($product['product_id']);
                    if(!empty($main_category)):
                        $product_category = addcslashes($main_category['name'],"'");
                    endif;

                    $order_quantity = $product['quantity'];
                    $quantity_option = $this->model_account_order->getOrderOptions($data['order_id'], $product['order_product_id']);

                    if(!empty($quantity_option)):
                        $quantity_option = array_shift($quantity_option);
                        $option_quantity = (int)$quantity_option['name'];

                        $order_quantity = $order_quantity * $option_quantity;
                    endif;

                    $transactionData['products'][] = Array(
                        'id' => $product['product_id'],
                        'sku' => $product['model'],
                        'name' => addcslashes($product['name'],"'"),
                        'price' => round($product['price'], 2),
                        'category' => $product_category,
                        'quantity' => $order_quantity,
                    );
                endforeach;

                $data['transactionData'] = $transactionData;

            endif;*/
            
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			//unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('account/download', '', true), $this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
        
        if (isset($this->session->data['order_id'])) {
            unset($this->session->data['order_id']);
        }

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}
