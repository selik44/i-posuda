<?php
class ControllerCheckoutShippingMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		$data['text_checkout_payment_address'] = $this->language->get('text_checkout_payment_address');
		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_your_address'] = $this->language->get('text_your_address');
		$data['text_your_password'] = $this->language->get('text_your_password');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_newsletter'] = sprintf($this->language->get('entry_newsletter'), $this->config->get('config_name'));
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
		$data['entry_shipping'] = $this->language->get('entry_shipping');

		if (isset($this->session->data['shipping_address']['postcode'])) {
			$data['postcode'] = $this->session->data['shipping_address']['postcode'];
		} else {
			$data['postcode'] = '';
		}



		if (isset($this->session->data['shipping_address']['zone_id'])) {
			$data['zone_id'] = $this->session->data['shipping_address']['zone_id'];
		} else {
			$data['zone_id'] = '';
		}
///
		$this->load->model('account/address');

		$addresses = $this->model_account_address->getAddresses();
		$data['addresses'] = $addresses;
		if (!empty($addresses)) {
		foreach ($addresses as $address) {
			$data['address_id'] =  $address['address_id'];
			$data['address_1'] = $address['address_1'];
$data['address_2'] = $address['address_2'];
$data['newpost_type'] = $address['newpost_type'] ? $address['newpost_type'] : "department";
			$data['postcode'] = $address['postcode'];
			$data['city'] = $address['city'];
		}
		} else {
			$data['address_id'] = '';
			$data['address_1'] = '';
			$data['city'] = '';
			$data['postcode'] = '';
$data['newpost_type'] = "department";
		}

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();

		if (!empty($addresses[$data['address_id']]['zone_id'])) {
			$data['zone_id'] = $addresses[$data['address_id']]['zone_id'];
		} else if (isset($this->session->data['shipping_address']['zone_id'])) {
			$data['zone_id'] = $this->session->data['shipping_address']['zone_id'];
		} else {
			$data['zone_id'] = '';
		}
		if (!empty($addresses[$data['address_id']]['country_id'])) {
			$data['country_id'] = $addresses[$data['address_id']]['country_id'];
		} else if (isset($this->session->data['shipping_address']['country_id'])) {
			$data['country_id'] = $this->session->data['shipping_address']['country_id'];
		} else {
			$data['country_id'] = 220;//$this->config->get('config_country_id');
		}
		$data['country_id'] = 220;

		$this->load->model('localisation/zone');
		$data['zones'] = $this->model_localisation_zone->getZonesByCountryId($data['country_id']);
		// Custom Fields
		$this->load->model('account/custom_field');

		$data['custom_fields'] = $this->model_account_custom_field->getCustomFields();

		if (isset($this->session->data['shipping_address'])) {
			// Shipping Methods
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('shipping');

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/shipping/' . $result['code']);

					$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

					if ($quote) {
						$method_data[$result['code']] = array(
							'title'      => $quote['title'],
							'quote'      => $quote['quote'],
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['shipping_methods'] = $method_data;
		}

		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['shipping_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['shipping_methods'])) {
			$data['shipping_methods'] = $this->session->data['shipping_methods'];
		} else {
			$data['shipping_methods'] = array();
		}

		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomer($this->customer->getID());
		if (isset($this->session->data['shipping_method']['code'])) {
			$data['code'] = $this->session->data['shipping_method']['code'];
		} elseif(isset($customer_info['shipping_code']) && !empty($customer_info['shipping_code'])) {
			$data['code'] = $customer_info['shipping_code'].'.'.$customer_info['shipping_code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

                $data['npCities'] = $this->newPostCity(true);

		$this->response->setOutput($this->load->view('checkout/shipping_method', $data));
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

			$this->load->model('account/customer');

			if($this->request->post['shipping_method'] != 'pickup.pickup'):
				if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
					$json['error']['address_1'] = $this->language->get('error_address_1');
				}

				if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
					$json['error']['city'] = $this->language->get('error_city');
				}
				$this->load->model('localisation/country');

				$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

				if ($this->request->post['country_id'] == '') {
					$json['error']['country'] = $this->language->get('error_country');
				}

				if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '' || !is_numeric($this->request->post['zone_id'])) {
					$json['error']['zone'] = $this->language->get('error_zone');
				}
			endif;

		// Validate if shipping is required. If not the customer should not have reached this page.
		if (!$this->cart->hasShipping()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate if shipping address has been set.
		if (!isset($this->session->data['shipping_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}
$data = array();
		if (!isset($this->request->post['shipping_method'])) {
			$json['error']['warning'] = $this->language->get('error_shipping');
		} else {
			$shipping = explode('.', $this->request->post['shipping_method']);

			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			}
		}
			if (!$json) {

		if (isset($this->request->post['address_1'])) {
		$data['address_1'] = $this->request->post['address_1'];
		} else {
		$data['address_1'] = '';
		}

		if (isset($this->request->post['address_2'])) {
		$data['address_2'] = $this->request->post['address_2'];
		} else {
		$data['address_2'] = '';
		}
		if (isset($this->request->post['postcode'])) {
		$data['postcode'] = $this->request->post['postcode'];
		} else {
		$data['postcode'] = '';
		}

		if (isset($this->request->post['city'])) {
		$data['city'] = $this->request->post['city'];
		} else {
		$data['city'] = '';
		}

		if (isset($this->request->post['zone_id'])) {
		$data['zone_id'] = $this->request->post['zone_id'];
		} else {
		$data['zone_id'] = '';
		}

		if (isset($this->request->post['country_id'])) {
		$data['country_id'] = $this->request->post['country_id'];
		} else {
		$data['country_id'] = '';
		}

			if ($this->request->post['address_id']) {
			$this->load->model('account/customer2');
			$this->model_account_customer2->update2Address($this->request->post);
			$this->load->model('account/address');

			$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());

			if (!empty($this->request->post['shipping_address'])) {
			$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}
			} else {
			$this->session->data['payment_address']['address_1'] = $this->request->post['address_1'];
			$this->session->data['payment_address']['address_2'] = $data['address_2'];
			$this->session->data['payment_address']['postcode'] = $this->request->post['postcode'];
			$this->session->data['payment_address']['city'] = $this->request->post['city'];
			$this->session->data['payment_address']['country_id'] = $this->request->post['country_id'];
			$this->session->data['payment_address']['zone_id'] = $this->request->post['zone_id'];

			$this->session->data['shipping_address']['address_1'] = $this->request->post['address_1'];
			$this->session->data['shipping_address']['address_2'] = $data['address_2'];
			$this->session->data['shipping_address']['postcode'] = $this->request->post['postcode'];
			$this->session->data['shipping_address']['city'] = $this->request->post['city'];
			$this->session->data['shipping_address']['country_id'] = $this->request->post['country_id'];
			$this->session->data['shipping_address']['zone_id'] = $this->request->post['zone_id'];

			$this->load->model('localisation/country');

			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

			if ($country_info) {
				$this->session->data['payment_address']['country'] = $country_info['name'];
				$this->session->data['payment_address']['iso_code_2'] = $country_info['iso_code_2'];
				$this->session->data['payment_address']['iso_code_3'] = $country_info['iso_code_3'];
				$this->session->data['payment_address']['address_format'] = $country_info['address_format'];
				$this->session->data['shipping_address']['country'] = $country_info['name'];
				$this->session->data['shipping_address']['iso_code_2'] = $country_info['iso_code_2'];
				$this->session->data['shipping_address']['iso_code_3'] = $country_info['iso_code_3'];
				$this->session->data['shipping_address']['address_format'] = $country_info['address_format'];
			} else {
				$this->session->data['payment_address']['country'] = '';
				$this->session->data['payment_address']['iso_code_2'] = '';
				$this->session->data['payment_address']['iso_code_3'] = '';
				$this->session->data['payment_address']['address_format'] = '';
				$this->session->data['shipping_address']['country'] = '';
				$this->session->data['shipping_address']['iso_code_2'] = '';
				$this->session->data['shipping_address']['iso_code_3'] = '';
				$this->session->data['shipping_address']['address_format'] = '';
			}

			if (isset($this->request->post['custom_field']['address'])) {
				$this->session->data['payment_address']['custom_field'] = $this->request->post['custom_field']['address'];
			} else {
				$this->session->data['payment_address']['custom_field'] = array();
			}

			$this->load->model('localisation/zone');

			$zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);

			if ($zone_info) {
				$this->session->data['payment_address']['zone'] = $zone_info['name'];
				$this->session->data['payment_address']['zone_code'] = $zone_info['code'];
				$this->session->data['shipping_address']['zone'] = $zone_info['name'];
				$this->session->data['shipping_address']['zone_code'] = $zone_info['code'];
			} else {
				$this->session->data['payment_address']['zone'] = '';
				$this->session->data['payment_address']['zone_code'] = '';
				$this->session->data['shipping_address']['zone'] = '';
				$this->session->data['shipping_address']['zone_code'] = '';
			}
			}
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);

			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function newPostCity($array = false)
	{
		$json = Array();

		if(file_exists(DIR_DOWNLOAD.'np_data.txt')):

			$cities = $this->cache->get('newpost.city_list');

			if(!$cities):
				$np_data = file_get_contents(DIR_DOWNLOAD.'np_data.txt');
				$np_data = json_decode($np_data);

				$cities = Array();
				foreach($np_data->response as $data):
					$cities[$data->city_ref] = $data->cityRu;
				endforeach;

				asort($cities);
				$this->cache->set('newpost.city_list',$cities);
			endif;

                        if($array) {
                            return $cities;
                        }

			if(!empty($cities)):
				$search_string = '';
				if(isset($this->request->post['city_name'])):
					$search_string = $this->request->post['city_name'];
				endif;

				$result_array = array_filter($cities , function($var) use($search_string){
					return preg_match("/" . quotemeta(mb_strtolower($search_string)) . "/i", mb_strtolower($var)) == 1;
				});

				$json = $result_array;
			endif;

		endif;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function newPostWarehous()
	{
		$json = Array();

		if(file_exists(DIR_DOWNLOAD.'np_data.txt')):
			$warehouses = $this->cache->get('newpost.warehous_list');

			if(!$warehouses):

				$np_data = file_get_contents(DIR_DOWNLOAD.'np_data.txt');
				$np_data = json_decode($np_data);

				$warehouses = Array();
				foreach($np_data->response as $data):
					$address ='Отделение №'.$data->number. (!empty($data->addressRu) ? ' - '.$data->addressRu  : '' );
					$warehouses[$data->ref] = Array(
						'address' => $address,
						'number' => $data->number,
						'city_name' => $data->cityRu,
					);
				endforeach;

				usort($warehouses, function($a, $b) {
					return $a['number'] - $b['number'];
				});

				$this->cache->set('newpost.warehous_list',$warehouses);
			endif;

			if(!empty($warehouses)):
				$search_data = Array();

				if(isset($this->request->post['city_ref'])):
					$search_data['city_ref']= $this->request->post['city_ref'];
				endif;

				$result_array = Array();

				if(isset($this->request->post['city_name']) && !empty($this->request->post['city_name'])):

					$search_data['city_name']= $this->request->post['city_name'];

					$result_array = array_filter($warehouses , function($var) use($search_data){
						$is_filter = true;

						if(isset($search_data['address'])):
							$is_filter *= preg_match("/" . quotemeta(mb_strtolower($search_data['address'])) . "/i", mb_strtolower($var['address'])) == 1;
						endif;

						$is_filter *= preg_match("/^" . quotemeta(mb_strtolower($search_data['city_name'])) . "/i", mb_strtolower($var['city_name'])) == 1;

						/*if(isset($search_data['city_name']) && !empty($search_data['city_name'])):

						endif;*/

						return $is_filter;
					});

					array_splice($result_array, 300);
					$json = $result_array;

				endif;
			endif;

		endif;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
