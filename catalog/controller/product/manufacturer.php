<?php
class ControllerProductManufacturer extends Controller {
    public function index() {

        $this->load->language('product/manufacturer');

        $this->load->model('catalog/manufacturer');

        $this->load->model('tool/image');

        //get canonical link
        $this->getCanonicalLink();

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $cities = [
            'kharkov' => 'Харьков',
            'odessa'  => 'Одесса',
            'dnepr'   => 'Днепр',
            'opt'     => 'Опт'
        ];
        $city_name = null;
        $city_key= explode('/', $_REQUEST['_route_'])[0];
        foreach ($cities as $key => $city) {
            if($key == $city_key) {
                $city_name =  ' '.$city;
            }
        }

        $html_h1 = $this->language->get('heading_title');
        $meta_title = $this->language->get('heading_title');
        $meta_description = '';
        $seo_text = '';

        $brands_lang_data = $this->config->get('config_city_brands_langdata');
        if(isset($brands_lang_data[$this->config->get('config_language_id')])):
            $this->load->library('city');

            $city_id = $this->city->getCityId();
            if(empty($city_id)):
                $city_id = 0;
            endif;

            if(isset($brands_lang_data[$this->config->get('config_language_id')][$city_id])):
                $brands_lang_data = $brands_lang_data[$this->config->get('config_language_id')][$city_id];

                $html_h1 = $brands_lang_data['html_h1'];
                $meta_title = $brands_lang_data['meta_title'];
                $meta_description = $brands_lang_data['meta_description'];
                $seo_text = html_entity_decode($brands_lang_data['description']);
            endif;
        endif;

        $this->document->setTitle($meta_title);
        $this->document->setDescription($meta_description);
        $data['heading_title'] = $html_h1;
        $data['seo_text'] = $seo_text;


        $html_h1 = $this->language->get('heading_title');
        $meta_title = $this->language->get('heading_title');
        $meta_description = '';
        $seo_text = '';

        $brands_lang_data = $this->config->get('config_city_brands_langdata');
        if(isset($brands_lang_data[$this->config->get('config_language_id')])):
            $this->load->library('city');

            $city_id = $this->city->getCityId();
            if(empty($city_id)):
                $city_id = 0;
            endif;

            if(isset($brands_lang_data[$this->config->get('config_language_id')][$city_id])):
                $brands_lang_data = $brands_lang_data[$this->config->get('config_language_id')][$city_id];

                $html_h1 = $brands_lang_data['html_h1'];
                $meta_title = $brands_lang_data['meta_title'];
                $meta_description = $brands_lang_data['meta_description'];
                $seo_text = html_entity_decode($brands_lang_data['description']);
            endif;
        endif;

        $this->document->setTitle($meta_title);
        $this->document->setDescription($meta_description);
        $data['heading_title'] = $html_h1;
        $data['seo_text'] = $seo_text;

        $data['text_index'] = $this->language->get('text_index');
        $data['text_empty'] = $this->language->get('text_empty');

        $data['button_continue'] = $this->language->get('button_continue');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_brand'),
            'href' => $this->url->link('product/manufacturer')
        );

        $data['categories'] = array();

        $results = $this->model_catalog_manufacturer->getManufacturers();

        foreach ($results as $result) {
            $name = $result['name'];

            if (is_numeric(utf8_substr($name, 0, 1))) {
                $key = '0 - 9';
            } else {
                $key = utf8_substr(utf8_strtoupper($name), 0, 1);
            }

            if (!isset($data['categories'][$key])) {
                $data['categories'][$key]['name'] = $key;
            }

            if ($city_name) {
                $data['categories'][$key]['manufacturer'][] = array(
                    'name' => $name,
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
                );
            } else {
                $data['categories'][$key]['manufacturer'][] = array(
                    'name' => $name,
                    'href' => $this->url->linkFormatted('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']."&city_id=1")
                );
            }

        }

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $this->load->model( 'module/mega_filter' );

        $data = $this->model_module_mega_filter->prepareData( $data );


        $this->load->model( 'module/mega_filter' );

        $data = $this->model_module_mega_filter->prepareData( $data );

        $this->response->setOutput($this->load->view('product/manufacturer_list', $data));
    }

    public function info() {

        $this->load->language('product/manufacturer');

        $this->load->model('catalog/manufacturer');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

//===========
        $cities = [
            'kharkov' => 'Харьков',
            'odessa'  => 'Одесса',
            'dnepr'   => 'Днепр',
            'opt'    => 'Опт'
        ];
        $city_name = '';
        $city_key= explode('/', $_REQUEST['_route_'])[0];
        foreach ($cities as $key => $city) {
            if($key == $city_key) {
                $city_name =  ' '.$city;
            }
        }

        if ($city_name == '') {
            unset($this->session->data['city_id']);
        }

//===========
        if (isset($this->request->get['manufacturer_id'])) {
            $manufacturer_id = (int)$this->request->get['manufacturer_id'];
        } else {
            $manufacturer_id = 0;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
        } else {
            $limit = (int)$this->config->get($this->config->get('config_theme') . '_product_limit');
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_brand'),
            'href' => $this->url->link('product/manufacturer')
        );

        $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

        //получаем мета-данные производителя для текущего города
        $this->load->library('city');
        $manufacturer_city_info = $this->model_catalog_manufacturer->getManufacturerCityDescription($manufacturer_id, $this->city->getCityId());


        //получаем мета-данные производителя для текущего города
        $this->load->library('city');
        $manufacturer_city_info = $this->model_catalog_manufacturer->getManufacturerCityDescription($manufacturer_id, $this->city->getCityId());


        if ($manufacturer_info) {
            $this->document->setTitle($manufacturer_info['name']);

            $url = '';

            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $manufacturer_info['name'],
                'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
            );


            //при необходимости заменяем мета-данные категории по умолчанию на указаные для текущего города
            if(!empty($manufacturer_city_info)):
                // $manufacturer_info['description'] = $manufacturer_city_info['description'];
                // $manufacturer_info['meta_title'] = $manufacturer_city_info['meta_title'];
                // $manufacturer_info['meta_h1'] = $manufacturer_city_info['meta_h1'];
                // $manufacturer_info['meta_description'] = $manufacturer_city_info['meta_description'];
            elseif($this->city->getCityId()):
                $manufacturer_info['description'] = '';
            endif;

            if($manufacturer_info['meta_title'] || $manufacturer_city_info['meta_title'] != '') {
                if(!empty($manufacturer_city_info)){
                    $meta_title =  $manufacturer_city_info['meta_title'];
                }else{
                    $meta_title = $manufacturer_info['meta_title'];
                }
            } else {
                    $meta_title = "Посуда " . $manufacturer_info['name'] . $city_name . ": купить в интернет магазине iPosuda по выгодным ценам!";
            }

            $this->document->setTitle($meta_title);


            if(!empty($page) && $page > 1) {
                $meta_title = "Страница ".$page." - ".$meta_title;
                $this->document->setTitle($meta_title);
            }
            if($manufacturer_info['meta_description'] || $manufacturer_city_info['description'] != '') {
//        $meta_description = $manufacturer_info['meta_description'];
                if(!empty($manufacturer_city_info)){
                    $meta_description =  $manufacturer_city_info['meta_description'];
                }else{
//              $meta_title = $manufacturer_info['meta_title'];
                    $meta_description = $manufacturer_info['meta_description'];
                }
            } else {
                $meta_description = "❶В НАЛИЧИИ 🍽️ ПОСУДА " . mb_strtoupper($manufacturer_info['name'], 'utf-8') . "" . mb_strtoupper($city_name, 'utf-8') . "➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55";
            }

            $this->document->setDescription($meta_description);

            $this->document->setKeywords($manufacturer_info['meta_keyword']);

//        if($manufacturer_info['meta_description'] || !empty($manufacturer_city_info['description'])) {
////        $meta_description = $manufacturer_info['meta_description'];
//            if(!empty($manufacturer_city_info)){
//                $meta_description =  $manufacturer_city_info['description'];
//            }else{
////              $meta_title = $manufacturer_info['meta_title'];
//                $meta_description = $manufacturer_info['meta_description'];
//            }
//        } else {
//            $meta_description = "❶В НАЛИЧИИ 🍽️ ПОСУДА " . mb_strtoupper($manufacturer_info['name'], 'utf-8') . "" . mb_strtoupper($city_name, 'utf-8') . "➔ ❷ Огромный каталог посуды ✓ Лучшие цены ✓ Большой ассортимент ✈ Быстрая доставка ❸ ☎ Звоните +38 (067) 481 02 09 +38 (099) 603 26 55";
//        }

            if ($manufacturer_info['meta_h1'] || $manufacturer_city_info['meta_h1'] != '') {
//        $data['heading_title'] = $manufacturer_info['meta_h1'];
                if(!empty($manufacturer_city_info['meta_h1'])){
                    $data['heading_title'] =  $manufacturer_city_info['meta_h1'];
                }else{
                    $data['heading_title'] = $manufacturer_info['meta_h1'];
                }
            } else {
                $data['heading_title'] = 'Посуда '.$manufacturer_info['name'].$city_name;
            }


            if ($manufacturer_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($manufacturer_info['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
                $this->document->setOgImage($data['thumb']);
            } else {
                $data['thumb'] = '';
            }

            $data['description'] = html_entity_decode($manufacturer_info['description'], ENT_QUOTES, 'UTF-8');

            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_quantity'] = $this->language->get('text_quantity');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_price'] = $this->language->get('text_price');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_list'] = $this->language->get('button_list');
            $data['button_grid'] = $this->language->get('button_grid');

            $data['compare'] = $this->url->link('product/compare');

            $data['products'] = array();

            $filter_data = array(
                'filter_manufacturer_id' => $manufacturer_id,
                'sort'                   => $sort,
                'order'                  => $order,
                'start'                  => ($page - 1) * $limit,
                'limit'                  => $limit
            );


            $filter_data['mfp_overwrite_path'] = true;


            $filter_data['mfp_overwrite_path'] = true;

            $product_total = $this->model_catalog_product->getTotalProducts($filter_data);

            $results = $this->model_catalog_product->getProducts($filter_data);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }


                $options = array();
                $speciales = $this->model_catalog_product->getProductSpeciales($result['product_id']);
                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                    $product_option_value_data = array();

                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $opt_price = $this->currency->format($this->tax->calculate(($option_value['price']+$option_value['price']*$result['markup']/100), $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $opt_price = false;
                            }
                            if ((float)$option_value['special']) {
                                $opt_special = $this->currency->format($this->tax->calculate(($option_value['special']+$option_value['special']*$result['markup']/100), $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $opt_special = false;
                            }
                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id'         => $option_value['option_value_id'],
                                'name'                    => $option_value['name'],
                                'image'                   => $option_value['image'] ? $this->model_tool_image->resize($option_value['image'], 50, 50) : '',
                                'price'                   => $opt_price,
                                'see'            => $option_value['see'],
                                'status'            => $option_value['status'],
                                'special'		  => $opt_special,
                                'quant'        		  => $option_value['quant'],
                                'price_prefix'            => $option_value['price_prefix']
                            );
                        }
                    }

                    $options[] = array(
                        'product_option_id'    => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id'            => $option['option_id'],
                        'name'                 => $option['name'],
                        'type'                 => $option['type'],
                        'value'                => $option['value'],
                        'required'             => $option['required']
                    );
                }


                $fmSettings = $this->config->get('mega_filter_settings');

                if( ! empty( $fmSettings['not_remember_filter_for_products'] ) && false !== ( $mfpPos = strpos( $url, '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' ) ) ) {
                    $mfUrlBeforeChange = $url;
                    $mfSt = mb_strpos( $url, '&', $mfpPos+1, 'utf-8');
                    $url = $mfSt === false ? '' : mb_substr($url, $mfSt, mb_strlen( $url, 'utf-8' ), 'utf-8');
                }


                $options = array();
                $speciales = $this->model_catalog_product->getProductSpeciales($result['product_id']);
                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                    $product_option_value_data = array();

                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $opt_price = $this->currency->format($this->tax->calculate(($option_value['price']+$option_value['price']*$result['markup']/100), $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $opt_price = false;
                            }
                            if ((float)$option_value['special']) {
                                $opt_special = $this->currency->format($this->tax->calculate(($option_value['special']+$option_value['special']*$result['markup']/100), $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $opt_special = false;
                            }
                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id'         => $option_value['option_value_id'],
                                'name'                    => $option_value['name'],
                                'image'                   => $option_value['image'] ? $this->model_tool_image->resize($option_value['image'], 50, 50) : '',
                                'price'                   => $opt_price,
                                'see'            => $option_value['see'],
                                'status'            => $option_value['status'],
                                'special'		  => $opt_special,
                                'quant'        		  => $option_value['quant'],
                                'price_prefix'            => $option_value['price_prefix']
                            );
                        }
                    }

                    $options[] = array(
                        'product_option_id'    => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id'            => $option['option_id'],
                        'name'                 => $option['name'],
                        'type'                 => $option['type'],
                        'value'                => $option['value'],
                        'required'             => $option['required']
                    );
                }


                $fmSettings = $this->config->get('mega_filter_settings');

                if( ! empty( $fmSettings['not_remember_filter_for_products'] ) && false !== ( $mfpPos = strpos( $url, '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' ) ) ) {
                    $mfUrlBeforeChange = $url;
                    $mfSt = mb_strpos( $url, '&', $mfpPos+1, 'utf-8');
                    $url = $mfSt === false ? '' : mb_substr($url, $mfSt, mb_strlen( $url, 'utf-8' ), 'utf-8');
                }

                $data['products'][] = array(
                    'product_id'  => $result['product_id'],
                    'thumb'       => $image,
                    'name'        => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'speciales'     => $speciales,

                    'speciales'     => $speciales,

                    'special'     => $special,
                    'tax'         => $tax,
                    'minimum'     => ($result['minimum'] > 0) ? $result['minimum'] : 1,
                    'rating'      => $rating,
                    'options' => $options,
                    'options' => $options,
                    'href'        => $this->url->link('product/product', 'manufacturer_id=' . $result['manufacturer_id'] . '&product_id=' . $result['product_id'] . $url),
                    'info'        => $this->getProductRatings($result['product_id'])
                );
            }

            $url = '';

            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_default'),
                'value' => 'p.sort_order-ASC',
                'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.sort_order&order=ASC' . $url)
            );

            /*$data['sorts'][] = array(
              'text'  => $this->language->get('text_name_asc'),
              'value' => 'pd.name-ASC',
              'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=pd.name&order=ASC' . $url)
            );

            $data['sorts'][] = array(
              'text'  => $this->language->get('text_name_desc'),
              'value' => 'pd.name-DESC',
              'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=pd.name&order=DESC' . $url)
            );*/

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price&order=DESC' . $url)
            );

            /*if ($this->config->get('config_review_status')) {
              $data['sorts'][] = array(
                'text'  => $this->language->get('text_rating_desc'),
                'value' => 'rating-DESC',
                'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=rating&order=DESC' . $url)
              );

              $data['sorts'][] = array(
                'text'  => $this->language->get('text_rating_asc'),
                'value' => 'rating-ASC',
                'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=rating&order=ASC' . $url)
              );
            }

            $data['sorts'][] = array(
              'text'  => $this->language->get('text_model_asc'),
              'value' => 'p.model-ASC',
              'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.model&order=ASC' . $url)
            );

            $data['sorts'][] = array(
              'text'  => $this->language->get('text_model_desc'),
              'value' => 'p.model-DESC',
              'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.model&order=DESC' . $url)
            );*/

            $url = '';

            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

            sort($limits);

            foreach($limits as $value) {
                $data['limits'][] = array(
                    'text'  => $value,
                    'value' => $value,
                    'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&limit=' . $value)
                );
            }

            $url = '';

            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            //показывать ли карточку след. страницы
            $data['show_next_pagelink'] = false;
            if(($page * $limit) < $product_total):
                $data['show_next_pagelink'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] .  $url . '&page='. (int)($page + 1));
            endif;

            $pagination = new Pagination();
            $pagination->is_front = true;
            $pagination->is_front = true;
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] .  $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
            $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'], true), 'canonical');
            if ($page == 1) {

            } elseif ($page == 2) {
                $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'], true), 'prev');
            } else {
                $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&page='. ($page - 1), true), 'prev');
            }

            if ($limit && ceil($product_total / $limit) > $page) {
                $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&page='. ($page + 1), true), 'next');
            }

            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;

            $data['continue'] = $this->url->link('common/home');

            /****************** Каталог брендов *******************************************************************/
            $this->load->model('catalog/product');
            $this->load->model('catalog/category');

            $data['categories_brand'] = array();

            $categories = $this->model_catalog_category->getCategories(0);

            foreach ($categories as $category) {

                $children_data = array();

                $children = $this->model_catalog_category->getCategoriesManufacturers($category['category_id'], $manufacturer_id);

                $category_info = $this->model_catalog_category->getCategory($category['category_id']);
                if ($category_info) {
                    if ($category_info['image']) {
                        $thumb = $this->model_tool_image->resize($category_info['image'], 28, 28);
                    } else {
                        $thumb = '';
                    }
                }
                $breadCity = $this->city->getCityData();

                $breadCityName = ' в Киеве';
                if ($breadCity) {
                    switch ($breadCity['city_name']):
                        case 'Одесса':
                            $breadCityName = ' Одесса';
                            break;
                        case 'Харьков':
                            $breadCityName = ' Харьков';
                            break;
                        case 'Днепр':
                            $breadCityName = ' Днепр';
                            break;
                        case 'Опт':
                            $breadCityName = ' оптом';
                            break;
                    endswitch;
                }
                foreach ($children as $child) {
                    $children_info = $this->model_catalog_category->getCategory($child['category_id']);
                    if ($children_info) {
                        if ($children_info['image']) {
                            $thumb_child = $this->model_tool_image->resize($children_info['image'], 28, 28);
                        } else {
                            $thumb_child = '';
                        }
                    }
                    $title = $child['name'] . " " . $manufacturer_info["name"] . $breadCityName;
                    $href_url_children = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);
                    $href_url_children = $href_url_children . mb_strtolower($manufacturer_info["name"]) . "/";
                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name' => $child['name'],
                        'thumb' => $thumb_child,
                        'href' => $href_url_children,
                        'title' => $title,
                    );
                }

                if (!empty($children_data) && count($children_data) > 0) {
                    $title = $category['name'] . " " . $manufacturer_info["name"] .  $breadCityName;
                    $href_url = $this->url->link('product/category', 'path=' . $category['category_id']);
                    $href_url = $href_url . mb_strtolower($manufacturer_info["name"]) . "/";
                    $data['categories_brand'][] = array(
                        'category_id' => $category['category_id'],
                        'name' => $category['name'],
                        'thumb' => $thumb,
                        'children' => $children_data,
                        'href' => $href_url,
                        'title' => $title,
                        'column' => $category['column'] ? $category['column'] : 1,
                        'top' => $category['top']
                    );
                }
            }

            /******************** Каталог брендов *****************************************************************/


            $data['min_max_prices'] = $this->model_catalog_product->minMaxCategoryPrice(['manufacturer_id' => $this->request->get['manufacturer_id']]);

            $total_category_reviews = $this->model_catalog_review->getLastCategoryProductsRewiews(['manufacturer_id' => $this->request->get['manufacturer_id']]);

            $data['products_total_count'] = $product_total;

            $data['total_category_reviews'] = count($total_category_reviews);

            $data['average_category_rating'] = 0;

            foreach ($total_category_reviews as $review) {
                $data['average_category_rating'] += $review['rating'];
            }

            $data['average_category_rating'] /= $data['total_category_reviews'];

            $data['average_category_rating'] = $data['average_category_rating'] == round($data['average_category_rating']) ? $data['average_category_rating'] : round($data['average_category_rating'], 1);

            $data['manufacturer_name'] = $manufacturer_info['name'];

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');


            $this->load->model( 'module/mega_filter' );

            $data = $this->model_module_mega_filter->prepareData( $data );


            $this->load->model( 'module/mega_filter' );

            $data = $this->model_module_mega_filter->prepareData( $data );

            $this->response->setOutput($this->load->view('product/manufacturer_info', $data));
        } else {
            $url = '';

            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if( $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp') ) {
                $url .= '&'.($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp').'=' . $this->rgetMFP($this->config->get('mfilter_url_param')?$this->config->get('mfilter_url_param'):'mfp');
            }


            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/manufacturer/info', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['header'] = $this->load->controller('common/header');
            $data['footer'] = $this->load->controller('common/footer');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');


            $this->load->model( 'module/mega_filter' );

            $data = $this->model_module_mega_filter->prepareData( $data );


            $this->load->model( 'module/mega_filter' );

            $data = $this->model_module_mega_filter->prepareData( $data );

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    public function getProductRatings($poduct_id) {
        $this->load->model('catalog/review');

        $data['reviews'] = array();

        $review_total = $this->model_catalog_review->getTotalReviewsByProductId($poduct_id);

        $results = $this->model_catalog_review->getReviewsByProductId($poduct_id);


        $rating_arr = [];

        foreach ($results as $result) {
            $rating_arr[] = (int)$result['rating'];
        }

        $data['reviews']['review_text'] = '';
        $data['reviews']['review_text_null'] = 'Оставить отзыв';
        if ($review_total == 1) {
            $data['reviews']['review_text'] = 'отзыв';
        } elseif ($review_total > 1 && $review_total < 5) {
            $data['reviews']['review_text'] = 'отзыва';
        } else {
            $data['reviews']['review_text'] = 'отзывов';
        }

        $data['reviews']['average_rating'] = ceil(array_sum($rating_arr) / $review_total);
        $data['reviews']['total_reviews'] = $review_total;

        return $data['reviews'];
    }

    public function getCanonicalLink()
    {
        $this->document->addLink($this->url->link('product/manufacturer', '', true), 'canonical');
    }
}
