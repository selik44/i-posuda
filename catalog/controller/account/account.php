<?php
class ControllerAccountAccount extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', true);
		$data['password'] = $this->url->link('account/password', '', true);
		$data['address'] = $this->url->link('account/address', '', true);
		
		$data['credit_cards'] = array();
		
		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');
		
		foreach ($files as $file) {
			$code = basename($file, '.php');
			
			if ($this->config->get($code . '_status') && $this->config->get($code . '_card')) {
				$this->load->language('extension/credit_card/' . $code);

				$data['credit_cards'][] = array(
					'name' => $this->language->get('heading_title'),
					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				);
			}
		}
		
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		
		
		$data['return'] = $this->url->link('account/return', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['recurring'] = $this->url->link('account/recurring', '', true);
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        $customer_info = $this->model_account_customer->getCustomer($this->customer->getID());
        if (isset($this->request->post['surname'])) {
            $data['surname'] = $this->request->post['surname'];
        } elseif (!empty($customer_info)) {
            $data['surname'] = $customer_info['surname'];
        } else {
            $data['surname'] = '';
        }
        $data['npCities'] = $this->newPostCity(true);

        $address_info2 = Array();
        if(isset($customer_info['address_id'])):
            $this->load->model('account/address');
            $address_info2 = $this->model_account_address->getAddress($customer_info['address_id']);
        endif;

        if (isset($this->request->post['input-newpost-type'])) {
            $data['input_newpost_type'] = $this->request->post['input-newpost-type'];
        } elseif(!empty($address_info2)) {
            $data['input_newpost_type'] = $address_info2['newpost_type'];
        } else {
            $data['input_newpost_type'] = 'department';
        }
        $data['input_newpost_type'] = $data['input_newpost_type'] ? $data['input_newpost_type'] : "department";

        if (isset($this->request->post['city-np'])) {
            $data['city_np'] = $this->request->post['city-np'];
        } elseif(!empty($address_info2)) {
            $data['city_np'] = $address_info2['address_2'];
        } else {
            $data['city_np'] = '';
        }


        if (isset($this->request->post['shipping-custom-address'])) {
            $data['shipping_custom_address'] = $this->request->post['shipping-custom-address'];
        } elseif(!empty($address_info2)) {
            $data['shipping_custom_address'] = $address_info2['address_1'];
        } else {
            $data['shipping_custom_address'] = '';
        }


        if (isset($this->request->post['telephone_operator_code'])) {
            $data['telephone_operator_code'] =  $this->request->post['telephone_operator_code'];
        } elseif (!empty($customer_info)) {
            $data['telephone_operator_code'] =  $customer_info['telephone'];
        } else {
            $data['telephone_operator_code'] = '';
        }
        $this->response->setOutput($this->load->view('account/account', $data));
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    public function newPostCity($array = false)
    {
        $json = Array();

        if(file_exists(DIR_DOWNLOAD.'np_data.txt')):

            $cities = $this->cache->get('newpost.city_list');

            if(!$cities):
                $np_data = file_get_contents(DIR_DOWNLOAD.'np_data.txt');
                $np_data = json_decode($np_data);

                $cities = Array();
                foreach($np_data->response as $data):
                    $cities[$data->city_ref] = $data->cityRu;
                endforeach;

                asort($cities);
                $this->cache->set('newpost.city_list',$cities);
            endif;

            if($array) {
                return $cities;
            }

            if(!empty($cities)):
                $search_string = '';
                if(isset($this->request->post['city_name'])):
                    $search_string = $this->request->post['city_name'];
                endif;

                $result_array = array_filter($cities , function($var) use($search_string){
                    return preg_match("/" . quotemeta(mb_strtolower($search_string)) . "/i", mb_strtolower($var)) == 1;
                });

                $json = $result_array;
            endif;

        endif;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
