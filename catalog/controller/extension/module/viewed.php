<?php
class ControllerExtensionModuleViewed extends Controller {
  public function index($setting) {
    
    $this->load->language('extension/module/viewed');
    
    $data['heading_title'] = $this->language->get('heading_title');
    
    $data['text_tax'] = $this->language->get('text_tax');
    
    $data['button_cart'] = $this->language->get('button_cart');
    $data['button_wishlist'] = $this->language->get('button_wishlist');
    $data['button_compare'] = $this->language->get('button_compare');
    
    $this->load->model('catalog/product');
    
    $this->load->model('tool/image');
    
    $data['products'] = array();
    
    if (!$setting['limit']) {
      $setting['limit'] = 3;
    }
    $viewed_array = $this->session->data['viewed'];
    
    $viewed_products =  isset($viewed_array) ? array_reverse(array_slice($viewed_array, -$setting['limit'])) : [];

    if (!empty($viewed_products)) {
      
      foreach ($viewed_products as $product_id) {
        $product_info = $this->model_catalog_product->getProduct($product_id);
        
        if ($product_info) {
          if ($product_info['image']) {
            $image = $this->model_tool_image->resize($product_info['image'], 228, 228);
          } else {
            $image = $this->model_tool_image->resize('placeholder.png', 228, 228);
          }
          
          if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
          } else {
            $price = false;
          }
          
          if ((float)$product_info['special']) {
            $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
          } else {
            $special = false;
          }
          
          if ($this->config->get('config_tax')) {
            $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
          } else {
            $tax = false;
          }
          
          if ($this->config->get('config_review_status')) {
            $rating = $product_info['rating'];
          } else {
            $rating = false;
          }
          
          foreach ($this->model_catalog_product->getProductOptions($product_id) as $option) {
            $product_option_value_data = array();
      
            foreach ($option['product_option_value'] as $option_value) {
              if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                $real_price = $option_value['price'] + ($option_value['price'] * ($product_info['markup'] / 100));
                
                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                  $price = $this->currency->format($this->tax->calculate($real_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                } else {
                  $price = false;
                }

                $product_option_value_data[] = array(
                  'product_option_value_id' => $option_value['product_option_value_id'],
                  'option_value_id'         => $option_value['option_value_id'],
                  'name'                    => $option_value['name'],
                  'image'                   => $option_value['image'] ? $this->model_tool_image->resize($option_value['image'], 50, 50) : '',
                  'price'                   => $price,
                  'price_prefix'            => $option_value['price_prefix'],
                  'status'                  => $option_value['status'],
                  'see'                     => $option_value['see'],
                  'quant'                   => $option_value['quant']
                );
              }
            }

            $options[0] = array(
              'product_option_value' => $product_option_value_data,
              'option_id'            => $option['option_id'],
              'product_option_id'    => $option['product_option_id'],
              'name'                 => $option['name'],
              'type'                 => $option['type'],
              'value'                => $option['value'],
              'required'             => $option['required']
            );
          }
          
          $data['products'][] = array(
            'product_id'  => $product_info['product_id'],
            'thumb'       => $image,
            'name'        => $product_info['name'],
            'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
            'price'       => $price,
            'special'     => $special,
            'tax'         => $tax,
            'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
            'rating'      => $rating,
            'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
            'youtube'       => $product_info['youtube'],
            'options'     => $options
          );
        }
      }
    }
    if ($data['products']) {
      return $this->load->view('extension/module/viewed', $data);
    }
  }
}
