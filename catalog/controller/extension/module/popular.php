<?php
class ControllerExtensionModulePopular extends Controller {
  public function index($setting) {
    $this->load->language('extension/module/popular');

    $this->load->model('catalog/product');
    
    $this->load->model('tool/image');

    $data['products'] = array();
    $setting = [
      'limit' => 5,
      'width' => 100,
      'height'=> 100
    ];
  
    $parts = explode('_', (string)$this->request->get['path']);
  
    $category_id = (int)array_pop($parts);
   
    $results = $this->model_catalog_product->getPopularProductsByCategoryId($category_id, 5);
  
    $filter_data = array(
      'filter_category_id' => $category_id,
      'sort'               => 'p.date.added',
      'order'              => 'DESC',
      'limit'              => 5
    );
    
    if (!$results) {
      $results = $this->model_catalog_product->getProducts($filter_data);
    }
    
    if ($results) {
      
      foreach ($results as $result) {
        if ($result['image']) {
          $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'], $result['watermark']);
        } else {
          $image = $this->model_tool_image->resize('no_image.png', $setting['width'], $setting['height']);
        }
        
        $data['products'][] = array(
          'product_id'       => $result['product_id'],
          'thumb'            => $image,
          'name'             => $result['name'],
          'model'            => $result['model'],
          'href'             => $this->url->link('product/product', 'product_id=' . $result['product_id']),
        );
      }

      return $this->load->view('extension/module/popular', $data);
    }
  }
}
