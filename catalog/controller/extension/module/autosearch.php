<?php
########################################################
#    AutoSearch 1.10 for Opencart 2.3.0.x by AlexDW    #
########################################################
class ControllerExtensionModuleAutosearch extends Controller {

	public function ajax_asr()
	{
	function get_sum($stra)
  {
   $var1 = array_flip(get_html_translation_table());
   $stra = strtr($stra, $var1);
   $stra = strip_tags($stra);

    return $stra;
  }

		$data = array();
		$this->NOW = date('Y-m-d H:i') . ':00';
		if( isset($this->request->get['keyword']) ) {

			$keywords = mb_strtolower(( $this->request->get['keyword'] ), 'UTF-8');

			if( ((mb_strlen(($keywords), 'UTF-8')) >= ($this->config->get('autosearch_symbol'))) AND ((($this->config->get('autosearch_status')) > 0)) ) {
				$parts = explode( ' ', $keywords );
				$add = '';

			if (($this->config->get('autosearch_asr_image')) <>'' ) {
				$image_width = $this->config->get('autosearch_asr_image');
				$image_height = $this->config->get('autosearch_asr_image');
			} else  { 
				$image_width = $this->config->get('config_image_cart_width');
				$image_height = $this->config->get('config_image_cart_height');
			}

				foreach( $parts as $part ) {
					$add .= ' AND (LOWER(pd.name) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_model')) > 0 ) $add .= ' OR LOWER(p.model) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_sku')) > 0 ) $add .= ' OR LOWER(p.sku) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_upc')) > 0 ) $add .= ' OR LOWER(p.upc) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_ean')) > 0 ) $add .= ' OR LOWER(p.ean) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_jan')) > 0 ) $add .= ' OR LOWER(p.jan) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_isbn')) > 0 ) $add .= ' OR LOWER(p.isbn) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_mpn')) > 0 ) $add .= ' OR LOWER(p.mpn) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_location')) > 0 ) $add .= ' OR LOWER(p.location) LIKE "%' . $this->db->escape($part) . '%"';
				if (($this->config->get('autosearch_tag')) > 0 ) $add .= ' OR LOWER(pd.tag) LIKE "%' . $this->db->escape($part) . '%"';

				if (($this->config->get('autosearch_attr')) > 0 ) $add .= ' OR LOWER(pa.text) LIKE "%' . $this->db->escape($part) . '%"';

					$add .= ')';
				}
				$add = substr( $add, 4 );

		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

$sql = "SELECT DISTINCT *, ";

if (($this->config->get('autosearch_attr')) > 0 ) $sql .= " pa.text, ";

$sql .= " pd.product_id, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < '" . $this->NOW . "') AND (pd2.date_end = '0000-00-00' OR pd2.date_end > '" . $this->NOW . "')) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < '" . $this->NOW . "') AND (ps.date_end = '0000-00-00' OR ps.date_end > '" . $this->NOW . "')) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) ";

if (($this->config->get('autosearch_attr')) > 0 ) $sql .= "LEFT JOIN " . DB_PREFIX . "product_attribute pa ON (p.product_id = pa.product_id) ";

				$sql .= ' WHERE ' . $add . ' AND p.status = 1 ';
				$sql .= 'AND pd.language_id = ' . (int)$this->config->get('config_language_id');
				$sql .= ' AND p2s.store_id =  ' . (int)$this->config->get('config_store_id'); 

				$sql .= ' GROUP BY p.product_id ';
			
				if (($this->config->get('autosearch_sort')) > 0 ) {
				$sql .= ' ORDER BY LOWER(pd.name) ASC';
				} else {
				$sql .= ' ORDER BY p.date_available DESC, LOWER(pd.name) ASC';
				}

				$sql .= ' LIMIT ' . (int)$this->config->get('autosearch_limit');
				$res = $this->db->query( $sql );
				if( $res ) {
					$data1 = ( isset($res->rows) ) ? $res->rows : $res->row;

					$this->load->language('product/product');
					$this->load->language('extension/module/autosearch');
					$this->load->model('tool/image');
					$this->load->model('catalog/product');

					$basehref = 'product/product&product_id=';

					foreach( $data1 as $key => $values ) {
						$data[$key]['href'] = htmlspecialchars_decode($this->url->link(get_sum ($basehref . $values['product_id'])), ENT_QUOTES);
						
					$code = $values['name'];
					if (($this->config->get('autosearch_codepage')) == 1 ) $code = mb_convert_encoding($code, 'utf-8', mb_detect_encoding($code));
					if (($this->config->get('autosearch_codepage')) == 2 ) $code = mb_convert_encoding($code, 'utf-8');
					$data[$key]['name'] = htmlspecialchars_decode( (get_sum ($code)), ENT_QUOTES);
						
					if (($this->config->get('autosearch_show')) > 0 ) {
						if (($values['image']) != '') {
							$data[$key]['thumb'] = $this->model_tool_image->resize($values['image'], $image_width, $image_height);
						} else {
							$data[$key]['thumb'] = $this->model_tool_image->resize('no_image.png', $image_width, $image_height);
						}

					} else {
					$data[$key]['thumb'] = '';	
					}

					if (($this->config->get('autosearch_show_model')) > 0 ) {

					$code = $values['model'];
					if (($this->config->get('autosearch_codepage')) == 1 ) $code = mb_convert_encoding($code, 'utf-8', mb_detect_encoding($code));
					if (($this->config->get('autosearch_codepage')) == 2 ) $code = mb_convert_encoding($code, 'utf-8');
					$data[$key]['model'] = htmlspecialchars_decode( (get_sum ($code)), ENT_QUOTES);
					
					} else {
					$data[$key]['model'] = '';	
					}

					if (($this->config->get('autosearch_show_quantity')) > 0 ) {					
						if (($values['quantity']) > 0 ) {
							if ($this->config->get('config_stock_display')) { 
								$data[$key]['stock'] = ($this->language->get('text_stock')) . ' ' . ($values['quantity']);
							} else {
								$data[$key]['stock'] = $this->language->get('text_instock');
							}
						} else {
						$data[$key]['stock'] = ($values['stock_status']);
						}
					} else {
						$data[$key]['stock'] = '';	
					}

					if ((($this->config->get('autosearch_show_price')) > 0 ) and (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))){

						if ((float)$values['special']) {
						$data[$key]['special'] = strip_tags(html_entity_decode($this->currency->format($this->tax->calculate($values['price'], $values['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) , ENT_QUOTES, 'UTF-8'));
						$data[$key]['price'] = strip_tags(html_entity_decode($this->currency->format($this->tax->calculate($values['special'], $values['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) , ENT_QUOTES, 'UTF-8'));
						} else {
						$data[$key]['special'] = '';
						$data[$key]['price'] = strip_tags(html_entity_decode($this->currency->format($this->tax->calculate($values['price'], $values['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) , ENT_QUOTES, 'UTF-8'));
						}	

					} else {
					$data[$key]['price'] = '';
					}
					
					if (($this->config->get('autosearch_viewall')) != 'no' ) {
					$data[$key]['viewall'] = $this->language->get('text_viewall');
					} else {
					$data[$key]['viewall'] = '';
					}

					} // конец массива
				}
			}
		}
		echo json_encode( $data );
	}
}
?>