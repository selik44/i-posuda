<?php
class ControllerExtensionModuleLastReviews extends Controller {
  public function index($setting) {
    
    $this->load->model('catalog/review');
    
    $this->load->model('tool/image');
    
    $data['products'] = array();
    $setting = [
      'limit' => 5,
      'width' => 100,
      'height'=> 100
    ];
    
    $parts = explode('_', (string)$this->request->get['path']);
    
    $category_id = (int)array_pop($parts);
  
    $results = $this->model_catalog_review->getLastCategoryProductsRewiews($category_id, $setting['limit']);
    
    if ($results) {
      
      foreach ($results as $result) {
        if ($result['image']) {
          $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'], $result['watermark']);
        } else {
          $image = $this->model_tool_image->resize('no_image.png', $setting['width'], $setting['height']);
        }
        
        $data['products'][] = array(
          'product_id'       => $result['product_id'],
          'author'           => $result['author'],
          'date_added'       => $result['date_added'],
          'review_text'      => utf8_substr(strip_tags(html_entity_decode($result['text'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
          'thumb'            => $image,
          'name'             => $result['name'],
          'model'            => $result['model'],
          'href'             => $this->url->link('product/product', 'product_id=' . $result['product_id']),
        );
      }
      
      return $this->load->view('extension/module/last_reviews', $data);
    }
  }
}
