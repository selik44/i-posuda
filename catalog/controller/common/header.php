<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		
		$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
		
		$this->document->addScript('catalog/view/javascript/js/jquery.number.min.js');
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');
		$data['metas'] = $this->document->getMetas();
		$data['custom_code'] = $this->document->getCustomCode();

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');
		$data['og_url'] = (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		$data['og_image'] = $this->document->getOgImage();

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_page'] = $this->language->get('text_page');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['address'] = $this->config->get('config_address');

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
        $data['is_product'] = false;
        $data['is_catalog'] = false;
		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
                $data['is_product'] = true;
                $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
                $data['image_og'] = $this->model_tool_image->resize($product_info['image'], 500, 500);
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
                $data['is_catalog'] = true;

                $path_array = explode("_", $this->request->get['path']);
                $categoty_id2 = !empty($path_array) ? array_pop($path_array) : "";
                if (!empty($categoty_id2) && is_int((int)$categoty_id2)) {
                    $filter_data2 = [
                        'filter_category_id' => $categoty_id2,
                        'filter_filter' => "",
                        'sort' => "p.sort_order",
                        'order' => "ASC",
                        'start' => "0",
                        'limit' => "1",
                        'filter_sub_category' => true,
                    ];
                    if (isset($this->request->get['mfp']) && !empty($this->request->get['mfp'])) {
                        $brand = $this->request->get['mfp'];
                        $brand1 = explode("[", $brand);
                        $brand2 = explode("]", $brand1[1]);
                        $name_brand = !empty($brand2[0]) ? $brand2[0] : "";
                        if (!empty($name_brand)) {
                            $sql_name = "SELECT * FROM " . DB_PREFIX . "manufacturer WHERE name='" . $name_brand . "'";
                            $manufacturer_name = $this->db->query($sql_name);
                            if (!empty($manufacturer_name->num_rows)) {
                                foreach ($manufacturer_name->rows as $name) {
                                    $manufacturer_id = $name["manufacturer_id"];
                                    $filter_data2['filter_manufacturer_id'] = $manufacturer_id;
                                }
                            }
                        }
                    }
                    $products = $this->model_catalog_product->getProducts($filter_data2);
                    if (!empty($products) && is_array($products)) {
                        foreach ($products as $key => $product) {
                            $data['image_og'] = $this->model_tool_image->resize($product['image'], 500, 500);
                        }
                    }
                }

			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}
		
		if (isset($this->request->get['utm_source']) && !empty($this->request->get['utm_source'])) {
			$this->session->data['utm_source'] = $this->request->get['utm_source'];
		}
		
		if (isset($this->request->get['utm_medium']) && !empty($this->request->get['utm_medium'])) {
			$this->session->data['utm_medium'] = $this->request->get['utm_medium'];
		}
		
		if (isset($this->request->get['utm_campaign']) && !empty($this->request->get['utm_campaign'])) {
			$this->session->data['utm_campaign'] = $this->request->get['utm_campaign'];
		}
		
		if (isset($this->request->get['utm_term']) && !empty($this->request->get['utm_term'])) {
			$this->session->data['utm_term'] = $this->request->get['utm_term'];
		}
		
		if (isset($this->request->get['utm_content']) && !empty($this->request->get['utm_content'])) {
			$this->session->data['utm_content'] = $this->request->get['utm_content'];
		}
        
        if (isset($this->session->data['order_id'])):
            // get analytics info
            $this->load->model('checkout/order');
            $this->load->model('account/order');
            $this->load->model('catalog/product');

            $order_id = $this->session->data['order_id'];

            $order_info = $this->model_checkout_order->getOrder($order_id);

            if(!empty($order_info)):
                $transactionData = Array(
                    'id' => $order_info['order_id'].'-'.time(),
                    'affiliation' => 'iPosuda',
                    'revenue' => number_format($order_info['total'], 2, '.', ''),
                    'tax' => '0',
                    'shipping' => '0',
                    'currency' => 'UAH',
                    'products' => Array()
                );

                $produts = $this->model_account_order->getOrderProducts($order_id);

                foreach ($produts as $product):
                    $product_category = 'Посуда';
                    $main_category = $this->model_catalog_product->getMainCategory($product['product_id']);
                    if(!empty($main_category)):
                        $product_category = addcslashes($main_category['name'],"'");
                    endif;

                    $order_quantity = $product['quantity'];
                    $quantity_option = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);

                    if(!empty($quantity_option)):
                        $quantity_option = array_shift($quantity_option);
                        $option_quantity = (int)$quantity_option['name'];

                        $order_quantity = $order_quantity * $option_quantity;
                    endif;

                    $transactionData['products'][] = Array(
                        'id' => $product['product_id'],
                        'sku' => $product['model'],
                        'name' => addcslashes($product['name'],"'"),
                        'price' => number_format($product['price'], 2, '.', ''),
                        'category' => $product_category,
                        'quantity' => $order_quantity,
                    );
                endforeach;

                $data['transactionData'] = $transactionData;

            endif;
        endif;

		return $this->load->view('common/header', $data);
	}
}
