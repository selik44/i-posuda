<?php

class ControllerCommonCatsOnMain extends Controller
{

    public function index()
    {

        $this->load->model('catalog/product');

        $data = [];

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {

            $children_data = array();

            $children = $this->model_catalog_category->getCategories($category['category_id']);

            foreach ($children as $child) {

                $children2_data = array();
                $children2 = $this->model_catalog_category->getCategories($child['category_id']);

                /*foreach ($children2 as $child2) {

                    $data2 = array(
                        'filter_category_id' => $child2['category_id'],
                        'filter_sub_category' => true
                    );

                    $children3_data = array();
                    $children3 = $this->model_catalog_category->getCategories($child2['category_id']);

                    foreach ($children3 as $child3) {

                        $data3 = array(
                            'filter_category_id' => $child3['category_id'],
                            'filter_sub_category' => true
                        );

                        $children3_data[] = array(
                            'category_id' => $child3['category_id'],
                            'name' => $child3['name'],
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'])
                        );
                    }

                    $children2_data[] = array(
                        'category_id' => $child2['category_id'],
                        'child3_id' => $children3_data,
                        'name' => $child2['name'],
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'])
                    );
                }*/
                $children_info = $this->model_catalog_category->getCategory($child['category_id']);
                if ($children_info) {
                    if ($children_info['image']) {
                        $thumb_child = $this->model_tool_image->resize($children_info['image'], 28, 28);
                    } else {
                        $thumb_child = '';
                    }
                }

                $children_data[] = array(
                    'category_id' => $child['category_id'],
                    'child2_id' => $children2_data,
                    'name' => $child['name'],
                    'thumb' => $thumb_child,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                );
            }


            $category_info = $this->model_catalog_category->getCategory($category['category_id']);
            if ($category_info) {
                if ($category_info['image']) {
                    $thumb = $this->model_tool_image->resize($category_info['image'], 28, 28);
                } else {
                    $thumb = '';
                }
            }
//            $breadCity = $this->city->getCityData();
            $breadCity = [];

            $breadCityName = ' в Киеве';
            if($breadCity){
                switch ($breadCity['city_name']):
                    case 'Одесса':
                        $breadCityName = ' Одесса';
                        break;
                    case 'Харьков':
                        $breadCityName = ' Харьков';
                        break;
                    case 'Днепр':
                        $breadCityName = ' Днепр';
                        break;
                    case 'Опт':
                        $breadCityName = ' оптом';
                        break;
                endswitch;
            }
            $title = $category['name'].$breadCityName;

            $data['categories'][] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'thumb' => $thumb,
                'children' => $children_data,
                'href' => $this->url->link('product/category', 'path=' . $category['category_id']),
                'title' => $title,
                'column' => $category['column'] ? $category['column'] : 1,
                'top' => $category['top']
            );
        }

        return $this->load->view('common/cats_on_main', $data);
    }

}
