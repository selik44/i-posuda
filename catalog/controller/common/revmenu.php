<?php
class ControllerCommonRevmenu extends Controller {
	public function index() {
		
		$data['heading_title'] = 'Категории<span class="hidden-xs"> товаров</span>';
		$this->document->addScript('catalog/view/javascript/aim.js');
		$this->document->addScript('catalog/view/javascript/amazoncategory.js');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/revmenu.css?v=1.0');

		$setting = $this->config->get('revtheme_header_menu');
		if ($setting['inhome']) {
			$data['module_class'] = 'inhome';
		} else {
			$data['module_class'] = false;
		}

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
		
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
		
		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}
		
		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}
		
		if (isset($parts[2])) {
            $data['child2_id'] = $parts[2];
        } else {
            $data['child2_id'] = 0;
        }

        if (isset($parts[3])) {
            $data['child3_id'] = $parts[3];
        } else {
            $data['child3_id'] = 0;
        }
			
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
					$this->load->model('tool/image');
		$data['categories'] = array();
		
		$categories = $this->model_catalog_category->getCategories(0);
		

		foreach ($categories as $category) {

			$children_data = array();

			$children = $this->model_catalog_category->getCategories($category['category_id']);

			foreach ($children as $child) {

				$children2_data = array();
                $children2 = $this->model_catalog_category->getCategories($child['category_id']);

                foreach ($children2 as $child2) {

                    $data2 = array(
                        'filter_category_id'  => $child2['category_id'],
                        'filter_sub_category' => true
                    );

                    $children3_data = array();
                    $children3 = $this->model_catalog_category->getCategories($child2['category_id']);

                    foreach ($children3 as $child3) {

                        $data3 = array(
                            'filter_category_id'  => $child3['category_id'],
                            'filter_sub_category' => true
                        );

                        $children3_data[] = array(
                            'category_id' => $child3['category_id'],
                            'name'        => $child3['name'],
                            'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'])
                        );
                    }

                    $children2_data[] = array(
                        'category_id' => $child2['category_id'],
                        'child3_id'   => $children3_data,                    
                        'name'        => $child2['name'],
                        'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'])
                    );
                }
			$children_info = $this->model_catalog_category->getCategory($child['category_id']);
			if ($children_info) {
				if ($children_info['image']) {
					$thumb_child = $this->model_tool_image->resize($children_info['image'], 28, 28);
				} else {
					$thumb_child = '';
				}
			}
				$children_data[] = array(
					'category_id' => $child['category_id'],
					'child2_id'   => $children2_data,
					'name'        => $child['name'],
					'thumb'        => $thumb_child,
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
				);		
			}
			

			$category_info = $this->model_catalog_category->getCategory($category['category_id']);
			if ($category_info) {
				if ($category_info['image']) {
					$thumb = $this->model_tool_image->resize($category_info['image'], 28, 28);
				} else {
					$thumb = '';
				}
			}
			
			$data['categories'][] = array(
				'category_id' => $category['category_id'],
				'name'        => $category['name'],		
				'thumb'       => $thumb,
				'children'    => $children_data,
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id']),
				'column'      => $category['column'] ? $category['column'] : 1,
				'top'         => $category['top']
			);	

		}
    
    $this->load->model('catalog/manufacturer');
    
    $results = $this->model_catalog_manufacturer->getManufacturers();
    
    foreach ($results as $key => $result) {
      $name = $result['name'];
      
      if ($result['image']) {
        $thumb = $this->model_tool_image->resize($result['image'], 28, 28);
      } else {
        $thumb = '';
      }
      
      $data['manufacturers'][] = array(
        'name'   => $name,
        'thumb'  => $thumb,
        'href'   => $this->url->linkFormatted('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']."&city_id=1"),
        'column' => $key % 2 == 0 ? 1 : 2
      );
    }
    
		
		$this->load->language('common/header');
		
		$data['search'] = $this->load->controller('common/search');
		
		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_logout'] = $this->language->get('text_logout');
		
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		
		$data['telephone'] = $this->config->get('config_telephone');
		$data['telephone_m'] = $this->config->get('config_telephone_mts');
		
		$data['home_link'] = $this->url->link('common/home', '', true);
		
		return $this->load->view('common/revmenu', $data);
  	}
}
