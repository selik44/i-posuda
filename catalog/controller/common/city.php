<?php
class ControllerCommonCity extends Controller {
	public function index() {
		if (isset($this->request->get['city_id'])) {
			if ($this->request->get['city_id'] != 0) 
			{
				$this->session->data['city_id'] = $this->request->get['city_id'];
			} else { 
				unset($this->session->data['city_id']); 
			}
			
			$this->session->data['user_change_city'] = $this->request->get['city_id'];
			
			$json = $this->request->get['city_id']; 
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
