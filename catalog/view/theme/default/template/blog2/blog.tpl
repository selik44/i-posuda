<?php echo $header; ?>
            <div class="row main"><?php echo $content_top; ?>
                <!-- heading -->
                <div class="heading clearfix tablet-hidden">      <h1>Блог</h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
                </div>
                <div class="clearfix">
                    <!-- sidebar -->
                    <!--  <aside class="sidebar">
                        <nav class="sidebar-nav clearfix">
                            <ul>
                                <li><a href="/news">Новости</a></li>
                                <li><a class="active">Статьи</a></li>
                            </ul>
                        </nav>
                    </aside>  -->
    <div class="container">
                        <div class="content-holder">
      <?php if ($articles) { ?>
	<ul class="news-list">
        <?php foreach ($articles as $article) { ?>
<li>
<a class="box" href="<?php echo $article['href']; ?>">
                        <h2><?php echo $article['name']; ?></h2>
                        <p><?php echo $article['intro_text']; ?></p>
<div class="bottom-row">
<span class="date"><?php echo $article['date_modified']; ?></span><span class="text-more"><?php echo $button_read_more; ?></span>
                  </div>
</a>
              </li>
        <?php } ?>
</ul>
<div><?php echo $pagination; ?></div>
              </div>
              </div>

      <?php } ?>
      <?php if (!$articles) { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
</div>
</div>
<?php echo $footer; ?>
