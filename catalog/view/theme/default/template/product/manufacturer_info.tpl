<?php echo $header; ?>
<div class="spiner__overlay">
    <div class="spinner-css">
  <span class="side sp_left">
    <span class="fill"></span>
  </span>
        <span class="side sp_right">
    <span class="fill"></span>
  </span>
    </div>
</div>
<div class="row main "><?php echo $content_top; ?>
    <div class="form-catalog">
        <div class="content-holder">
            <!-- heading -->
            <div class="heading clearfix">
                     <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                          <?php $i=1; ?>
                          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                          <li itemprop="itemListElement" itemscope
                              itemtype="http://schema.org/ListItem">
                              <?php if($i<count($breadcrumbs)) { ?>
                              <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                  <?php } ?>
                                  <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                                  <?php if($i<count($breadcrumbs)) { ?>
                              </a>
                              <?php } ?>
                              <meta itemprop="position" content="<?php echo $i ?>"/>
                          </li>
                          <?php $i++; ?>
                          <?php } ?>
                      </ol>
                  </div>

            <div class='row-new' >
                <div class="row-new" >
                    <section class='cats-on-main'>
                        <?php foreach ($categories_brand as $key => $category): ?>
                        <?php if($key == 0 || $key == 4): ?>
                        <div class="cats-line">
                            <?php endif; ?>
                            <div class='category-block category-block-back-<?php echo $category['category_id'] ?>'>
                            <div class='category-block-title'>
                                <img class="category-block-thumb"
                                     alt="image"
                                     src="<?php echo $category['thumb']; ?>">
                                <a href="<?php echo $category['href']; ?>"
                                   title="<?php echo $category['title']; ?>"><?php echo $category['name']; ?></a>

                            </div>
                            <?php if (isset($category['children'])): ?>
                            <ul class='category-subblock subblock-hide'>
                                <?php foreach ($category['children'] as $child): ?>
                                <li>
                                    <a href="<?php echo $child['href']; ?>" title="<?php echo $child['title']; ?>"><?php echo $child['name']; ?></a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>
                        </div>
                        <?php if($key == 3 || $key == count($categories_brand) - 1): ?>

                        <?php endif; ?>
                        <?php endforeach; ?>
                         <div class='block-discount hide'>
                            <a href="specials/"><span class='block-discount-image'></span>Товары со
                                  скидкой</a>
                          </div>
                      </section>
                  </div>
              </div>

                <?php if ($products) { ?>
                 <div class="sort-block" data-ajax-content="scroll">
                     <span class="text"><?php echo $text_sort; ?></span>
                     <div class="popup-holder">
                         <span class="opener"><span class="inner"></span></span>
                         <div class="popup">
                             <ul>
                                 <?php foreach ($sorts as $sorts) { ?>
                                 <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                 <li class="active"><a class="close-link"
                                                       href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a>
                                 </li>
                                 <?php } else { ?>
                                 <li><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                                 <?php } ?>
                                 <?php } ?>
                             </ul>
                             <script type="text/javascript">
                                 $(".sort-block > .popup-holder > span > span").html($(".active > a").html());
                             </script>
                         </div>
                     </div>
                 </div>
            <div data-ajax-content="container">
                 <ul class="list-products clearfix" data-load-more="container">
                     <?php foreach ($products as $product) { ?>
                     <li class="product-tile">
                         <div class="box" id="ptoduct<?php echo $product['product_id']; ?>">
                             <?php if ($product['speciales']) { ?>
                             <span class="tag discount">Скидка</span>
                             <?php } ?>
                             <div class="visual">
                                 <a href="<?php echo $product['href']; ?>">
                                     <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                          title="<?php echo $product['name']; ?>" class="img-responsive"/>
                                 </a>
                             </div>
                             <p class="product_name"><a
                                         href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
                             <?php if ($product['options']) { ?>
                             <?php foreach ($product['options'] as $option) { ?>

                             <?php if ($option['type'] == 'radio') { ?>
                             <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                 <div id="input-option<?php echo $option['product_option_id']; ?>">
                                     <?php $n = 1; foreach ($option['product_option_value'] as $i=>$option_value) { ?>
                                     <?php if ($option_value['status']==1) { ?><?php if ($option_value['see'] == 1) { ?>
                                     <div class="radio row-choose clearfix <?php if ($n != 1) { ?> hidden <?php } ?>">
                                         <input id="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>" <?php if ($n ==1) { ?>
                                         checked="checked"  <?php } ?> type="radio"
                                         name="option[<?php echo $option['product_option_id']; ?>]"
                                         value="<?php echo $option_value['product_option_value_id']; ?>"
                                         class="product-<?php echo $product["product_id"]; ?> " />
                                         <label for="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>">
                                                         <span class="radio-text"><?php echo $option_value['name']; ?> <?php if ($option_value['quant']) { ?>
                                                             (<?php echo $option_value['quant']; ?>шт)
                                                             <?php } ?></span>

                                             <?php if ($option_value['special']) { ?>
                                             <span class="price"><span
                                                         class="text-grey"><?php echo $option_value['price']; ?></span>
                                       <span class="price-new"><strong
                                                   class="sum text-red"><?php echo $option_value['special']; ?></strong></span></span>
                                             <?php } else { ?>
                                             <span class="price"><span class="price-new"><strong
                                                             class="sum text-red"><?php echo $option_value['price']; ?></strong></span></span>

                                             <?php } ?>
                                         </label>
                                     </div>

                                     <?php $n++; } ?>
                                     <?php } ?>
                                     <?php  } ?>
                                 </div>
                             </div>
                             <?php } ?>
                             <?php } ?>
                             <?php } ?>
                             <input readonly class="spinner quantity_input" role="spinbutton" type="text"
                                    value="<?php echo $product['minimum']; ?>"/>
                             <div class="bottom-row clearfix">
                                 <a class="link-cart"
                                    onclick="cart.add('<?php echo $product['product_id']; ?>', $(this).parent().parent().find('.quantity_input').attr('aria-valuenow'), $(this).parent().parent().find('.rad-checked').next().attr('name'), $(this).parent().parent().find('.rad-checked').next().val() );">
                                     <span class="hidden-xs hidden-sm hidden-md text-hidden"><?php echo $button_cart; ?></span>
                                     <span class="text-red"> </span></a>
                                 <a data-id="<?php echo $product['product_id']; ?>"
                                    class="link-favorite <?php echo isset($product['class_button'])? $product['class_button'] :''; ?>"
                                    title="<?php echo $button_wishlist; ?>" <?php if (empty($product['class_button'])) { ?>
                                 onclick="wishlist.add('<?php echo $product['product_id']; ?>');" <?php } else { ?>
                                 onclick="wishlist.remove('<?php echo $product['product_id']; ?>');" <?php }  ?>
                                 ><?php echo $button_wishlist; ?></a>
                             </div>
                             <div class="product__reviews">
                                 <?php if($product['info']['average_rating'] > 0) { ?>
                                 <div class="product__stars-wrapper">
                                     <span class="product__stars stars stars<?php echo $product['info']['average_rating']; ?>"></span>
                                 </div>
                                 <?php } ?>
                                 <a class="product__reviews-link" href="<?php echo $product['href']; ?>#reviews">
                                     <?php if($product['info']['total_reviews'] > 0) { ?>
                                     <?php echo $product['info']['total_reviews']; ?> <?php echo $product['info']['review_text']; ?>
                                     <?php } ?>
                                 </a>
                             </div>
                         </div>
                     </li>
                     <?php } ?>
                     <?php if(isset($show_next_pagelink) && $show_next_pagelink): ?>
                     <li class="next-page-link">
                         <a href="javascript:void(0);"
                            data-load-more="btn"
                            data-load-more-find="product-tile"
                            data-load-more-href="<?php echo $show_next_pagelink; ?>" class="box">
                             <div class="center"><span class="text">Загрузить еще</div>
                         </a>
                     </li>
                     <?php endif; ?>
                 </ul>
                 <div class="pagination">
                     <div class="lds-spinner">
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                         <div></div>
                     </div>
                     <?php echo $pagination; ?>
                 </div>
            </div>
                 <?php } else { ?>
                 <p><?php /* echo $text_empty; */ ?></p>
                 <?php } ?>
                 <?php echo $content_bottom; ?>
             </div>

               <?php echo $column_right; ?>
         </div>
         <?php if ($description) { ?>
         <section class="text-article content-holder" style="margin: 0 0px 16px;">
             <?php if ($description) { ?>
             <div class="text-section"><?php echo $description; ?></div>
             <?php } ?>
         </section>
         <?php } ?>
     </div>
    <div itemscope itemType="http://schema.org/Product" style="display:none">
        <span itemprop="name"><?php echo $manufacturer_name; ?></span>
        <?php if ($total_category_reviews > 0 ) { ?>
        <div itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating" itemscope>
            <meta itemprop="reviewCount" content="<?php echo $total_category_reviews; ?>" />
            <meta itemprop="ratingValue" content="<?php echo $average_category_rating;?>" />
        </div>
        <?php } ?>
        <div itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
            <?php if(isset($min_max_prices) && !empty($min_max_prices)): ?>
            <?php
                    $lowPrice = ceil($min_max_prices['min_price']);
                    $highPrice = ceil($min_max_prices['max_price']);

                    if(isset($min_max_prices['min_special_price']) && $min_max_prices['min_special_price'] && $min_max_prices['min_special_price'] < $min_max_prices['min_price']):
                        $lowPrice = ceil($min_max_prices['min_special_price']);
                    endif;

                    if(isset($min_max_prices['max_special_price']) && $min_max_prices['max_special_price'] && $min_max_prices['max_special_price'] > $min_max_prices['max_price']):
            $highPrice = ceil($min_max_prices['max_special_price']);
            endif;
            if($lowPrice && $highPrice): ?>
            <meta itemprop="lowPrice" content="<?php echo $lowPrice; ?>" />
            <meta itemprop="highPrice" content="<?php echo $highPrice; ?>" />
            <meta itemprop="priceCurrency" content="UAH" />
            <span itemprop="offerCount"><?php echo $products_total_count; ?></span>
            <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
     <script><!--
         $(document).ready(function () {
             if ($(window).width() > 767) {
                 jQuery('.cats-on-main .cats-line').sameHeight({
                     elements: '.category-block',
                     multiLine: true
                 });
             }

             var url_direct = new URL(window.location.href);
             var page_num_direct = url_direct.searchParams.get("page");
             var text_section = $("div.text-section");
             if( page_num_direct != null ){
                 text_section.hide();
             } else {
                 text_section.show();
             }
             $(document).on('click', '.pagination a', function () {
                 var url = new URL($(this).attr('href'));
                 var page_num = url.searchParams.get("page");
                 if( page_num != null ){
                     text_section.hide();
                 } else {
                     text_section.show();
                 }
             });
         });

         $(document).on('click', '.category-block-title', function (e) {
             var thisSublock = $(this).closest('.category-block')
                 .find('.category-subblock');
             $(this).closest('.cats-on-main')
                 .find('.category-subblock.subblock-show').not(thisSublock)
                 .removeClass('subblock-show')
                 .addClass('subblock-hide')

             if ($(thisSublock).hasClass('subblock-hide')) {
                 if ($(window).width() < 769) {
                     e.preventDefault();
                 }
                 $(thisSublock).removeClass('subblock-hide')
                     .addClass('subblock-show');
             } else {
                 $(thisSublock).removeClass('subblock-show')
                     .addClass('subblock-hide');
             }
         });
         --></script>
<script type="text/javascript"><!--
    $(document).ready(function () {
        $('.link-cart .text-red').each(function () {
            var default_price_value = $(this).closest('.box').find('.jcf-label-active .price .price-new').html();
            $(this).html(default_price_value);
        });
    });

    $('input[type=radio]').change(function () {
        var this_box = $(this).closest('.box');
        var this_price = parseInt($(this).closest('.row-choose').find('.price .price-new').text().replace(' ', ''));
        var this_qnt = parseInt($(this_box).find(".spinner").spinner("value"));

        var calc_price = $.number(this_price * this_qnt, 0, ',', ' ');

        $(this_box).find('.link-cart .cur-value').text(calc_price);
    });

    $(".spinner").on("spin", function (event, ui) {
        str = "";

        var this_box = $(this).closest('.box');
        if (ui.value > 0) {
            if ($(this_box).find('.jcf-label-active .price .price-new').length > 0) {
                var this_price = parseInt($(this_box).find('.jcf-label-active .price .price-new').text().replace(' ', ''));
                var this_qnt = ui.value;

                var calc_price = $.number(this_price * this_qnt, 0, ',', ' ');

                $(this_box).find('.link-cart .cur-value').text(calc_price);
            }
        }
    });

      $(window).load(function() {
        $('.spiner__overlay').fadeOut('slow');
      });
    --></script>

<?php echo $footer; ?>
