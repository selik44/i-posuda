<?php echo $header; ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#youtube_thumb').on('click', function(event){
            $('.tabset-product .tab-youtube').click();
            $("html, body").animate({ scrollTop: $('.tabset-product').offset().top }, 1000);
        });
         $('#youtube_thumb').on('click', function(event){
            $('#open-youtube-mobile').click();
            $("html, body").animate({ scrollTop: $('.tabs-content').offset().top }, 1000);
        });
    });
</script>
<div class="row main">
    <?php echo $content_top; ?>
    <!-- heading -->
    <div class="heading clearfix tablet-hidden">
        <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php $i=1; ?>
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem">
                <?php if($i<count($breadcrumbs)) { ?>
                <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                    <?php } ?>
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                    <?php if($i<count($breadcrumbs)) { ?>
                </a>
                <?php } ?>
                <meta itemprop="position" content="<?php echo $i ?>"/>
            </li>
            <?php $i++; ?>
            <?php } ?>
        </ol>
    </div>
    <div id="product" class="product-card clearfix">
        <div class="heading-product">
            <h1><?php echo $heading_title; ?></h1>
            <span class="article"><?php echo $text_model; ?> <span class="text-red"><?php echo $model; ?></span></span>
        </div>
        <div class="slider-product">
            <?php if ($special) { ?>
            <span class="tag discount">Скидка</span>
            <?php } ?>
            <?php if ($youtube) { ?>
            <span class="tag-youtube youtube" data-toggle="tooltip" data-placement="right" title="Видеобзор">
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                     width="20" height="20"
                     viewBox="0 0 172 172"
                     style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><g id="surface1"><path d="M17.2,30.96c-9.46,0 -17.2,7.74 -17.2,17.2v75.68c0,9.46 7.74,17.2 17.2,17.2h92.88c9.46,0 17.2,-7.74 17.2,-17.2v-18.3825l39.6675,21.3925l5.0525,2.795v-87.29l-5.0525,2.795l-39.6675,21.3925v-18.3825c0,-9.46 -7.74,-17.2 -17.2,-17.2zM17.2,37.84h92.88c5.73781,0 10.32,4.58219 10.32,10.32v75.68c0,5.73781 -4.58219,10.32 -10.32,10.32h-92.88c-5.73781,0 -10.32,-4.58219 -10.32,-10.32v-75.68c0,-5.73781 4.58219,-10.32 10.32,-10.32zM165.12,53.8575v64.285l-37.84,-20.425v-23.435z"></path></g></g></g></svg>
            </span>
           <?php } ?>
            <div class="slider-product-for">
                <?php if($popup && $thumb): ?>
                <div class="slide">
                    <img src="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>"/>
                </div>
                <?php endif; ?>
                <?php if ($images) { ?>
                <?php foreach ($images as $image) { ?>
                <div class="slide">
                    <img src="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>"/>
                </div>
                <?php } ?>
                <?php } /* else { ?>
                <div class="slide">
                    <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>"/>
                </div>
                <?php } */ ?>
            </div>
            <div class="slider-product-nav">
                <?php if($popup && $thumb): ?>
                <div class="slide">
                    <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>"/>
                </div>
                <?php endif; ?>
                <?php if ($images) { ?>
                <?php foreach ($images as $image) { ?>
                <div class="slide">
                    <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>"/>
                </div>
                <?php } ?>
                <?php } ?>
                <?php if($youtube){ ?>
                <div class="slide">
                    <img src="images/youtube.jpg" title="Видеобзор" id="youtube_thumb"
                         alt="Видеобзор"/>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="form-product">
            <?php if (isset($options) && !empty($options)) { ?>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="row-choose clearfix hidden form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>

                <div class="row-qty clearfix" id="input-option<?php echo $option['product_option_id']; ?>">
                    <div class="top-holder">
                        <?php if ($option_value['see'] == 1) { ?>

                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]"
                               value="<?php echo $option_value['product_option_value_id']; ?>"/>
                        <div class="title-holder">
											<span class="title">
												<strong><?php echo $option_value['name']; ?></strong>
                                                <?php if ($option_value['quant']) { ?>
                                                (<?php echo $option_value['quant']; ?>шт)
                                                <?php } ?>
											</span></span></div>
                        <?php if ($option_value['special']) { ?>
                        <span class="price"><span class="text-grey"><?php echo $option_value['price']; ?></span>
												<span class="price-new <?php echo ($option_value['status'] != 1) ? 'disable' : '' ?>"><strong
                                                            class="sum text-red"><?php echo $option_value['special']; ?></strong></span></span>
                        <?php } else { ?>

                        <span class="price"><span
                                    class="price-new <?php echo ($option_value['status'] != 1) ? 'disable' : '' ?>"><strong
                                        class="sum text-red"><?php echo $option_value['price']; ?></strong></span></span>

                        <?php } ?>
                    </div>

                    <?php if ($option_value['status'] == 1) { ?>
                    <div class="spinner2"><input readonly class="spinner quantity_input" type="text"
                                                 value="<?php echo $minimum; ?>"/></div>
                    <button onclick="cart.add('<?php echo $product_id; ?>', $(this).parent().find('.quantity_input').attr('aria-valuenow'), $(this).parent().find('input[type=\'hidden\']').attr('name'), $(this).parent().find('input[type=\'hidden\']').val());"
                            data-loading-text="<?php echo $text_loading; ?>"
                            class="btn btn-primary btn-lg btn-block button"><span
                                class="cart"><?php echo $button_cart; ?></span></button>
                    <?php } else { ?>
                    <span class="button button-border-gray-light">Не продаётся</span>
                    <?php } ?>
                    <?php } ?>
                </div>


                <?php } ?>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <div class="row-buttons clearfix">
                <div class="buy-click clearfix">
                    <input placeholder="+38 (0XX) XXX XX XX" type="tel" id="phone"/>
                    <button type="button" class="button gray-dark"
                            onclick="oneclick.add('<?php echo $product_id; ?>',$('.buy-click #phone').val() )">Купить в
                        один клик
                    </button>
                    <div class="box-success"></div>
                    <div class="box-fail"></div>
                </div>
                <a data-id="<?php echo $product_id; ?>"
                   class="button-favorite <?php echo (isset($in_wishlist) && $in_wishlist) ? 'active' : '' ?>"
                   title="<?php echo $button_wishlist; ?>"  <?php if (!$in_wishlist) { ?>
                onclick="wishlist.add('<?php echo $product_id; ?>');" <?php } else { ?>
                onclick="wishlist.remove('<?php echo $product_id; ?>');" <?php }  ?>><span
                        class="inner"><?php echo $button_wishlist; ?></span></a>
            </div>
        </div>
    </div>
</div>
<div class="row main">
    <ul class="tabset-product">
        <li class="active" rel="features"><?php echo $tab_attribute; ?></li>
        <?php if ($description) { ?>
       <!-- <li rel="description"><?php echo $tab_description; ?></li> -->
        <?php } ?>
        <?php if ($youtube) { ?>
            <li class="tab-youtube" rel="youtube">Видеобзор <span class="text-gray"></span></li>
        <?php } ?>
        <li class="tab-reviews" rel="reviews">Отзывы <span class="text-gray"></span></li>
        <?php if ($product_tabs_1) { ?>
        <?php foreach($product_tabs_1 as $product_tab_1) { ?>
        <li rel="tab-product-tab<?php echo $product_tab_1['tab_id'];?>"><?php echo $product_tab_1['name']; ?></li>
        <?php } ?>
        <?php } ?>
    </ul>
    <div class="tabs-content">
        <h3 class="d_active tab_drawer_heading" rel="features"><?php echo $tab_attribute; ?></h3>
        <div class="tab_content" id="features">
            <div class="clearfix">
                <?php $s=1; ?>
                <ul class="column discount-column">
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                    <?php if ($s == 5) { ?>
                </ul>
                <ul class="column discount-column">
                    <?php } ?>
                    <li>
                        <span class="title"><?php echo $attribute['name']; ?>:</span>
                        <span class="text"><?php echo $attribute['text']; ?></span>
                    </li>
                    <?php $s = $s+1; } ?><?php } ?>
                    <?php if(isset($option['product_option_value'])): ?>
                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                    <?php if ($option_value['quant'] > 1) {
                    $option_text = $option_value['name'];
                    if( $option_value['name'] == 'Ящик'):
                    $option_text = 'ящике';
                    elseif($option_value['name'] == 'Упаковка'):
                    $option_text = 'упаковке';
                    endif;
                    ?>
                    <li>
                        <span class="title">Количество в <?php echo $option_text; ?>:</span>
                        <span class="text">
                    <?php echo $option_value['quant']; ?>шт</span>
                    </li>
                    <?php } ?><?php } ?>
                    <?php endif; ?>
                    <?php /*
                  <li>
                    <span class="title">Упаковок в ящике:</span>
                    <span class="text"></span>
                    </li>
                    */?>
                    <li>
                        <span class="title">Бренд:</span>
                        <a href="<?php echo $manufacturers; ?>" title="<?php echo $manufacturer; ?>"
                           class="text"><?php echo $manufacturer; ?></a>
                    </li>
                    <?php if($description){ ?>
                    <li>
                    <div class="column">
                        <h2>Описание</h2>
                        <?php echo $description; ?>
                    </div>
                    </li>
                    <?php } ?>
                </ul>

            </div>
        </div>
         <?php if($youtube){ ?>
           <h3 class="tab_drawer_heading" id="open-youtube-mobile" rel="youtube-mobile">Видеобзор</h3>
        <?php } ?>
        <div class="tab_content text-section" id="youtube-mobile">
              <iframe id="ytplayer" type="text/html" 
                        src="https://www.youtube.com/embed/<?php echo $youtube_id ?>?autoplay=1&origin=<?php echo $youtube_id ?>"
                        frameborder="0"/></iframe>
        </div>
        <div class="tab_content" id="youtube">
            <div class="clearfix">
                <iframe id="ytplayer" type="text/html" 
                        src="https://www.youtube.com/embed/<?php echo $youtube_id ?>?autoplay=1&origin=<?php echo $youtube_id ?>"
                        frameborder="0"/></iframe>
            </div>
        </div>
        <?php if ($review_status) { ?>
        <h3 class="tab_drawer_heading" rel="reviews">Отзывы <span class="text-gray"></span></h3>
        <div class="tab_content" id="reviews">
            <div class="clearfix">
                <div class="open-close active">
                    <span class="slide-opener button tablet-show button-border-gray">Закрыть форму</span>
                    <form class="slide-block form-add-review form-horizontal" id="form-review">
                        <fieldset>
                            <h2>Оставить отзыв</h2>
                            <div class="label-holder"><label for="name"><?php echo $entry_name; ?> <span
                                            class='text-red'>*</span></label></div>
                            <div class="input-holder">
                                <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name"
                                       class="form-control"/></div>
                            <div class="label-holder">
                                <label for="email">E-mail</label>
                                <span class="text-right">необязательно для заполнения</span>
                            </div>
                            <div class="input-holder"><input id="email" name="email" type="text"
                                                             value="<?php echo $customer_email; ?>"/></div>
                            <div class="label-holder"><label for="comment"><?php echo $entry_review; ?> <span
                                            class='text-red'>*</span></label></div>
                            <div class="input-holder"><textarea placeholder="Общее впечатления от использования товара"
                                                                id="comment" cols="30" rows="10" name="text"></textarea>
                            </div>
                            <div class="popup-holder advantages-block">
                                <span class="opener">Указать достоинства и недостатки</span>
                                <div class="popup">
                                    <div class="label-holder"><label for="advantages">Достоинства</label></div>
                                    <div class="input-holder"><input id="advantages" name="advantages" type="text"/>
                                    </div>
                                    <div class="label-holder"><label for="disadvantages">Недостатки</label></div>
                                    <div class="input-holder"><input name="limitations" id="disadvantages" type="text"/>
                                    </div>
                                </div>
                            </div>
                            <div class="stars-block">
                                <span class="title">Оцените товар <span class='text-red'>*</span></span>
                                <ul class="star-rating">
                                    <li><a href="#" title="1" class="one-star">1</a></li>
                                    <li><a href="#" title="2" class="two-stars">2</a></li>
                                    <li><a href="#" title="3" class="three-stars">3</a></li>
                                    <li><a href="#" title="4" class="four-stars">4</a></li>
                                    <li><a href="#" title="5" class="five-stars">5</a></li>
                                </ul>
                                <input class="rating-star" type="hidden" name="rating" value=""/>
                            </div>
                            <div id="review-status"></div>
                             <?php echo $captcha; ?>
                            <button type="button" class="button" id="button-review"
                                    data-loading-text="<?php echo $text_loading; ?>"
                                    class="btn btn-primary"><?php echo $button_continue; ?></button>
                        </fieldset>
                    </form>
                </div>
                <!-- testimonials-list -->
                <div id="review"></div>
            </div>
        </div>
        <?php } ?>
        <?php if ($product_tabs_1) { ?>
        <?php foreach($product_tabs_1 as $product_tab_1) { ?>
        <h3 class="tab_drawer_heading"
            rel="tab-product-tab<?php echo $product_tab_1['tab_id'];?>"><?php echo $product_tab_1['name']; ?></h3>
        <div id="tab-product-tab<?php echo $product_tab_1['tab_id'];?>"
             class="tab_content"><?php echo $product_tab_1['description']; ?>
        </div>
        <?php } ?>
        <?php } ?>





        </div>

    </div>
    <?php echo $viewed; ?>
    <?php if ($products) { ?>
    <section class="section-similar">
        <h2>Вам может также понравиться</h2>

        <ul class="list-products clearfix">
            <?php foreach ($products as $product) { ?>
            <li>
                <div class="box" id="ptoduct<?php echo $product['product_id']; ?>">
                    <?php if ($product['special']) { ?>
                    <span class="tag discount">Скидка</span>
                    <?php } ?>
                  <?php if ($youtube) { ?>
                   <!-- <span class="tag-youtube youtube">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                             width="20" height="20"
                             viewBox="0 0 172 172"
                             style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><g id="surface1"><path d="M17.2,30.96c-9.46,0 -17.2,7.74 -17.2,17.2v75.68c0,9.46 7.74,17.2 17.2,17.2h92.88c9.46,0 17.2,-7.74 17.2,-17.2v-18.3825l39.6675,21.3925l5.0525,2.795v-87.29l-5.0525,2.795l-39.6675,21.3925v-18.3825c0,-9.46 -7.74,-17.2 -17.2,-17.2zM17.2,37.84h92.88c5.73781,0 10.32,4.58219 10.32,10.32v75.68c0,5.73781 -4.58219,10.32 -10.32,10.32h-92.88c-5.73781,0 -10.32,-4.58219 -10.32,-10.32v-75.68c0,-5.73781 4.58219,-10.32 10.32,-10.32zM165.12,53.8575v64.285l-37.84,-20.425v-23.435z"></path></g></g></g></svg>
                    </span> -->
                    <?php } ?>
                    <div class="visual">
                        <a href="<?php echo $product['href']; ?>">
                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                 title="<?php echo $product['name']; ?>" class="img-responsive"/>
                        </a>
                    </div>
                    <p class="product_name"><a
                                href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
                    <?php if ($product['options']) { ?>
                    <?php foreach ($product['options'] as $option) { ?>
                    <?php if ($option['type'] == 'select') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"
                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <select name="option[<?php echo $option['product_option_id']; ?>]"
                                id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'radio') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php $n = 1; foreach ($option['product_option_value'] as $i=>$option_value) { ?>
                            <?php if ($option_value['status']==1) { ?><?php if ($option_value['see'] == 1) { ?>
                            <div class="radio row-choose clearfix <?php if ($n != 1) { ?> hidden <?php } ?>">
                                <input id="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>" <?php if ($n ==1) { ?>
                                checked="checked"  <?php } ?> type="radio"
                                name="option[<?php echo $option['product_option_id']; ?>]"
                                value="<?php echo $option_value['product_option_value_id']; ?>"
                                class="product-<?php echo $product["product_id"]; ?> " />
                                <label for="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>">
                                                    <span class="radio-text"><?php echo $option_value['name']; ?> <?php if ($option_value['quant']) { ?>
                                                        (<?php echo $option_value['quant']; ?>шт)
                                                        <?php } ?></span>

                                    <?php if ($option_value['special']) { ?>
                                    <span class="price"><span
                                                class="text-grey"><?php echo $option_value['price']; ?></span>
								  <span class="price-new"><strong
                                              class="sum text-red"><?php echo $option_value['special']; ?></strong></span></span>
                                    <?php } else { ?>
                                    <span class="price"><span class="price-new"><strong
                                                    class="sum text-red"><?php echo $option_value['price']; ?></strong></span></span>

                                    <?php } ?>
                                </label>
                            </div>

                            <?php $n++; } ?>
                            <?php } ?>
                            <?php  } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'checkbox') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"
                                           name="option[<?php echo $option['product_option_id']; ?>][]"
                                           value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                    <?php echo $option_value['name']; ?>
                                    <?php if ($option_value['price']) { ?>
                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                    )
                                    <?php } ?>
                                </label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'image') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <div class="radio">
                                <label>
                                    <input <?php if ($i==1) { ?> checked="checked"  <?php } ?> type="radio"
                                    name="option[<?php echo $option['product_option_id']; ?>]"
                                    value="<?php echo $option_value['product_option_value_id']; ?>"
                                    class="product-<?php echo $product["product_id"]; ?> " />
                                    <img src="<?php echo $option_value['image']; ?>"
                                         alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                         class="img-thumbnail"/> <?php echo $option_value['name']; ?>
                                    <?php if ($option_value['price']) { ?>
                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                    )
                                    <?php } ?>
                                </label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'text') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"
                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                               value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
                               id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'textarea') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"
                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5"
                                  placeholder="<?php echo $option['name']; ?>"
                                  id="input-option<?php echo $option['product_option_id']; ?>"
                                  class="form-control"><?php echo $option['value']; ?></textarea>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'file') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>"
                                data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block">
                            <i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value=""
                               id="input-option<?php echo $option['product_option_id']; ?>"/>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'date') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"
                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <div class="input-group date">
                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                   value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                   class="form-control"/>
                            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'datetime') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"
                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <div class="input-group datetime">
                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                   value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm"
                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                   class="form-control"/>
                            <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'time') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"
                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <div class="input-group time">
                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                   value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                   class="form-control"/>
                            <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>
                    <input readonly class="spinner quantity_input" type="text"
                           value="<?php echo $product['minimum']; ?>"/>
                    <div class="bottom-row clearfix">
                        <a class="link-cart"
                           onclick="cart.add('<?php echo $product['product_id']; ?>', $(this).parent().parent().find('.quantity_input').attr('aria-valuenow'), $(this).parent().parent().find('.jcf-label-active > input[type=\'radio\']').attr('name'), $(this).parent().parent().find('.jcf-label-active > input[type=\'radio\']').val() );">
                            <span class="hidden-xs hidden-sm hidden-md text-hidden"><?php echo $button_cart; ?></span>
                            <span class="text-red"><?php if (!$product['special']) { ?>
                                <?php echo $product['price']; ?>
                                <?php } else { ?>
                                <?php echo $product['special']; ?>
                                <?php } ?></span></a>
                        <a data-id="<?php echo $product['product_id']; ?>"
                           class="link-favorite <?php echo $product['class_button']; ?>"
                           title="<?php echo $button_wishlist; ?>" <?php if (empty($product['class_button'])) { ?>
                        onclick="wishlist.add('<?php echo $product['product_id']; ?>');" <?php } else { ?>
                        onclick="wishlist.remove('<?php echo $product['product_id']; ?>');" <?php }  ?>
                        ><?php echo $button_wishlist; ?></a>
                    </div>
                </div>
            </li>

            <?php } ?>
    </section>
    <?php } ?>
</div>

<style>
 legend {
   display: none;
 }
 .g-recaptcha > div {
   margin: auto;
   margin-bottom: 20px;
 }
 legend + .form-group .control-label {
   display: none;
 }
</style>

<?php if(isset($product_microdata) && !empty($product_microdata)): ?>
<script type="application/ld+json">
{
	"@context": "http://schema.org/",
	"@type": "Product",
	"name": "<?php echo $product_microdata['name']; ?>",
	"image": ["<?php echo implode('","',$product_microdata['images']) ?>"],
	"description": "<?php echo $product_microdata['description']; ?>",
	<?php if($ratings['average_rating'] > 0) { ?>
        "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "<?php echo $ratings['average_rating']?>",
        "reviewCount": "<?php echo $ratings['total_reviews']?>"
        },
    <?php } ?>
 	<?php if(!empty($product_microdata['brand'])): ?>
		  "brand": {
			"@type": "Thing",
			"name": "<?php echo $product_microdata['brand']; ?>"
		  },
	<?php endif; ?>
	<?php if(!empty($product_microdata['offers'])): ?>
		"offers": [
			<?php foreach($product_microdata['offers'] as $key => $offer): ?>
				<?php if($key != 0): echo ',';  endif;?>
				{
					"@type": "Offer",
					"priceCurrency": "UAH",
					"price": "<?php echo $offer['price']; ?>",
					"description": "<?php echo $offer['description']; ?>",
					"availability": "http://schema.org/InStock"
				}
			<?php endforeach; ?>
		],
	<?php endif; ?>
	"mpn": "<?php echo $product_microdata['mpn'] ?>"
}
</script>
<?php endif; ?>
<script type="text/javascript"><!--
  $(document).ready(function () {
    $('.link-cart .text-red').each(function () {
      var default_price_value = $(this).closest('.box').find('.jcf-label-active .price .price-new').html();
      $(this).html(default_price_value);
    });
  });

  $('input[type=radio]').change(function () {
    var this_box = $(this).closest('.box');
    var this_price = parseInt($(this).closest('.row-choose').find('.price .price-new').text().replace(' ', ''));
    var this_qnt = parseInt($(this_box).find(".spinner").spinner("value"));

    var calc_price = $.number(this_price * this_qnt, 0, ',', ' ');

    $(this_box).find('.link-cart .cur-value').text(calc_price);
  });

  $(".spinner").on("spin", function (event, ui) {
    str = "";

    var this_box = $(this).closest('.box');
    if (ui.value > 0) {
      if ($(this_box).find('.jcf-label-active .price .price-new').length > 0) {
        var this_price = parseInt($(this_box).find('.jcf-label-active .price .price-new').text().replace(' ', ''));
        var this_qnt = ui.value;

        var calc_price = $.number(this_price * this_qnt, 0, ',', ' ');

        $(this_box).find('.link-cart .cur-value').text(calc_price);
      }
    }
  });
  --></script>
<script type="text/javascript"><!--
  $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
    $.ajax({
      url: 'index.php?route=product/product/getRecurringDescription',
      type: 'post',
      data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
      dataType: 'json',
      beforeSend: function () {
        $('#recurring-description').html('');
      },
      success: function (json) {
        $('.alert, .text-danger').remove();

        if (json['success']) {
          $('#recurring-description').html(json['success']);
        }
      }
    });
  });
  //--></script>
<script type="text/javascript"><!--
  $('.star-rating > li > a').click(function () {
    $(this).parent().addClass("active");
    $(".rating-star").val($(this).attr('title'));
  });

  $('.button-cart').on('click', function () {
    $.ajax({
      url: 'index.php?route=checkout/cart/add',
      type: 'post',
      data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
      dataType: 'json',
      beforeSend: function () {
        $('#button-cart').button('loading');
      },
      complete: function () {
        $('#button-cart').button('reset');
      },
      success: function (json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');

        if (json['error']) {
          if (json['error']['option']) {
            for (i in json['error']['option']) {
              var element = $('#input-option' + i.replace('_', '-'));

              if (element.parent().hasClass('input-group')) {
                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
              } else {
                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
              }
            }
          }

          if (json['error']['recurring']) {
            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
          }

          // Highlight any found errors
          $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
          $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

          $('html, body').animate({scrollTop: 0}, 'slow');

          $('#cart > ul').load('index.php?route=common/cart/info ul li');
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
  //--></script>
<script type="text/javascript"><!--
  $('.date').datetimepicker({
    pickTime: false
  });

  $('.datetime').datetimepicker({
    pickDate: true,
    pickTime: true
  });

  $('.time').datetimepicker({
    pickDate: false
  });

  $('button[id^=\'button-upload\']').on('click', function () {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

    if (typeof timer != 'undefined') {
      clearInterval(timer);
    }

    timer = setInterval(function () {
      if ($('#form-upload input[name=\'file\']').val() != '') {
        clearInterval(timer);

        $.ajax({
          url: 'index.php?route=tool/upload',
          type: 'post',
          dataType: 'json',
          data: new FormData($('#form-upload')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend: function () {
            $(node).button('loading');
          },
          complete: function () {
            $(node).button('reset');
          },
          success: function (json) {
            $('.text-danger').remove();

            if (json['error']) {
              $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
            }

            if (json['success']) {
              alert(json['success']);

              $(node).parent().find('input').val(json['code']);
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    }, 500);
  });
  //--></script>
<script type="text/javascript"><!--
  $('#review').delegate('.pagination a', 'click', function (e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
  });

  $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

  $('#button-review').on('click', function () {
    $.ajax({
      url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
      type: 'post',
      dataType: 'json',
      data: $("#form-review").serialize(),
      beforeSend: function () {
        $('#button-review').button('loading');
      },
      complete: function () {
        $('#button-review').button('reset');
      },
      success: function (json) {
        $('#review-status').html("");

        if (json['error']) {
          $('#review-status').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
        }

        if (json['success']) {
          $('#review-status').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

          $('input[name=\'name\']').val('');
          $('textarea[name=\'text\']').val('');
          $('input[name=\'rating\']:checked').prop('checked', false);
        }
      }
    });
    grecaptcha.reset();
  });

  $(document).ready(function () {
    $('.thumbnails').magnificPopup({
      type: 'image',
      delegate: 'a',
      gallery: {
        enabled: true
      }
    });
  });

  $(document).ready(function () {

    var hash = window.location.hash;
    if (window.location.hash == '#reviews') {
      $('li.tab-reviews').click();
    }
    if (hash) {
      var hashpart = hash.split('#');
      var vals = hashpart[1].split('-');
      for (i = 0; i < vals.length; i++) {
        $('#product').find('select option[value="' + vals[i] + '"]').attr('selected', true).trigger('select');
        $('#product').find('input[type="radio"][value="' + vals[i] + '"]').attr('checked', true).trigger('click');
        $('#product').find('input[type="checkbox"][value="' + vals[i] + '"]').attr('checked', true).trigger('click');
      }
    }
  })
  //--></script>
  <style>
  #ytplayer {
    width: 100%;
    height: 360px;
  }
  @media (min-width: 768px) {
    #ytplayer {
      width: 640px;
      height: 360px;
    }
  }
  </style>
<?php echo $footer; ?>
