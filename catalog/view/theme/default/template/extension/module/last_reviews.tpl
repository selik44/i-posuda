<?php if($products) { ?>
<div class="open-close active">
<strong class="slide-opener">Отзывы покупателей</strong>
    <div class="slide-block">
      <div class="product-reviews">
          <?php foreach($products as $product) { ?>
            <div class="product-reviews">
                <div class="product-reviews__item-row">
                    <div class="product-reviews__item-img">
                        <a href="<?php echo $product['href']; ?>#reviews"> <img src="<?php echo $product['thumb']; ?>" alt=""></a>
                    </div>
                    <div class="product-reviews__info">
                        <div class="product-reviews__info-author"><?php echo $product['author'];?></div>
                        <div class="product-reviews__info-date-added"><?php echo $product['date_added'];?></div>
                        <a class="product-reviews__info-name" href="<?php echo $product['href']; ?>#reviews"><?php echo $product['name'];?></a>
                    </div>
                </div>
                <div class="product-reviews__text">
                    <?php echo $product['review_text'];?>
                    <a class="product-reviews__info-href" href="<?php echo $product['href']; ?>#reviews">Еще</a>
                </div>
            </div>
          <?php } ?>
      </div>
    </div>
</div>
<?php } ?>