            <!-- intro-slider -->
            <div id="slideshow<?php echo $module; ?>" class="intro-slider">
  <?php foreach ($banners as $banner) { ?>
                <a class="slide" style="background:url(<?php echo $banner['image']; ?>)no-repeat;" href="<?php echo $banner['link']; ?>">
                    <div class="text-block">
                        <div class="row">
                            <h2><?php echo $banner['title']; ?></h2>
			    <?php if ($banner['description']) { ?>
                            <p><?php echo $banner['description']; ?></p>
			  <?php } ?>
                        </div>
		    </div>	
		</a>
  <?php } ?>
</div>
