<div class="section-partners">
   <ul class="list-partners">
  <?php foreach ($banners as $banner) { ?>
      <li><span class="center">
      <?php if ($banner['link']) { ?>
      <a target="_blank" href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
      </span>
      </li>
  <?php } ?>
  </ul>
</div>

