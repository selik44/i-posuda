<?php if($products) { ?>
<div class="open-close active">
<strong class="slide-opener">Топ товаров этой категории</strong>
    <div class="slide-block">
      <div class="products-popular">
          <?php foreach($products as $product) { ?>
                <div class="products-popular__item">
                    <div class="popular__item-img">
                        <a href="<?php echo $product['href']; ?>"> <img src="<?php echo $product['thumb']; ?>" alt=""></a>
                    </div>
                    <div class="popular__item-href">
                        <a href="<?php echo $product['href']; ?>"><?php echo $product['name'];?></a>
                    </div>
                </div>
          <?php } ?>
      </div>
    </div>
</div>
<?php } ?>