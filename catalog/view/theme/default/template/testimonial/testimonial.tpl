<?php echo $header; ?>
            <div class="row main"><?php echo $content_top; ?>
                <!-- heading -->
                <div class="heading clearfix tablet-hidden">            <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
                </div>
                <div class="clearfix">
                    <!-- sidebar -->
                    <aside class="sidebar">
                        <nav class="sidebar-nav clearfix">
                            <ul>
								<?php foreach ($informations as $information) { ?>
									<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
								<?php } ?>
                            </ul>
                        </nav>
                    </aside>
                    <!-- content -->
                    <div class="content">
                        <div class="content-holder">
                            <!-- add-testimonial -->
                            <div class="open-close add-testimonial">
                                <a class="button slide-opener" href="">Оставить отзыв</a>
<div class="slide-block js-slide-hidden">
            <form class="form-horizontal" id="form-review">
                <?php if ($review_status) { ?>

                <?php if ($review_guest) { ?>
                <h2><?php echo $text_write; ?></h2>
                                            <div class="form-row clearfix">
        <div class="form-column required">
                 <div class="label-holder"><label for="name"><?php echo $entry_name; ?> <span class='text-red'>*</span></label></div>
                 <div class="input-holder"><input id="name" name="name" placeholder="<?php echo $entry_name; ?>" type="text" value="<?php echo $customer_name; ?>" /></div>
        </div>
</div>
                  <div class="form-row required">
                           <div class="label-holder"><label for="input-review"><?php echo $entry_review; ?> <span class='text-red'>*</span></label></div>
                           <div class="input-holder"><textarea placeholder="<?php echo $entry_review; ?>" cols="30" rows="10" name="text" rows="5" id="input-review" class="form-control"></textarea>
</div>
                  </div>
                                         <div class="stars-block required">
                                                <span class="title"><?php echo $entry_rating; ?> <span class='text-red'>*</span></span>
                                                <ul class="star-rating">
                                                    <li><a href="#" title="1" class="one-star">1</a></li>
                                                    <li><a href="#" title="2" class="two-stars">2</a></li>
                                                    <li><a href="#" title="3" class="three-stars">3</a></li>
                                                    <li><a href="#" title="4" class="four-stars">4</a></li>
                                                    <li><a href="#" title="5" class="five-stars">5</a></li>
                                                </ul>
                        <input class="rating-star" type="hidden" name="rating" value="" />
                                            </div>
                <?php if (isset($site_key) && $site_key) { ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                    </div>
                </div>
                <?php } elseif(isset($captcha) && $captcha){ ?>
                <?php echo $captcha; ?>
                <?php } ?>
<div id="review-text"></div>
                <div class="buttons-row">
                    <button class="button" id="button-review" type="button"><?php echo $button_continue; ?></button>
                    <a class="button button-border-gray slide-opener" href="">Закрыть форму</a>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
                <?php } ?>
            </form></div>
			</div>
                <div id="review"></div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
    <script type="text/javascript"><!--
$('.star-rating > li > a').click(function()
{
$(this).parent().addClass("active");
$(".rating-star").val($(this).attr('title'));
});

        $('#review').delegate('.pagination a', 'click', function (e) {
            e.preventDefault();
            $('#review').load(this.href);
        });

        $('#review').load('<?php echo html_entity_decode($review); ?>');

        $('#button-review').on('click', function () {
            $.ajax({
                url: '<?php echo html_entity_decode($write); ?>',
                type: 'post',
                dataType: 'json',
                data:  $("#form-review").serialize(),
                beforeSend: function () {
                    if ($("textarea").is("#g-recaptcha-response")) {
                        grecaptcha.reset();
                    }
                    $('#button-review').button('loading');
                },
                complete: function () {
                    $('#button-review').button('reset');
                },
                success: function (json) {
                    $('.alert-success, .alert-danger').remove();
                    if (json['error']) {
                        $('#review-text').html('<div class="alert alert-danger"> ' + json['error'] + '</div>');
                    }
					if(json['redirect'])
					{
						location = json['redirect'];
					}
                    if (json['success']) {
						
                        $('#review-text').html('<div class="alert alert-success">' + json['success'] + '</div>');

                        $('input[name=\'name\']').val('');
                        $('textarea[name=\'text\']').val('');
                   //     $('input[name=\'rating\']:checked').prop('checked', false);
                    }
                }
            });
        });
        //--></script>
</div>
<?php echo $footer; ?>
