<?php if ($reviews) { ?>
                                    <ul class="testimonials-list">
<?php foreach ($reviews as $review) { ?>
                                <li>
                                    <div class="meta-info">
                                        <strong class="author"><?php echo $review['author']; ?></strong>
                           <!--             <span class="place"><?php echo $review['date_added']; ?></span>-->
                                    </div>
<p><?php echo $review['text']; ?></p>
                                   <div class="bottom-row">
      <?php if ($review['rating'] > "0") { ?>
                                        <span class="stars stars<?php echo $review['rating']; ?>"></span>
      <?php } ?>
                                        <span class="date"><?php echo $review['date_added']; ?></span>
                                    </div>
<?php if ($review['answer']) { ?>
                                    <div class="answer">
                                        <div class="meta-info">
                                            <strong class="author">iPosuda</strong>
                                        </div>
                                        <p><?php echo $review['answer']; ?></p>
                                        <span class="date"><?php echo $review['date_added']; ?></span>
                                    </div>
<?php } ?>
</li>
<?php } ?>
</ul>
<?php echo $pagination; ?>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
