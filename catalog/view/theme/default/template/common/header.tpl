<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;  ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <?php

if($is_catalog || $is_product) {
    if($is_catalog) { echo '<meta property="og:type" content="product.group" />';
    } else { echo '<meta property="og:type" content="product"/>'; } ?>
    <meta property="og:title" content="<?php echo $title; ?>"/>
    <meta property="og:description" content="<?php echo $description; ?>"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <meta property="og:locale" content="ru_RU"/>
    <meta property="og:site_name" content="Iposuda.com.ua"/>
    <?php if ($image_og) { ?>
    <meta property="og:image" content="<?php echo $image_og; ?>"/>
    <?php } else { ?>
    <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <?php } ?>

    <?php foreach ($metas as $meta) { ?>
    <meta name="<?php echo $meta['name']; ?>" content="<?php echo $meta['content']; ?>" />
    <?php } ?>

    <link href="catalog/view/javascript/bootstrap/css/tooltip.css" type="text/css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,600,700&amp;subset=cyrillic-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=cyrillic-ext" rel="stylesheet">
    <link media="all" rel="stylesheet" href="catalog/view/theme/default/build/application.css?v=190619">

    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>

    <script src="catalog/view/javascript/select2/select2.min.js" ></script>

    <script src="catalog/view/theme/default/build/application.js" type="text/javascript"></script>

    <!--[if IE]><script src="catalog/view/javascript/js/ie.js"></script><![endif]-->
    <script src="catalog/view/javascript/common.js?v=1.5" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>?v=1.0.0.1" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
    <?php if(isset($transactionData) && !empty($transactionData)): ?>
    <script>
      dataLayer = [{
        'transactionId': '<?php echo $transactionData['id'] ?>',
        'transactionAffiliation': '<?php echo $transactionData['affiliation'] ?>',
        'transactionTotal': <?php echo $transactionData['revenue'] ?>,
        'transactionTax': <?php echo $transactionData['tax'] ?>,
      'transactionShipping': <?php echo $transactionData['shipping'] ?>,
      'transactionProducts': [
      <?php foreach ($transactionData['products'] as $key => $product): ?>
      <?php echo $key > 0 ? ',' : ''; ?>
      {
        'sku': '<?php echo $product['sku'] ?>',
        'name': '<?php echo $product['name'] ?>',
        'category': '<?php echo $product['category'] ?>',
        'price': <?php echo $product['price'] ?>,
        'quantity': <?php echo $product['quantity'] ?>
      }
      <?php endforeach; ?>
      ]
      }];
    </script>
    <?php endif; ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-NNN7TDQ');</script>
    <!-- End Google Tag Manager -->

    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "<?php echo HTTP_SERVER; ?>",
  "logo": "<?php echo $logo; ?>"
}
</script>
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Store",
  "@id": "<?php echo HTTP_SERVER; ?>",
  "name": "<?php echo $name; ?>",
  "image": ["<?php echo $logo; ?>"],
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "<?php echo $address; ?>",
    "addressLocality": "Одесса",
    "addressRegion": "Одесса",
    "addressCountry": "UA"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 46.487988,
    "longitude": 30.733824
  },
  "telephone": "<?php echo $telephone; ?>"
}
</script>
    <?php if(isset($custom_code) && !empty($custom_code)):
echo $custom_code;
endif; ?>
</head>

<body class="<?php echo $class; echo (isset($checkout_header) && $checkout_header) ? ' plain-page' : ''; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNN7TDQ"
                  height="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="wrapper">
    <div class="w1">
        <!-- header -->
        <header class="header open-close">
            <?php if(isset($checkout_header) && $checkout_header): ?>
            <div class="row">
                <div class="logo-block clearfix">
                    <strong class="logo"><a href="<?php echo $home; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>">iPosuda</a></strong>
                    <ul class="phones">
                        <li>
                            <a class="ks" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone); ?>"><span class="text-gray"><?php echo $telephone; ?></a>
                        </li>
                        <li>
                            <a class="mts" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone_m); ?>"><span class="text-gray"><?php echo $telephone_m; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <?php else: ?>
            <!-- top-block -->
            <div class="top-block">
                <div class="row">
                    <ul class="phones">
                        <li>
                            <a class="ks" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone); ?>"><span class="text-gray"><?php echo $telephone; ?></a>
                        </li>
                        <li>
                            <a class="mts" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone_m); ?>"><span class="text-gray"><?php echo $telephone_m; ?></a>
                        </li>
                    </ul>
                    <ul class="sign-nav">
                        <?php if ($logged) { ?>
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                        <?php } else { ?>
                        <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                        <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php echo $search; ?>
                </div>
            </div>
            <div class="row">
                <div class="logo-block clearfix">
                    <a class="mobile-opener slide-opener" href="">Меню</a>
                    <strong class="logo"><a href="<?php echo $home; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>">iPosuda</a></strong>
                    <ul class="tools">
                        <li>
                            <a class="favorite <?php if ($text_wishlist > 0) { ?>active <?php } ?> favorite" href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><?php echo $text_wishlist; ?></a>
                        </li>
                        <li>
                            <?php echo $cart; ?>
                        </li>
                    </ul>
                </div>
                <!-- nav -->
                <div class="nav-bar">
                    <?php echo $revmenu; ?>
                    <nav class="nav">
                        <ul>
                            <li><a href="<?php echo $home; ?>">Главная</a></li>
                            <li><a href="/customer/">Уголок покупателя</a></li>
                            <li><a href="/cooperation/">Сотрудничество</a></li>
                            <!--  <li><a href="/news/">Новости</a></li>  -->
                            <li><a href="/testimonials/">Отзывы</a></li>
                            <li><a href="/contact/">Контакты</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <?php endif; ?>
        </header>
