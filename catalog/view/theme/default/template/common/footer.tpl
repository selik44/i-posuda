        </div>
    </div>
    <!-- footer -->
    <footer class="footer">
        <div class="row">
            <div class="top-block clearfix">
                <nav class="footer-nav">
                            <ul>
                                <li><a href="/">Главная</a></li>
                                <li><a href="/customer">Уголок покупателя</a></li>
                                <li><a href="/cooperation">Сотрудничество</a></li>
                               <!-- <li><a href="/news">Новости</a></li> -->
                                <li><a href="/testimonials">Отзывы</a></li>
                                <li><a href="/contact">Контакты</a></li>
                                <li><a href="/blog/">Блог</a></li>
                                <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                            </ul>
                </nav>
                <!--?php var_dump(strpos($_SERVER['REQUEST_URI'], 'blog')); ?-->
                <?php if(strpos($_SERVER['REQUEST_URI'], 'blog') == false || strpos($_SERVER['REQUEST_URI'], 'news')){ ?>
                <div class="block-location popup-holder">
                    <a class="opener" href=""><span class="inner"><?php if (isset($city) && $city != 'Опт') { echo $city; } else {?> Выбрать город <?php } ?></span></a>
                    <div class="popup">
                        <ul>
		<?php foreach ($categories as $category) { ?>
                        <li><a class="close-link" <?php echo $category['href'] ? 'href="' .$category['href'] .'"' : '' ;  ?> data-city="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></a></li>

		<?php } ?>
                        </ul>
                    </div>
                </div>
        <?php } ?>
<script><!--
$('.close-link').on('click', function() {
	$.ajax({
		url: 'index.php?route=common/city&city_id=' + $(this).attr('data-city'),
		dataType: 'json',
		success: function(json) {
location.reload();
//location = 'index.php?route=common/home';
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});
--></script>
                <div class="right-block">
                    <ul class="phones">
                        <li>
                            <a class="ks" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone); ?>"><?php echo $telephone; ?></a>
                        </li>
                        <li>
                            <a class="mts" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone_mts); ?>"><?php echo $telephone_mts; ?></a>
                        </li>
                    </ul>
                    <a class="email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                </div>
            </div>
            <div class="bottom-block clearfix">
                <a class="by" href="https://seomasters.com.ua/"><img src="images/logo-footer.png" alt="image" width="116" height="47"/></a>
                <span class="copyright">© 1999–<?php echo date('Y'); ?> All rights reserved.</span>
                <div class="right-block">
                    <a class="email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                    <ul class="phones">
                        <li>
                            <a class="ks" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone); ?>"><?php echo $telephone; ?></a>
                        </li>
                        <li>
                            <a class="mts" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone_mts); ?>"><?php echo $telephone_mts; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
	<div class="mfp-fast-popup mfp-hide"><div class="mfp-fast-popup-content"></div></div>
	<script>
	function quantity_control() {
	$('.quantity_input').each(function() {
	$(this).attr('readonly', 'readonly');
	var minimum = $(this).val();
	var maximum = $(this).attr('data-maximum');
	if(maximum <= 0) {
		$(this).val('0');
		$(this).parent().parent().find('.button-group').children().first().attr('disabled', 'disabled');
		if ($('.form-group').length !=0) {
			$(this).parent().parent().find('#button-cart').attr('disabled', 'disabled');
		}
		var text = '';
	} else {
		var text = ''
	}

	$(this).next().click(function () {
	if ((~~$(this).prev().val()+ ~~minimum) <= ~~maximum) {
		$(this).prev().val(~~$(this).prev().val()+ ~~minimum);
	} else {
	/*if ($(this).parent().find('.stock_warning').length ==0) { $(this).parent().append($('<span class="stock_warning">На нашем складе ' + text + '</span>').fadeIn()); }
	$(this).parent().find('.stock_warning').fadeIn().delay('2000').fadeOut();*/
	}
	});
	$(this).prev().click(function () {
	if ($(this).next().val() > ~~minimum) {
		$(this).next().val(~~$(this).next().val()- ~~minimum);
	}
	});
	});
	}
	$(document).ready(function() {
		quantity_control();
	});
	</script>
</body>
</html>
