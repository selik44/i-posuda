                        <div class="catalog-block">
		<div id="menu2_button" <?php if ($module_class) { ?>class="<?php echo $module_class; ?>"<?php } ?>>
			<div class="btn btn-primary" ><span class="button"><span class="inner"><?php echo $heading_title; ?></span></span><span class="icorightmenu"><i class="fa fa-chevron-down"></i></span></div>
			<div class="box-content am collapse navbar navbar-revmenu-collapse">
				<div id="menu2" <?php if ($module_class) { ?>class="<?php echo $module_class; ?>"<?php } ?>>
					<div class="catalog_list catalog_list_popup catalog_as_popup">
						<?php foreach ($categories as $category) { ?>
							<div class="level_1 closed <?php echo !empty($category['children']) ? 'hasChildren' : '' ?>">
							<?php if ($category['top'] !=1) { ?>
								<?php if ($category['children']) { ?>
									<div class="title with-child">
									<a style="background:url(<?php echo $category['thumb']; ?>) 0 12px no-repeat;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?>
									<span class="arrow-btn hidden-xs hidden-sm"><i class="fa fa-angle-right"></i></span>
									</a>
									</div>
								<?php } else { ?>
									<div class="title">
									<a style="background:url(<?php echo $category['thumb']; ?>) 0 12px no-repeat;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
									</div>
								<?php } ?>
								<?php if ($category['children']) { ?>
								<div class="childrenList hidden-xs hidden-sm" style="display: none;">
									<?php if ($category['column'] == 1) { ?>
										<?php	$box_class = 'box-col-1'; $col_class = 'col-1'; ?>
									<?php } elseif ($category['column'] == 2) { ?>
										<?php	$box_class = 'box-col-2'; $col_class = 'col-2'; ?>
									<?php } elseif ($category['column'] == 3) { ?>
										<?php	$box_class = 'box-col-3'; $col_class = 'col-3'; ?>
									<?php } else { ?>
										<?php	$box_class = 'box-col-4'; $col_class = 'col-4'; ?>
									<?php } ?>
									<div class="child-box <?php echo $box_class; ?>">
									<ul class="ul_block<?php echo $category['category_id']; ?> <?php echo $col_class; ?>">
									<?php foreach ($category['children'] as $child) { ?>
									<li class="glavli"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
									</li>
									<?php } ?>
									</ul>
									</div>
								</div>
								<?php } ?>
							<?php } ?>
							</div>
							<?php } ?>

							<div class="level_1 closed hasChildren">
								<div class="title with-child">
									<a style="background:url(<?php echo $category['thumb']; ?>) 0 12px no-repeat;" href="/brands/">Бренды
										<span class="arrow-btn hidden-xs hidden-sm"><i class="fa fa-angle-right"></i></span>
									</a>
								</div>
								<div class="childrenList hidden-xs hidden-sm" style="display: none;">
									<?php if ($category['column'] == 1) { ?>
									<?php	$box_class = 'box-col-1'; $col_class = 'col-1'; ?>
									<?php } elseif ($category['column'] == 2) { ?>
									<?php	$box_class = 'box-col-2'; $col_class = 'col-2'; ?>
									<?php } elseif ($category['column'] == 3) { ?>
									<?php	$box_class = 'box-col-3'; $col_class = 'col-3'; ?>
									<?php } else { ?>
									<?php	$box_class = 'box-col-4'; $col_class = 'col-4'; ?>
									<?php } ?>
									<div class="child-box <?php echo $box_class; ?>">
										<ul class="ul_block<?php echo $category['category_id']; ?> <?php echo $col_class; ?>">
											<?php foreach ($manufacturers as $manufacturer) { ?>
											<li class="glavli"><a title="Посуда <?php echo $manufacturer['name']; ?>" href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a>
											</li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>

						<div class="level_1 closed">
								<div class="title">
									<a class="discounted" href="specials/">Товары со скидкой</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
                <div class="mobile-nav slide-block" style="display: none;">
				<?php echo $search; ?>
				<div class="catalog-list">
						<?php foreach ($categories as $category) { ?>
							<div class="level_1 closed <?php echo !empty($category['children']) ? 'hasChildren' : '' ?>">
							<?php if ($category['top'] !=1) { ?>

								<?php if ($category['children']) { ?>
									<a class="slide-opener-inner mobile-open" style="background:url(<?php echo $category['thumb']; ?>) 0 12px no-repeat;" ><?php echo $category['name']; ?>
									<span class="arrow-btn hidden-xs hidden-sm"><i class="fa fa-angle-right"></i></span>
									</a>
								<?php } else { ?>
									<div class="title">
									<a style="background:url(<?php echo $category['thumb']; ?>) 0 12px no-repeat;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
									</div>
								<?php } ?>
								<?php if ($category['children']) { ?>
								<div class="slide-block-inner js-slide-hidden">
									<ul class="ul_block<?php echo $category['category_id']; ?> <?php echo $col_class; ?>">
									<?php foreach ($category['children'] as $child) { ?>
									<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
									</li>
									<?php } ?>
									</ul>
								</div>
								<?php } ?>
							<?php } ?>
							</div>
							<?php } ?>
							<div class="level_1 closed">
								<div class="title">
									<a class="discounted" href="specials/">Товары со скидкой</a>
								</div>
							</div>
				</div>
		<nav class="nav">
                        <ul>
                            <li><a href="<?php echo $home_link; ?>">Главная</a></li>
							<li><a href="/customer/">Уголок покупателя</a></li>
							<li><a href="/cooperation/">Сотрудничество</a></li>
						<!--	<li><a href="/news/">Новости</a></li> -->
							<li><a href="/testimonials/">Отзывы</a></li>
							<li><a href="/contact/">Контакты</a></li>
                        </ul>
                    </nav>

					<ul class="sign-nav">
          		   <?php if ($logged) { ?>
        		    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
    		           <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
          		   <?php } else { ?>
   		           <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
      		           <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
         		   <?php } ?>
                        </ul>
                    <ul class="phones">
                         <li>
							<a class="ks" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone); ?>"><?php echo $telephone; ?></a>
						</li>
						<li>
							<a class="mts" href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone_m); ?>"><?php echo $telephone_m; ?></a>
						</li>
                    </ul>
		</div>
<script><!--
$(".mobile-open").on("click", function () {
if ($(this).next().hasClass("js-slide-hidden")) {;
$(this).next().removeClass("js-slide-hidden");
} else {
$(this).next().addClass("js-slide-hidden");
}
});

function autocol_cats(cat_id, cat_col) {
	$('.ul_block'+cat_id).autocolumnlist({
		columns: cat_col,
		min: 1
	});
}
<?php foreach ($categories as $category) { ?>
	autocol_cats(<?php echo $category['category_id']; ?>, <?php echo $category['column']; ?>);
<?php } ?>
//--></script>
