<div class='row'>
    <div class="row">
        <section class='cats-on-main'>
            <?php foreach ($categories as $key => $category): ?>
                <?php if($key == 0 || $key == 4): ?>
                    <div class="cats-line">
                <?php endif; ?>
                        <div class='category-block category-block-back-<?php echo $category['category_id'] ?>'>
                            <div class='category-block-title'>
                                <img class="category-block-thumb" alt="<?php echo $category['title']; ?>" src="<?php echo $category['thumb']; ?>">
                                <a href="<?php echo $category['href']; ?>" title="<?php echo $category['title']; ?>"><?php echo $category['name']; ?></a>

                            </div>
                            <?php if (isset($category['children'])): ?>
                                <ul class='category-subblock subblock-hide'>
                                    <?php foreach ($category['children'] as $child): ?>
                                    <li>
                                        <a href='<?php echo $child['href']; ?>'><?php echo $child['name']; ?></a>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                <?php if($key == 3 || $key == count($categories) - 1): ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <div class='block-discount'>
                <a href="specials/"><span class='block-discount-image'></span>Товары со скидкой</a>
            </div>
</div>
<script>
    $(document).ready(function(){
        if($(window).width() > 767) {
            jQuery('.cats-on-main .cats-line').sameHeight({
                elements: '.category-block',
                multiLine: true
            });
        }
    });

    $(document).on('click','.category-block-title', function(e){
        var thisSublock = $(this).closest('.category-block')
                .find('.category-subblock');
        $(this).closest('.cats-on-main')
                .find('.category-subblock.subblock-show').not(thisSublock)
                .removeClass('subblock-show')
                .addClass('subblock-hide')

        if($(thisSublock).hasClass('subblock-hide')) {
            if($(window).width() < 769) {
                e.preventDefault();
            }
            $(thisSublock).removeClass('subblock-hide')
                      .addClass('subblock-show');
        } else {
            $(thisSublock).removeClass('subblock-show')
                      .addClass('subblock-hide');
        }
    });
</script>