<?php echo $header; ?>
            <div class="row main">
                <div class="block-success">
                    <img class="aligncenter" width="78" height="96" src="images/ico13@2x.png" alt="success"/>
					
					<?php if(isset($testimonial) && $testimonial): ?>
						<p>Спасибо за оставленный отзыв, <br/>после модерации он появится на нашем сайте!</p>
					<?php else: ?>
						<h2>Здравствуйте, <span class="text-red"><?php echo $name; ?>!</span></h2>
						<p><span class="text-black">Ваш заказ № <?php echo $order_id; ?>  в обработке.</span> Для подтверждения заказа наш менеджер свяжется с Вами в ближайшее время. Статус заявки вы можете отследить в своем личном кабинете.</p>
					<?php endif; ?>
					
                    <p class="text-black">Будем рады ответить на ваши вопросы по телефонам:</p>
                    <ul class="phones-success">
                        <li>
                            <a href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone); ?>"><?php echo $telephone; ?></a>
                        </li>
                        <li>
                            <a href="tel:+<?php echo preg_replace("/[^,.0-9]/", '', $telephone_mts); ?>"><?php echo $telephone_mts; ?></a>
                        </li>
                    </ul>
                    <a class="button" href="/">На главную</a>
                </div>
            </div>
      <?php echo $content_bottom; ?>
<?php echo $footer; ?>
