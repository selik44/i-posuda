<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
                                    <div class="slide-block">
    <fieldset id="address">
<input type="hidden" name="address_id" value="<?php echo $address_id; ?>">
<input type="hidden" name="country_id" class='qwe' value="<?php echo $country_id; ?>">
<?php /*
      <div class="form-group required">
        <div class="label-holder"><label class="control-label" for="input-payment-country"><?php echo $entry_country; ?></label></div>
        <select name="country_id" id="input-payment-country" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($countries as $country) { ?>
          <?php if ($country['country_id'] == $country_id) { ?>
          <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
      </div>
	  */ ?>

      <div class="form-group" style="height:0;overflow: hidden;margin:0;">
        <label class="control-label" for="input-payment-postcode"><?php echo $entry_postcode; ?></label>
        <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-payment-postcode" class="form-control" />
      </div>
      <div class="form-group required" style="display: none;">
        <label class="control-label" for="input-payment-address-1"><?php echo $entry_address_1; ?></label>
        <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-payment-address-1" class="form-control" />
      </div>
      <?php foreach ($custom_fields as $custom_field) { ?>
      <?php if ($custom_field['location'] == 'address') { ?>
      <?php if ($custom_field['type'] == 'select') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
        <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
          <?php } ?>
        </select>
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'radio') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label"><?php echo $custom_field['name']; ?></label>
        <div id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>">
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <div class="radio">
            <label>
              <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
              <?php echo $custom_field_value['name']; ?></label>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'checkbox') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label"><?php echo $custom_field['name']; ?></label>
        <div id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>">
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
              <?php echo $custom_field_value['name']; ?></label>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'text') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
        <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'textarea') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
        <textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo $custom_field['value']; ?></textarea>
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'file') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label"><?php echo $custom_field['name']; ?></label>
        <br />
        <button type="button" id="button-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
        <input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" />
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'date') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
        <div class="input-group date">
          <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'time') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
        <div class="input-group time">
          <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
      <?php } ?>
      <?php if ($custom_field['type'] == 'datetime') { ?>
      <div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
        <label class="control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
        <div class="input-group datetime">
          <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
      <?php } ?>
      <?php } ?>
      <?php } ?>
    </fieldset>
</div>
<?php if ($shipping_methods) { ?>
<div class="box-inner">
<?php foreach ($shipping_methods as $shipping_method) { ?>
<?php if (!$shipping_method['error']) { ?>
<?php $i=1; foreach ($shipping_method['quote'] as $quote) { ?>
<div class="row-radio">
  <label for="shipping_<?php echo $quote['code']; ?>" >
	<?php $shipping_custom_class = 'hidden'; ?>

<?php if ($quote['code'] == $code || !$code) { $i++; ?>
    <?php $code = $quote['code']; ?>
		<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="shipping_<?php echo $quote['code']; ?>" checked="checked" />
		<?php $shipping_custom_class = 'showing'; ?>
    <?php } else { ?>
		<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="shipping_<?php echo $quote['code']; ?>" />
    <?php } ?>
		<div class="shipping_text" ><?php echo $quote['title']; ?></div>

    <?php if (isset($quote['description'])) { ?>
    <br /><small><?php echo $quote['description']; ?></small>
    <?php } ?>
  </label>
  <?php if($quote['code'] == 'new_post.new_post'): ?>
            <div id="address">
                <input type="hidden" name="zone_id" value="3501">
                <div class="form-group block-shipping-field">
                      <select name="input-newpost-type" id="input-newpost-type" class="form-control">
                          <?php if(!empty($newpost_type) && $newpost_type == "department") { ?>
                          <option value="department" selected='selected'>На отделение</option>
                          <option value="by-address">Доставка курьером</option>
                          <?php } else { ?>
                          <option value="department">На отделение</option>
                          <option value="by-address" selected='selected'>Доставка курьером</option>
                          <?php }?>
                      </select>
                </div>

                <div class="form-group required block-shipping-field">
                    <?php /*
                      <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-payment-city" class="form-control" />
                     * */ ?>
                    <select class="selectSearch" name="city" id="select-np-city">
                        <?php  foreach($npCities as $ref => $cityName): ?>
                            <option <?php echo ($address_2 == $cityName) ? 'selected="selected"' : ''; ?> value="<?php echo $cityName ?>"><?php echo $cityName; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="shipping_custom_field department new_post showing " id="shipping_custom_address_wrap">
               <select name="shipping-custom-address" id="shipping_custom_address" class="selectSearch">
                    <?php if(empty($address_1)) { ?>
                    <option value="">Выберите отделение</option>
                    <?php } else { ?>
                    <option value = "<?php echo $address_1 ?>"><?php echo $address_1 ?></option>
                    <?php } ?>
                </select>
            </div>


            <div class="shipping_custom_field by-address new_post hidden" id="shipping_custom_address2_wrap">
                    <input type="text" id="shipping_custom_address" name="shipping-custom-address" value="<?php echo ($address_1 == '---') ? '' : $address_1 ; ?>" placeholder="Адрес доставки" class="form-control" >
            </div>
	<?php elseif($quote['code'] == 'citylink.citylink'): ?>
		<div class="shipping_custom_field  <?php echo $shipping_custom_class; ?>">
			<input type="text" id="shipping_custom_address" name="shipping-custom-address" value="<?php echo ($address_1 == '---') ? '' : $address_1 ; ?>" placeholder="Адрес доставки" class="form-control" >
		</div>
	<?php elseif($quote['code'] == 'free.free'): ?>
		<div class="shipping_custom_field  <?php echo $shipping_custom_class; ?>">
			<input type="text" id="shipping_custom_address" name="shipping-custom-address" value="<?php echo ($address_1 == '---') ? '' : $address_1 ; ?>" placeholder="Удобный способ доставки" class="form-control" >
		</div>
	<?php endif; ?>
</div>
<?php } ?>
<?php } else { ?>
<div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
<?php } ?>
<?php } ?>
</div>
<?php } ?>
<div class="button-right">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" data-loading-text="<?php echo $text_loading; ?>" class="button" />
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    var selectedCity = $('select.selectSearch').val();
    var custom_address = "<?php echo $address_1; ?>";
    var newpost_type = "<?php echo $newpost_type; ?>";
    if (newpost_type != "department") {
                $('#shipping_custom_address2_wrap').removeClass('hidden').addClass('showing');
                $('#shipping_custom_address_wrap').removeClass('showing').addClass('hidden');
        }
    var fInputNewpostType = function () {
        var city = $('select.selectSearch').val();
        if(city) {
            $.ajax({
                url: 'index.php?route=checkout/shipping_method/newPostWarehous',
                type: 'post',
                data: {
                    city_name: city
                },
                dataType: 'json',
                success: function (json) {
                    if(custom_address) {
                        addToDepartmentNew(json, custom_address);
                    } else {
                        addToDepartment(json);
                    }

                }
            });
        }
        if ($(this).val() === "by-address") {
        } else {      }
    };
    $('#input-newpost-type').on('selectric-select', fInputNewpostType);
    if ($(window).width() < 1030) {
        console.log($(window).width());
        $('#input-newpost-type').on('change', fInputNewpostType);
   }

    $('input#input-payment-address-1').val(selectedCity);
    if(newpost_type === "department") {
        $.ajax({
            url: 'index.php?route=checkout/shipping_method/newPostWarehous',
            type: 'post',
            data: {
                city_name: selectedCity
            },
            dataType: 'json',
            success: function (json) {
                if (custom_address) {
                    addToDepartmentNew(json, custom_address);
                } else {
                    addToDepartment(json);
                }
            }
        });
    }
});

$('select#select-np-city, select#shipping_custom_address').select2();
$('select#select-np-city').on('change', function(){
    var selectedCity = $(this).val();
    $('input#input-payment-address-1').val(selectedCity);
    $.ajax({
            url: 'index.php?route=checkout/shipping_method/newPostWarehous',
            type: 'post',
            data: {
                city_name: selectedCity

            },
            dataType: 'json',
            success: function(json) {
                addToDepartment(json);
            }
    });
});

/*$('select').not('.selectSearch').selectric({
    onChange: function() {
        $('selectric').addClass('change');

    }
});*/

$('select#shipping_custom_address').on('change', function(){
    $('input#input-payment-address-1').val($(this).val());
});



$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});
</script>
<script type="text/javascript">
// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('#collapse-shipping-method input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('#collapse-shipping-method .custom-field').hide();
			$('#collapse-shipping-method .custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#payment-custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#payment-custom-field' + custom_field['custom_field_id']).addClass('required');
				}
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

</script>
<script type="text/javascript">
$('#collapse-shipping-method button[id^=\'button-payment-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
</script>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
</script>
<script type="text/javascript">
$('#collapse-shipping-method select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#collapse-shipping-method select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#collapse-shipping-method input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('#collapse-shipping-method input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('#collapse-shipping-method select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-shipping-method select[name=\'country_id\']').trigger('change');

$('input#input-payment-city').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=checkout/shipping_method/newPostCity',
			type: 'post',
			data: { city_name: request },
			dataType: 'json',
			beforeSend: function()
			{
				$('input#input-payment-city').attr('data-ref','');
                                frechDepartmetns();

			},
			success: function(json) {

				response($.map(json, function(item, key) {
					return {
						label: item,
						value: key
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input#input-payment-city').val(item['label']);
		$('input#input-payment-city').attr('data-ref',item['value']);

                $.ajax({
			url: 'index.php?route=checkout/shipping_method/newPostWarehous',
			type: 'post',
			data: {
                            city_name: item['label'],
                            city_ref: item['value']
                        },
			dataType: 'json',
			success: function(json) {
                            addToDepartment(json);
			}
		});
	}
});

function addToDepartment(departments){
    var depSelect = $(document).find('select#shipping_custom_address');
    var options = '';
    $.each(departments, function(){
        options += '<option value = "' + this.address + '">' + this.address +'</option>';
    });
    //$(depSelect).html(options).selectric('refresh');
    $(depSelect).html(options).select2();
    $('input#input-payment-address-1').val($(depSelect).val());
}

function addToDepartmentNew(departments, custom_address){
    var depSelect = $(document).find('select#shipping_custom_address');
    var options = '';
    $.each(departments, function(){
if(custom_address === this.address) {
    options += '<option selected="selected" value = "' + this.address + '">' + this.address + '</option>';
} else {
    options += '<option value = "' + this.address + '">' + this.address + '</option>';
}
    });
    //$(depSelect).html(options).selectric('refresh');
    $(depSelect).html(options).select2();
   // $('input#input-payment-address-1').val($(depSelect).val());
}
function frechDepartmetns(){
    $(document)
        .find('select#shipping_custom_address')
        .html('<option value="">Выберите отделение</option>')
        //.selectric('refresh');

    $('input#input-payment-address-1').val('');
}
/*$('.new_post input#shipping_custom_address').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=checkout/shipping_method/newPostWarehous',
			type: 'post',
			data: {
				address: request,
				city_name: $('input#input-payment-city').val()
			},
			dataType: 'json',
			success: function(json) {

				response($.map(json, function(item) {
					return {
						label: item['address'],
						value: item['address']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('.new_post input#shipping_custom_address').val(item['label']);
		$('.new_post input#shipping_custom_address').trigger('change');
	}
});*/

$(document).ready(function(){

	//jcf.replaceAll();
	jcf.customForms.replaceAll();
	addShippingToSummary();
	showHideRegionCity();

        $("#input-newpost-type").selectric({
            onChange: function(element){
                var rawRadio = $(this).closest('.row-radio');
                $(rawRadio).find('.shipping_custom_field.showing').removeClass('showing').addClass('hidden');
                $(rawRadio).find('.shipping_custom_field.'+$(this).val()).removeClass('hidden').addClass('showing');
                $('#shipping_custom_address2_wrap input').val("");
            }
        });

    $("#input-newpost-type").on("change", function(element){
            var rawRadio = $(this).closest('.row-radio');
            $(rawRadio).find('.shipping_custom_field.showing').removeClass('showing').addClass('hidden');
            $(rawRadio).find('.shipping_custom_field.'+$(this).val()).removeClass('hidden').addClass('showing');
    });

});




</script>