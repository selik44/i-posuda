                <div class="order-step order-step1">
    <input type="button" value="Продолжить без регистрации" id="button-account-noreg" data-loading-text="Продолжить без регистрации" class="button button-border2" />
                    <div class="clearfix">
                        <div class="block-register-advantages left-block">
                            <h2>Преимущества регистрации</h2>
                            <ul class="listing-big">
                                <li>Накопительная система скидок</li>
                                <li>Отслеживание статуса выполнения заказа</li>
                                <li>История заказов</li>
                                <li>Специальные предложения и многое другое</li>
                            </ul>
                            <div class="tooltip-wrapper">
                                <input type="button" value="Зарегистрироваться" id="button-account" data-loading-text="Зарегистрироваться" class="button" />
                                <div class="tooltip-item tooltip-item-btn">
                                    <span>Заказ оформляется на 10 <br>секунд дольше</span>
                                </div>
                            </div>
                        </div>
                        <div class="right-block">
                            <form class="form-login" action="#">
                                <fieldset>
                                    <h2>Войти</h2>
                                    <p>Если Вы уже зарегистрированы</p>
                                    <div class="input-holder email">
                                        <input  name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" type="text"/>
                                    </div>
                                    <div class="input-holder password"><input name="password" id="input-password" placeholder="<?php echo $entry_password; ?>" type="password"/></div>
                                    <div class="bottom-row clearfix">
    <input type="button" value="<?php echo $button_login; ?>" id="button-login" class="button" />
                                        <a class="link-right" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
