<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class=""><?php echo $content_top; ?>
        <div class="content-holder">
            <!-- heading -->
            <div class="heading clearfix">
                <h1><?php echo $heading_title; ?></h1>
                <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <?php $i = 1; ?>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                                <?php if ($i < count($breadcrumbs)) { ?>
                                <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                                <?php if ($i < count($breadcrumbs)) { ?>
                                </a>
                            <?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
                        <?php $i++; ?>
                    <?php } ?>
                </ol>
            </div>
            <div class="panel-group order-step order-sttep-1" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapse-checkout-option" aria-expanded="true">
                        <div class="panel-body"></div>
                    </div>
                </div>
            </div>
            <div class="order-step order-step-2" style="display:none;">
                <div class=" left-block">
                    <?php if (!$logged && $account != 'guest') { ?>
                        <div class="panel panel-default order-tab">
                            <div class="panel-heading headline clearfix">
                                <h2 class="slide-opener panel-title"><span class="num">1</span> Контактные данные</h2>
                                <a class="link-right text-red" href="">Редактировать</a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse-payment-address">
                                <div class="panel-body"></div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="panel panel-default order-tab">
                            <div class="panel-heading headline clearfix">
                                <h2 class="slide-opener panel-title"><span class="num">1</span> Контактные данные</h2>
                                <a class="link-right text-red" href="">Редактировать</a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse-payment-address">
                                <div class="panel-body"></div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($shipping_required) { ?>
                        <div class="panel panel-default  order-tab">
                            <div class="panel-heading  headline clearfix">
                                <h2 class="panel-title slide-opener "><?php echo $text_checkout_shipping_method; ?></h2>
                                <a class="link-right text-red" href="">Редактировать</a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse-shipping-method">
                                <div class="panel-body"></div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="panel panel-default order-tab">
                        <div class="panel-heading headline clearfix">
                            <h2 class="panel-title slide-opener "><?php echo $text_checkout_payment_method; ?></h2>
                            <a class="link-right text-red" href="">Редактировать</a>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-payment-method">
                            <div class="panel-body"></div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default right-block">
                    <div class="panel-heading">
                        <h2 class="panel-title"><?php echo $text_checkout_confirm; ?></h2>
                    </div>

                    <ul class="list-summary">
                        <li>
                            <div class="column">
                                <span class="text-red"><?php echo count($products); ?></span> <?php if (count($products) == 1) { ?> товар <?php } else { ?> товара <?php } ?>на сумму
                            </div>

                            <div class="column">
                                <span class="cost"><span class="text-red sum">
                                        <?php if (isset($totals)): ?>
                                            <?php $total = end($totals);
                                            echo $total['text']; ?>
                                        <?php else: ?>
                                        <?php echo $product_total; ?></span> <sup>грн.</sup>
<?php endif; ?>
                                </span>
                        </li>
                        <li id="list-summary-shipping" style="display: none;">
                            <div class="column">
                                Способ доставки:
                            </div>
                            <div id="list-summary-shipping-value" class="column">
                            </div>
                        </li>
                        <li id="list-summary-payment" style="display: none;">
                            <div class="column">
                                Способ оплаты:
                            </div>
                            <div id="list-summary-payment-value" class="column">
                            </div>
                        </li>
                    </ul>
                    <div class="panel-collapse collapse" id="collapse-checkout-confirm">
                        <div class="panel-body">

                        </div>
                    </div>

                </div>
            </div>
        <?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>
</div>
<script type="text/javascript">
    /*$(document).on('click','.mobile-opener.slide-opener',function(){
     $('.mobile-nav.slide-block.js-slide-hidden').removeClass('js-slide-hidden');
     });*/
    $(document).on('change', 'input[name=\'account\']', function () {
	if ($('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
	    if (this.value == 'register') {
		$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
	    } else {
		$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
	    }
	} else {
	    if (this.value == 'register') {
		$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_account; ?>');
	    } else {
		$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_address; ?>');
	    }
	}
    });

<?php if (!$logged) { ?>
        $(document).ready(function () {
    	$.ajax({
    	    url: 'index.php?route=checkout/login',
    	    dataType: 'html',
    	    success: function (html) {
    		$('#collapse-checkout-option .panel-body').html(html);

    		$('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><i class="fa fa-caret-down"></i></a>');

    		$('a[href=\'#collapse-checkout-option\']').trigger('click');
    	    },
    	    error: function (xhr, ajaxOptions, thrownError) {
    		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    	    }
    	});
        });
<?php } else { ?>
        $(document).ready(function () {
    	$('.order-step-2').show();
    	$.ajax({
    	    url: 'index.php?route=checkout/payment_address',
    	    dataType: 'html',
    	    success: function (html) {
    		$('#collapse-payment-address .panel-body').html(html);

    		$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');

    		$('a[href=\'#collapse-payment-address\']').trigger('click');
    	    },
    	    error: function (xhr, ajaxOptions, thrownError) {
    		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    	    }
    	});
        });
<?php } ?>

// Checkout
    $(document).delegate('#button-account', 'click', function () {
	$.ajax({
	    url: 'index.php?route=checkout/register',
	    dataType: 'html',
	    beforeSend: function () {
		$('#button-account').button('loading');
	    },
	    complete: function () {
		$('#button-account').button('reset');
		$('.order-step-2').show();
	    },
	    success: function (html) {
		$('.alert, .text-danger').remove();

		$('#collapse-payment-address .panel-body').html(html);

		$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');

		$('a[href=\'#collapse-payment-address\']').trigger('click');
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });
    $(document).delegate('#button-account-noreg', 'click', function () {
	$.ajax({
	    url: 'index.php?route=checkout/guest',
	    dataType: 'html',
	    beforeSend: function () {
		$('#button-account-noreg').button('loading');
	    },
	    complete: function () {
		$('#button-account-noreg').button('reset');
		$('.order-step-2').show();
	    },
	    success: function (html) {
		$('.alert, .text-danger').remove();

		$('#collapse-payment-address .panel-body').html(html);

		$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');

		$('a[href=\'#collapse-payment-address\']').trigger('click');
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });
// Login
    $(document).delegate('#button-login', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');
	$.ajax({
	    url: 'index.php?route=checkout/login/save',
	    type: 'post',
	    data: $('#collapse-checkout-option :input'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-login').button('loading');
	    },
	    complete: function () {
		$('#button-login').button('reset');
	    },
	    success: function (json) {
		$('.alert, .text-danger').remove();
		$('div.has-error').removeClass('has-error');

		if (json['redirect']) {
		    location = json['redirect'];
		    $('.order-step-2').show();
		} else if (json['error']) {
		    $('#collapse-checkout-confirm').collapse('hide');
		    $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

		    // Highlight any found errors
		    $('input[name=\'email\']').parent().addClass('has-error');
		    $('input[name=\'password\']').parent().addClass('has-error');
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });

// Register
    $(document).delegate('#button-register', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');
	$.ajax({
	    url: 'index.php?route=checkout/register/save',
	    type: 'post',
	    data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'tel\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-register').button('loading');
	    },
	    success: function (json) {

		$('.alert, .text-danger').remove();
		$('div.has-error').removeClass('has-error');

		if (json['redirect']) {
		    location = json['redirect'];
		} else if (json['error']) {
		    $('#button-register').button('reset');
		    $('#collapse-checkout-confirm').collapse('hide');
		    if (json['error']['warning']) {
			$('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		    }

		    for (i in json['error']) {
			var element = $('#input-payment-' + i.replace('_', '-'));

			if ($(element).parent().hasClass('input-group')) {
			    $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
			} else {
			    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
			}
		    }

		    // Highlight any found errors
		    $('.text-danger').parent().addClass('has-error');
		} else {
<?php if ($shipping_required) { ?>
    		    var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');

    		    if (shipping_address) {
    			$.ajax({
    			    url: 'index.php?route=checkout/shipping_method',
    			    dataType: 'html',
    			    success: function (html) {
    				// Add the shipping address
    				$.ajax({
    				    url: 'index.php?route=checkout/shipping_method',
    				    dataType: 'html',
    				    success: function (html) {
    					$('#collapse-shipping-method .panel-body').html(html);
    					$('a[href=\'#collapse-payment-address\']').trigger('click');
    					$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');
    				    },
    				    error: function (xhr, ajaxOptions, thrownError) {
    					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    				    }
    				});

    				$('#collapse-shipping-method .panel-body').html(html);

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

    				$('a[href=\'#collapse-shipping-method\']').trigger('click');

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
    				$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

    			    },
    			    error: function (xhr, ajaxOptions, thrownError) {
    				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    			    }
    			});
    		    } else {
    			$.ajax({
    			    url: 'index.php?route=checkout/shipping_method',
    			    dataType: 'html',
    			    success: function (html) {
    				$('#collapse-shipping-method .panel-body').html(html);

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?></a>');
    				$('a[href=\'#collapse-payment-address\']').trigger('click');
    				$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle completed"><?php echo $text_checkout_payment_address; ?></a>');
    				$('a[href=\'#collapse-shipping-method\']').trigger('click');

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
    				$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

    			    },
    			    error: function (xhr, ajaxOptions, thrownError) {
    				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    			    }
    			});
    		    }
<?php } else { ?>
    		    $.ajax({
    			url: 'index.php?route=checkout/payment_method',
    			dataType: 'html',
    			success: function (html) {
    			    $('#collapse-payment-method .panel-body').html(html);

    			    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

    			    $('a[href=\'#collapse-payment-method\']').trigger('click');

    			},
    			error: function (xhr, ajaxOptions, thrownError) {
    			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    			}
    		    });
<?php } ?>


		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });
// Reregister
    $(document).delegate('#button-reregister', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');
// data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
        $.ajax({
	    url: 'index.php?route=checkout/register/resave',
	    type: 'post',
        data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'tel\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-reregister').button('loading');
	    },
	    success: function (json) {

		$('#button-reregister').button('reset');
		$('.alert, .text-danger').remove();
		$('div.has-error').removeClass('has-error');

		if (json['redirect']) {
		    location = json['redirect'];
		} else if (json['error']) {
		    //$('#button-reregister').button('reset');
		    $('#collapse-checkout-confirm').collapse('hide');
		    if (json['error']['warning']) {
			$('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		    }

		    for (i in json['error']) {
			var element = $('#input-payment-' + i.replace('_', '-'));

			if ($(element).parent().hasClass('input-group')) {
			    $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
			} else {
			    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
			}
		    }

		    // Highlight any found errors
		    $('.text-danger').parent().addClass('has-error');
		} else {
<?php if ($shipping_required) { ?>
    		    $('a[href=\'#collapse-payment-address\']').trigger('click');
    		    var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');
    		    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?></a>');
    		    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle completed"><?php echo $text_checkout_payment_address; ?></a>');
    		    $('a[href=\'#collapse-shipping-method\']').trigger('click');
    		    if (shipping_address) {
    			$.ajax({
    			    url: 'index.php?route=checkout/shipping_method',
    			    dataType: 'html',
    			    success: function (html) {
    				// Add the shipping address
    				$.ajax({
    				    url: 'index.php?route=checkout/shipping_method',
    				    dataType: 'html',
    				    success: function (html) {

    					$('#collapse-shipping-method .panel-body').html(html);


    				    },
    				    error: function (xhr, ajaxOptions, thrownError) {
    					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    				    }
    				});

    				$('#collapse-shipping-method .panel-body').html(html);

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

    				$('a[href=\'#collapse-shipping-method\']').trigger('click');

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
    				$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
    			    },
    			    error: function (xhr, ajaxOptions, thrownError) {
    				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    			    }
    			});
    		    } else {
    			$.ajax({
    			    url: 'index.php?route=checkout/shipping_method',
    			    dataType: 'html',
    			    success: function (html) {
    				$('#collapse-shipping-method .panel-body').html(html);

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

    					// $('a[href=\'#collapse-payment-address\']').trigger('click');
                        // $('a[href=\'#collapse-shipping-method\']').trigger('click');

    				$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
    				$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
    			    },
    			    error: function (xhr, ajaxOptions, thrownError) {
    				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    			    }
    			});
    		    }
<?php } else { ?>
    		    $.ajax({
    			url: 'index.php?route=checkout/payment_method',
    			dataType: 'html',
    			success: function (html) {
    			    $('#collapse-payment-method .panel-body').html(html);

    			    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

    			    $('a[href=\'#collapse-payment-method\']').trigger('click');


    			},
    			error: function (xhr, ajaxOptions, thrownError) {
    			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    			}
    		    });
<?php } ?>


		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });


// Shipping Address
    $(document).delegate('#button-payment-address', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');
	$.ajax({
	    url: 'index.php?route=checkout/account/save',
	    type: 'post',
	    data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-payment-address').button('loading');
	    },
	    success: function (json) {
		$('.alert, .text-danger').remove();

		if (json['redirect']) {
		    location = json['redirect'];
		} else if (json['error']) {
		    $('#button-payment-address').button('reset');
		    $('#collapse-checkout-confirm').collapse('hide');
		    if (json['error']['warning']) {
			$('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		    }

		    for (i in json['error']) {
			var element = $('#input-payment-' + i.replace('_', '-'));

			if ($(element).parent().hasClass('input-group')) {
			    $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
			} else {
			    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
			}
		    }

		    // Highlight any found errors
		    $('.text-danger').parent().parent().addClass('has-error');
		} else {
		    $.ajax({
			url: 'index.php?route=checkout/shipping_method',
			dataType: 'html',
			complete: function () {
			    $('#button-payment-address').button('reset');
			},
			success: function (html) {
			    $('#collapse-shipping-method .panel-body').html(html);
			    $('a[href=\'#collapse-payment-address\']').trigger('click');
			    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?></a>');

			    $('a[href=\'#collapse-shipping-method\']').trigger('click');

			    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

			    $.ajax({
				url: 'index.php?route=checkout/shipping_address',
				dataType: 'html',
				success: function (html) {
				    $('#collapse-shipping-address .panel-body').html(html);
				},
				error: function (xhr, ajaxOptions, thrownError) {
				    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			    });
			},
			error: function (xhr, ajaxOptions, thrownError) {
			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		    });

		    $.ajax({
			url: 'index.php?route=checkout/payment_address',
			dataType: 'html',
			success: function (html) {
			    $('#collapse-payment-address .panel-body').html(html);
			},
			error: function (xhr, ajaxOptions, thrownError) {
			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		    });
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });

// Guest
    $(document).delegate('#button-guest', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');

	$.ajax({
	    url: 'index.php?route=checkout/guest/save',
	    type: 'post',
	    data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'tel\'], #collapse-payment-address textarea, #collapse-payment-address select'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-guest').button('loading');
		$('div.has-error').removeClass('has-error');
	    },
	    success: function (json) {
		$('.alert, .text-danger').remove();
		$('#button-guest').button('reset');

		if (json['redirect']) {
		    location = json['redirect'];
		} else if (json['error']) {
		    $('#collapse-checkout-confirm').collapse('hide');

		    if (json['error']['warning']) {
			$('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		    }

		    for (i in json['error']) {
			var element = $('#input-payment-' + i.replace('_', '-'));

			if ($(element).parent().hasClass('input-group')) {
			    $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
			} else {
			    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
			}
		    }

		    // Highlight any found errors
		    $('.text-danger').parent().addClass('has-error');
		} else {
		    //$('a[href=\'#collapse-payment-address\']').trigger('click');
                    if ($('#collapse-shipping-method .panel-body').html().length > 0) {
                        $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle completed"><?php echo $text_checkout_payment_address; ?></a>');
                        $('a[href=\'#collapse-payment-address\']').trigger('click');
                    }

                    <?php if ($shipping_required) { ?>

    		    var shipping_address = $('#collapse-payment-address input[name=\'shipping_address\']:checked').prop('value');

    		    if ($('#collapse-shipping-method .panel-body').html().length == 0) {
    			if (shipping_address) {
    			    $.ajax({
    				url: 'index.php?route=checkout/shipping_method',
    				dataType: 'html',
    				complete: function () {
    				    //$('#button-guest').button('reset');
    				    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle completed"><?php echo $text_checkout_payment_address; ?></a>');
    				    $('a[href=\'#collapse-payment-address\']').trigger('click');
    				},
    				success: function (html) {
    				    // Add the shipping address
    				    $.ajax({
    					url: 'index.php?route=checkout/guest_shipping',
    					dataType: 'html',
    					success: function (html) {
    					    $('#collapse-shipping-address .panel-body').html(html);
    					    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
    					},
    					error: function (xhr, ajaxOptions, thrownError) {
    					    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    					}
    				    });

    				    $('#collapse-shipping-method .panel-body').html(html);

    				    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

    				    $('a[href=\'#collapse-shipping-method\']').trigger('click');

    				    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

    				},
    				error: function (xhr, ajaxOptions, thrownError) {
    				    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    				}
    			    });
    			} else {
    			    $.ajax({
    				url: 'index.php?route=checkout/guest_shipping',
    				dataType: 'html',
    				complete: function () {
    				    //$('#button-guest').button('reset');
    				},
    				success: function (html) {
    				    $('#collapse-shipping-address .panel-body').html(html);

    				    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

    				    $('a[href=\'#collapse-shipping-method\']').trigger('click');

    				    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
    				    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

    				},
    				error: function (xhr, ajaxOptions, thrownError) {
    				    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    				}
    			    });
    			}
    		    }
<?php } else { ?>
    		    $.ajax({
    			url: 'index.php?route=checkout/payment_method',
    			dataType: 'html',
    			complete: function () {
    			    //$('#button-guest').button('reset');
    			},
    			success: function (html) {
    			    $('#collapse-payment-method .panel-body').html(html);

    			    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

    			    $('a[href=\'#collapse-payment-method\']').trigger('click');

    			},
    			error: function (xhr, ajaxOptions, thrownError) {
    			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    			}
    		    });
<?php } ?>
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });

// Guest Shipping
    $(document).delegate('#button-guest-shipping', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');
	$.ajax({
	    url: 'index.php?route=checkout/guest_shipping/save',
	    type: 'post',
	    data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-guest-shipping').button('loading');
	    },
	    success: function (json) {
		$('.alert, .text-danger').remove();

		if (json['redirect']) {
		    location = json['redirect'];
		} else if (json['error']) {
		    $('#collapse-checkout-confirm').collapse('hide');
		    $('#button-guest-shipping').button('reset');
		    if (json['error']['warning']) {
			$('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		    }

		    for (i in json['error']) {
			var element = $('#input-shipping-' + i.replace('_', '-'));

			if ($(element).parent().hasClass('input-group')) {
			    $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
			} else {
			    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
			}
		    }

		    // Highlight any found errors
		    $('.text-danger').parent().addClass('has-error');
		} else {
		    $.ajax({
			url: 'index.php?route=checkout/shipping_method',
			dataType: 'html',
			complete: function () {
			    $('#button-guest-shipping').button('reset');
			},
			success: function (html) {
			    $('#collapse-shipping-method .panel-body').html(html);

			    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i>');

			    $('a[href=\'#collapse-shipping-method\']').trigger('click');

			    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

			},
			error: function (xhr, ajaxOptions, thrownError) {
			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		    });
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });

    $(document).delegate('#button-shipping-method', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');
	$.ajax({
	    url: 'index.php?route=checkout/shipping_method/save',
	    type: 'post',
	    data: $('#collapse-shipping-method input[type=\'text\'], #collapse-shipping-method input[type=\'hidden\'], #collapse-shipping-method input[type=\'date\'], #collapse-shipping-method input[type=\'datetime-local\'], #collapse-shipping-method input[type=\'time\'], #collapse-shipping-method input[type=\'password\'], #collapse-shipping-method input[type=\'checkbox\']:checked, #collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea, #collapse-shipping-method select, #collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-shipping-method').button('loading');
		$('div.has-error').removeClass('has-error');
	    },
	    success: function (json) {
		$('.alert, .text-danger').remove();
		if (json['redirect']) {
		    location = json['redirect'];
		} else if (json['error']) {
		    $('#collapse-checkout-confirm').collapse('hide');
		    $('#button-shipping-method').button('reset');
		    if (json['error']['warning']) {
			$('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		    }
		    for (i in json['error']) {
			var element_id = '#input-payment-' + i.replace('_', '-');
			var element = $(element_id);

			if (element_id == '#input-payment-address-1')
			{
			    var custom_address_field = $('input[name="shipping_method"]:checked').closest('.row-radio').find('.shipping_custom_field input');
			    //$(custom_address_field).css('outline','2px solid red');
			    if ($(custom_address_field).length > 0)
			    {
				$(custom_address_field).after('<div class="text-danger">' + json['error'][i] + '</div>');
			    }

			}

			if (element_id == '#input-payment-zone')
			{
			    $(element).closest('.selectric-wrapper').addClass('has-error');
			    $(element).closest('.selectric-wrapper').append('<div class="text-danger">' + json['error'][i] + '</div>');
			}

			if ($(element).parent().hasClass('input-group')) {
			    $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
			} else {
			    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
			}


		    }

		    // Highlight any found errors
		    $('.text-danger').parent().addClass('has-error');

		} else {
		    $.ajax({
			url: 'index.php?route=checkout/payment_method',
			dataType: 'html',
			complete: function () {
			    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle completed"><?php echo $text_checkout_shipping_method; ?></a>');
			    $('#button-shipping-method').button('reset');
			    $('a[href=\'#collapse-shipping-method\']').trigger('click');
			},
			success: function (html) {
			    $('#collapse-payment-method .panel-body').html(html);

			    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

			    $('a[href=\'#collapse-payment-method\']').trigger('click');

			},
			error: function (xhr, ajaxOptions, thrownError) {
			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		    });
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });

    $(document).delegate('#button-payment-method', 'click', function () {
	var step_number = $(this).closest('.panel-default').find('.accordion-toggle').removeClass('completed');
	var payment_method_data = $('#collapse-payment-method .payment_custom_field.showing input#payment_custom_method, #collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea');

	$('.payment_custom_field').removeClass('has-error').find('.text-danger').remove();

	if ($('#collapse-payment-method .payment_custom_field.showing').length > 0) {
	    var payment_custom_field = $('#collapse-payment-method .payment_custom_field.showing input#payment_custom_method');
	    if ($(payment_custom_field).val() <= 0) {
		$(payment_custom_field).parent().addClass('has-error');

		$(payment_custom_field).after('<div class="text-danger">Укажите способ оплаты</div>');
		return;
	    }
	}

	/*var payment_method_data = $('#collapse-payment-method .payment_custom_field.showing input#payment_custom_method, #collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea');
	 console.log($(payment_method_data).serializeArray());*/

	$.ajax({
	    url: 'index.php?route=checkout/payment_method/save',
	    type: 'post',
	    data: payment_method_data, //$('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
	    dataType: 'json',
	    beforeSend: function () {
		$('#button-payment-method').button('loading');
	    },
	    success: function (json) {
		$('.alert, .text-danger').remove();

		if (json['redirect']) {
		    location = json['redirect'];
		} else if (json['error']) {
		    $('#collapse-checkout-confirm').collapse('hide');
		    $('#button-payment-method').button('reset');

		    if (json['error']['warning']) {
			$('#collapse-payment-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		    }
		} else {
		    $.ajax({
			url: 'index.php?route=checkout/confirm',
			dataType: 'html',
			complete: function () {
			    $('a[href=\'#collapse-payment-method\']').trigger('click');
			    $('#button-payment-method').button('reset');
			    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle completed"><?php echo $text_checkout_payment_method; ?></a>');

			},
			success: function (html) {
			    $('#collapse-checkout-confirm .panel-body').html(html);

			    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"></a>');

			    $('#collapse-checkout-confirm').collapse('show');
			},
			error: function (xhr, ajaxOptions, thrownError) {
			    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		    });
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
	});
    });

//действие при показе вкладки способов оплаты
    $('#collapse-payment-method').on('show.bs.collapse', function (e) {
	addPaymentToSummary();
    });

    /*$('.panel-collapse.collapse').on('hide.bs.collapse', function (e) {
     $(this).find('.panel-body').fadeOut(20);
     });
     $('.panel-collapse.collapse').on('show.bs.collapse', function (e) {
     $(this).find('.panel-body').fadeIn(200);
     });*/

//обновление итого при смене способа доставки
    $(document).on('change', 'input[name="shipping_method"]', function () {
	showHideRegionCity();
	addShippingToSummary();

	//скрытие дополнительного поля ввода адреса
	$('.shipping_custom_field').removeClass('showing').addClass('hidden');

	//проверяем есть у выбраного способа доставки доп. поле адреса
	var custom_shipping_address = $('input[name="shipping_method"]:checked').closest('.row-radio').find('.shipping_custom_field');
	if ($(custom_shipping_address).length > 0)
	{

            if($(this).val() == 'new_post.new_post'){
                var typeClass = $('input[name="shipping_method"]:checked').closest('.row-radio').find('#input-newpost-type').val();
                //если есть, то показываем его
                custom_shipping_address = $('input[name="shipping_method"]:checked').closest('.row-radio').find('.shipping_custom_field.' + typeClass);
                $(custom_shipping_address).removeClass('hidden').addClass('showing');
                //и подставляем его значение в поле адреса
                $('input#input-payment-address-1').val($(custom_shipping_address).find('#shipping_custom_address').val());
            } else {
                //если есть, то показываем его
                $(custom_shipping_address).removeClass('hidden').addClass('showing');
                //и подставляем его значение в поле адреса
                $('input#input-payment-address-1').val($(custom_shipping_address).find('input#shipping_custom_address').val());
            }

	} else
	{
	    //если нет, то подставляем в поле адреса дефизы
	    $('input#input-payment-address-1').val('---');
	}
	$('input#input-payment-address-1').trigger('change');

    });

//при применении значения дополнительного поля ввода поля копируем его в поле адреса
    $(document).on('change', 'input#shipping_custom_address', function () {
	var custom_address = $(this).val();
	$('input#input-payment-address-1').val(custom_address);
    });



// действия при изменении способа доставки
    $(document).on('change', 'input[name="payment_method"]', function () {

	var selected_payment = $('input[name="payment_method"]:checked').val();

	$('.payment_custom_field').removeClass('showing').addClass('hidden');

	//проверяем есть у выбраного способа доставки доп. поле адреса
	var custom_payment_method = $('input[name="payment_method"]:checked').closest('.radio').find('.payment_custom_field');
	if ($(custom_payment_method).length > 0)
	{
	    //если есть, то показываем его
	    $(custom_payment_method).removeClass('hidden').addClass('showing');
	    //и подставляем его значение в поле адреса
	    //$('input#input-payment-address-1').val($(custom_shipping_address).find('input#shipping_custom_address').val());
	}

	addPaymentToSummary();
    });



//функция выводит выбраный способ доставки в списке ИТОГО
    function addShippingToSummary()
    {
	var selected_shiping = $('input[name="shipping_method"]:checked').val();
	var selected_shiping_text = $('label[for="shipping_' + selected_shiping + '"] .shipping_text').text();

	$('li#list-summary-shipping').find('#list-summary-shipping-value').text(selected_shiping_text);
	$('li#list-summary-shipping').show();
    }

//функция выводит выбраный способ оплаты в списке ИТОГО
    function addPaymentToSummary()
    {
	var html = '';
	var selected_payment = $('input[name="payment_method"]:checked').val();
	var selected_payment_text = $('label[for="payment_' + selected_payment + '"]').text();

	$('li#list-summary-payment').find('.list-summary-payment-description').remove();

	switch (selected_payment) {
	    case 'cod':
		selected_payment_text += ' <span class="text-red">*</span>';
		html += '<li class="list-summary-payment-description">';
		html += '<span class="text-red">*</span> Новым клиентам предоплата 15% в связи с тем, что недобросовестные конкуренты и покупатели делают ложные заказы, не забирая их с отделения и мы оплачиваем транспортные услуги';
		html += '</li>';
		break;
	    case 'bank_transfer':
		selected_payment_text += ' <span class="text-red">*</span>';
		html += '<li class="list-summary-payment-description">';
		html += '<span class="text-red">*</span> Менеджер передаст вам реквизиты для оплаты после подтверждения заказа';
		html += '</li>';
		break;
	    case 'cheque':
		selected_payment_text += '';
		html += '';
		break;
	    case 'cod1':
		selected_payment_text += '';
		html += '';
		break;
	    default:
		selected_payment_text += ' <span class="text-red">*</span>';
		html += '<li class="list-summary-payment-description">';
		html += '<span class="text-red">*</span> К стоимости заказа добавляется 20 грн<br>';
		html += '+ 2% от стоимости заказа - комиссия почтовой службы за наложенный платеж';
		html += '</li>';
	}

	$('li#list-summary-payment').find('#list-summary-payment-value').html(selected_payment_text);
	$('li#list-summary-payment').find('#list-summary-payment-value').after(html);

	$('li#list-summary-payment').show();
    }

//функция поиска города доставки Новой Почты
    function npFindCity(city_name)
    {
	$.ajax({
	    url: 'index.php?route=checkout/shipping_method/newPostCity',
	    type: 'post',
	    data: {city_name: city_name},
	    dataType: 'json',
	    success: function (data) {
		//console.log(data);
	    }
	});
    }


//функция скрытия показа полей области и города
    function showHideRegionCity()
    {
	//находим выбранный способ доставки
	var selected_shippin = $('input[name="shipping_method"]:checked').val();

	if (selected_shippin == 'new_post.new_post')
	{
	    //если доставка новой почтой, то показываем поля области и города
	    //block-shipping-field

	    $('#collapse-shipping-method').find('.block-shipping-field').show();
	} else
	{
	    //если доставка НЕ новой почтой, то скрываем поля области и города
	    $('#collapse-shipping-method .block-shipping-field').hide();
            $('#collapse-shipping-method .shipping_custom_field').removeClass('showing').addClass('hidden');

	    //проверяем что бы значение не был пустые
	    if ($('select#input-payment-zone').val() == '')
	    {
		$('select#input-payment-zone').val('3501');
	    }

	    if ($('#collapse-shipping-method .block-shipping-field input#input-payment-city').val() && $('#collapse-shipping-method .block-shipping-field input#input-payment-city').val().length < 3)
	    {
		$('#collapse-shipping-method .block-shipping-field input#input-payment-city').val('Город');
	    }
	}

	//$('input[name="shipping_method"]:checked').closest('.row-radio').find('.shipping_custom_field');
    }

</script>
<?php echo $footer; ?>
