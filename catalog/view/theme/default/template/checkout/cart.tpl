<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class=""><?php echo $content_top; ?>
        <div class="content-holder">
            <!-- heading -->
            <div class="heading clearfix">
                <h1><?php echo $heading_title; ?></h1>
                <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <?php $i=1; ?>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope
                        itemtype="http://schema.org/ListItem">
                        <?php if($i<count($breadcrumbs)) { ?>
                        <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                            <?php } ?>
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            <?php if($i<count($breadcrumbs)) { ?>
                        </a>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i ?>"/>
                    </li>
                    <?php $i++; ?>
                    <?php } ?>
                </ol>
            </div>
            <?php if ((isset($attention))&&($attention)) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ((isset($success))&&($success)) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ((isset($attention))&&($error_warning)) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if (!isset($text_error)) { ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="cart-table">
                    <div class="cart-table-heading clearfix tablet-hidden">
                        <span class="cell visual">
                            <?php if ($product['old_price']) { ?>
                            <span class="tag discount">Скидка</span>
                            <?php } ?><?php echo $column_image; ?>
                        </span>
                        <span class="cell text-block"><?php echo $column_name; ?></span>
                        <span class="cell"><?php echo $column_quantity; ?></span>
                        <span class="cell"><?php echo $column_price; ?></span>
                        <span class="cell"><?php echo $column_total; ?></span>
                    </div>
                    <?php foreach ($products as $product) { ?>
                    <div id="cart-table-row-<?php echo $product['cart_id']; ?>" class="cart-table-row clearfix">
                        <div class="cell visual">
                            <?php if ($product['old_price']) { ?>
                            <span class="tag discount">Скидка</span>
                            <?php } ?>
                            <img width="158" height="160" src="<?php echo $product['thumb']; ?>"
                                                      alt="<?php echo $product['name']; ?>"
                                                      title="<?php echo $product['name']; ?>"/></div>
                        <div class="tablet-block">
                            <div class="cell text-block">
                                <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
                                <span class="article">Артикул:	<span
                                            class="text-red"><?php echo $product['model']; ?></span></span>
                            </div>
                            <div class="right-block">
                                <div class="cell qty">
                                    <span class="tablet-show">Количество</span>
                                    <div class="spinner2"><input readonly class="spinner" type="text"
                                                                 data-cart-id="<?php echo $product['cart_id']; ?>"
                                                                 name="quantity"
                                                                 value="<?php echo $product['quantity']; ?>"/></div>
                                    <?php if ($product['option']) { ?>
                                    <?php foreach ($product['option'] as $option) { ?>
                                    <span class="text"><?php echo $option['value']; ?>:</span> <span
                                            class="text-red"><?php echo $option['quant']; ?></span>
                                    <?php } ?>
                                    <?php } ?>

                                </div>
                                <div class="cell"><span class="tablet-show">Цена</span>
                                    <div class="inner"><strong class="sum">
                                           <!--  <?php var_dump($product); ?> -->

                                            <?php if ($product['old_price']) { ?>
                                            <span class="price">
                                                <span class="text-grey"><?php echo $product['old_price']; ?></span>
								                <span class="price-new"><strong class="sum text-red"><?php echo $product['price']; ?></strong></span></span>
                                            <?php } else { ?>
                                            <span class="price"><span class="price-new"><strong
                                                            class="sum text-red"><?php echo $product['price']; ?></strong></span></span>

                                            <?php } ?>

                                        </strong></div>
                                </div>
                                <div class="cell"><span class="tablet-show">Сумма</span>
                                    <div class="inner"><strong id="total_product_sum"
                                                               class="sum"><?php echo $product['total']; ?></strong>
                                    </div>
                                </div>
                            </div>
                            <?php /*
							<button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn
                            btn-primary">update</button>
                            */ ?>
                            <a data-cart-id="<?php echo $product['cart_id']; ?>" title="<?php echo $button_remove; ?>"
                               class="delete" href="#"><?php echo $button_remove; ?></a>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="clearfix cart-total-block">
                        <div class="cart-total">
                            <?php foreach ($totals as $total) { ?>
                            <?php if(mb_strtolower(mb_substr($total['title'],0,6,'utf-8')) == 'cкидка'): ?>
                            <div class="discount-total">
                                <?php echo $total['title'];?>: <strong
                                        class="sum"><?php echo $total['text']; ?></strong>
                            </div>
                            <?php endif; ?>
                            <?php } ?>
                            <!--  <div class="discount-total">Скидка: <strong class="sum">234</strong> <sup>00 грн</sup></div>-->
                            <div class="cart-total-row">

                                <?php foreach ($totals as $total) { ?>
                                <?php if(mb_strtolower(mb_substr($total['title'],0,6,'utf-8')) != 'cкидка'): ?>
                                <div class="item">
                                    <span class="title"><?php echo $total['title'];?>:</span>

                                    <strong class="sum"><?php echo $total['text']; ?></strong>
                                </div>
                                <?php endif; ?>
                                <?php } ?>
                            </div>
                            <a href="<?php echo $checkout; ?>" class="button"><?php echo $button_checkout; ?></a>
                        </div>
                        <a class="link-continue" href="<?php echo $continue; ?>"><?php echo $button_shopping; ?></a>
                    </div>

                </div>
        </div>
        </form>

        <script>
            //обновление страницы корзины при изменении кол-ва
            $(".spinner").on("spin", function (event, ui) {
                var cart_id = $(this).attr('data-cart-id');
                var value = ui.value;

                var quantity = {};
                quantity[cart_id] = value;
                if (quantity[cart_id] > 0) {
                    $.ajax({
                        url: 'index.php?route=checkout/cart/edit',
                        type: 'post',
                        data: {quantity: quantity},
                        dataType: 'json',
                        beforeSend: function () {
                            $('#cart > a').button('loading');
                        },
                        /*complete: function() {
                            $('#cart > a').button('reset');
                        },*/
                        success: function (json) {

                            //обновленик корзины в шапке
                            setTimeout(function () {
                                $('#cart > a').html('<span id="cart-total">' + json['quant'] + '</span>');
                            }, 100);
                            //$('#cart > a').addClass('active');

                            $('#cart > ul').load('index.php?route=common/cart/info ul li');

                            //обновление суммы текущего товара
                            $('#cart-table-row-' + cart_id + ' #total_product_sum').load('index.php?route=checkout/cart #cart-table-row-' + cart_id + ' #total_product_sum');

                            //обновление итого корзины
                            $('.cart-total-block').load('index.php?route=checkout/cart .cart-total-block');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            });

            //удаление товара
            $(document).on('click', 'a.delete', function (e) {
                e.preventDefault();

                var cart_id = $(this).attr('data-cart-id');
                $.ajax({
                    url: 'index.php?route=checkout/cart/remove',
                    type: 'post',
                    data: 'key=' + cart_id,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#cart > a').button('loading');
                    },
                    complete: function () {
                        $('#cart > a').button('reset');
                    },
                    success: function (json) {
                        // Need to set timeout otherwise it wont update the total
                        setTimeout(function () {
                            $('#cart > a').html('<span id="cart-total">' + json['quant'] + '</span>');
                        }, 100);
                        $('#cart > a').removeClass('active');
                        $('#cart > ul').load('index.php?route=common/cart/info ul li');

                        $('#cart-table-row-' + cart_id).remove();

                        if (json['quant'] == 0) {
                            location = 'index.php?route=checkout/cart';
                        }

                        $('.cart-total-block').load('index.php?route=checkout/cart .cart-total-block');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            });
        </script>

        <?php } else { ?>
        <p><?php echo $text_error; ?></p>
        <?php } ?>
        <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
