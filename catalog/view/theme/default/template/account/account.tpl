<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class=""><?php echo $content_top; ?>
        <div class="content-holder">
            <!-- heading -->
            <div class="heading clearfix">
                <h1><?php echo $heading_title; ?></h1>
                <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <?php $i = 1; ?>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                                <?php if ($i < count($breadcrumbs)) { ?>
                                <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                                <?php if ($i < count($breadcrumbs)) { ?>
                                </a>
                            <?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
                        <?php $i++; ?>
                    <?php } ?>
                </ol>
            </div>
            <div class="clearfix">
                <!-- sidebar -->
                 <aside class="sidebar">
                    <nav class="sidebar-nav clearfix">
                        <ul>
                            <li><a class="active" href="javascript:void(0);">Личные данные</a></li>
                            <li><a href="<?php echo $order; ?>">История заказов</a></li>
                            <li><a href="/discounts/">Скидки</a></li>
                        </ul>
                    </nav>
                </aside>
				<!-- content -->
				<div class="content personal">
					<div class="content-holder">

						<h1 class="tablet-show">Личные данные</h1>

						<form action="<?php echo $action; ?>" method="post">
							<fieldset>
								<div class="columns-personal-data clearfix">
									<div class="column">
										<h2>Контактные данные</h2>
										<div class="input-holder <?php echo !empty($error_lastname) ? 'has-error' : ''; ?>">
											<input name="lastname" placeholder="<?php echo $entry_lastname; ?>" value="<?php echo $lastname; ?>" type="text"/>
											<div class="text-danger"><?php echo $error_lastname; ?></div>
										</div>
										<div class="input-holder <?php echo !empty($error_firstname) ? 'has-error' : ''; ?>">
											<input name="firstname" placeholder="<?php echo $entry_firstname; ?>" value="<?php echo $firstname; ?>" type="text"/>
											<div class="text-danger"><?php echo $error_firstname; ?></div>
										</div>
										<div class="input-holder <?php echo !empty($error_surname) ? 'has-error' : ''; ?>">
											<input name="surname" placeholder="Отчество" value="<?php echo $surname; ?>" type="text"/>
											<div class="text-danger"><?php echo $error_surname; ?></div>
										</div>
										<div class="input-holder email <?php echo !empty($error_email) ? 'has-error' : ''; ?>">
											<input name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email; ?>" type="text"/>
											<div class="text-danger"><?php echo $error_email; ?></div>
										</div>
										<!-- <div class="label-holder"><label><?php echo $entry_telephone ?></label></div> -->
										<div class="phone-holder input-holder clearfix <?php echo !empty($error_telephone) ? 'has-error' : ''; ?>">
											<div class="form-group phone-holder input-holder clearfix">
												<div>
													<input placeholder="+38 - (    ) -     -    -   " value="<?php echo $telephone_operator_code; ?>" name="telephone_operator_code" class="telephone_main_part" type="tel"/>
													<input value="&nbsp;" name="telephone_main_part" type="tel"  style="display: none" />
												</div>
											</div>
											<div class="text-danger" style="float: left;"><?php echo $error_telephone; ?></div>
										</div>

										<div class="phone-holder add-phone open-close-inner input-holder clearfix">
											<?php if(!isset($fax)): ?>
												<a class="slide-opener-inner" href="">+ указать дополнительный телефон</a>
											<?php endif; ?>

											<div class="form-group phone-holder add-phone open-close-inner input-holder clearfix">
												<div class="slide-block-inner">
													<input placeholder="+38 - (    ) -     -    -   " value="<?php echo $fax; ?>" name="fax" class="telephone_main_part" type="tel"/>
												</div>
											</div>
										</div>
										<h2>Изменить пароль</h2>
										<div class="input-holder password <?php echo !empty($error_password) ? 'has-error' : ''; ?>">
											<input name="password" value="<?php echo $password; ?>" placeholder="Новый пароль" type="password"/>
											<div class="text-danger"><?php echo $error_password; ?></div>
										</div>
										<div class="input-holder password <?php echo !empty($error_confirm) ? 'has-error' : ''; ?>">
											<input name="confirm" placeholder="Повторить новый пароль" type="password"/>
											<div class="text-danger"><?php echo $error_confirm; ?></div>
										</div>
									</div>
									<div class="column">
										<h2>Доставка</h2>
										<div class="input-holder select">
											<select name="shipping_code" id="shipping_code">
												<option value="0">Способ доставки</option>
												<?php foreach($shipping_list as $code => $shipping): ?>
													<option <?php echo ($code == $shipping_code) ? 'selected' : ''; ?> value="<?php echo $code; ?>"><?php echo $shipping['title']; ?></option>
												<?php endforeach; ?>
											</select>
										</div>

										<!-- *****************  newpost from shopping_method **************************** -->

										<div id="new_post" style="display: none">
											<div id="address">
												<input type="hidden" name="zone_id" value="3501">
												<div class="form-group block-shipping-field">
															<select name="input-newpost-type" id="input-newpost-type"
                                                                    class="form-control">
																<?php if(!empty($input_newpost_type) && $input_newpost_type == "department") { ?>
                                                                <option value="department" selected='selected'>На отделение</option>
                                                                <option value="by-address">Доставка курьером</option>
																<?php } else { ?>
																<option value="department">На отделение</option>
																<option value="by-address" selected='selected'>Доставка курьером</option>
																<?php }?>

                                                            </select>
                                                        </div>

                                                        <div class="form-group required block-shipping-field" >
                                                            <?php /*
                                                                  <input type="text" name="city" value="<?php echo $city; ?>"
                                                            placeholder="<?php echo $entry_city; ?>" id="input-payment-city"
                                                            class="form-control" />
                                                            * */ ?>
															 <select class="selectSearch" name="city-np" id="select-np-city">
                                                                 <?php  foreach($npCities as $ref => $cityName): ?>
                                                                 <option
                                                                 <?php echo ($city_np == $cityName) ? 'selected="selected"' : ''; ?>
                                                                 value="<?php echo $cityName ?>
                                                                 "><?php echo $cityName; ?></option>
                                                                 <?php endforeach; ?>
                                                             </select>
                                                         </div>

                                                         <div class="shipping_custom_field department new_post showing " id="shipping_custom_address_wrap" style="display: block">
                                                             <select name="shipping-custom-address" id="shipping_custom_address"
                                                                     class="selectSearch">
																 <?php if(empty($shipping_custom_address)) { ?>
                                                                 <option value="">Выберите отделение</option>
																 <?php } else { ?>
																 <option value = "<?php echo $shipping_custom_address ?>"><?php echo $shipping_custom_address ?></option>
																 <?php } ?>

                                                             </select>
                                                         </div>

												 <div class="shipping_custom_field by-address new_post"  id="shipping_custom_address2_wrap" style="display: none">
                                                        <input type="text" id="shipping_custom_address2"
                                                               name="shipping-custom-address2"
                                                               value="<?php echo $shipping_custom_address ?>"
                                                               placeholder="Адрес доставки" class="form-control">
                                                    </div>
                                                </div>
											<br>
                                            </div>

                                         <!-- *****************      end      **************************** -->

										<h2>Оплата</h2>
										<div class="input-holder select">
											<select name="payment_code">
												<option value="">Способ оплаты</option>
												<?php foreach($payment_list as $code => $payment): ?>
													<option <?php echo ($code == $payment_code) ? 'selected' : ''; ?> value="<?php echo $code; ?>"><?php echo $payment['title']; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
			 						<!--	<div class="input-holder select <?php echo !empty($error_zone) ? 'has-error' : ''; ?>">
											<select name="zone_id">
												<option value="">Выберите область</option>
												<?php foreach($zones as $zone): ?>
													<option <?php echo $zone['zone_id'] == $zone_id ? 'selected' : ''; ?> value="<?php echo $zone['zone_id']; ?>"><?php echo $zone['name']; ?></option>
												<?php endforeach; ?>
											</select>
											<div class="text-danger"><?php echo $error_zone; ?></div>
										</div>
										<div class="input-holder <?php echo !empty($error_city) ? 'has-error' : ''; ?>">
											<input type="text" name="city" placeholder="Город" value=<?php echo $city?> >
											<div class="text-danger"><?php echo $error_city; ?></div>
										</div>  -->
										<input type="hidden" name="address_id" value="<?php echo $address_id; ?>">
										<input type="hidden" name="country_id" value="<?php echo $country_id; ?>">

										<button class="button" type="submit">Сохранить изменения</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end content -->
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
    <script>
        $(document).ready(function(){
            $('.telephone_main_part').mask("+38 - (999) 999 - 99 - 99");
        });
        $(document).ready(function () {

            var Selectric = $('select#select-np-city').data('selectric');
            Selectric.destroy();
            $('select#select-np-city, select#shipping_custom_address').select2();

            var inputNewpostType = "<?php echo $input_newpost_type ?>";
            var shipping_custom_address = "<?php echo $shipping_custom_address ?>";
            if(inputNewpostType == "department" && !shipping_custom_address) {
                var ee = document.getElementById('select-np-city');
                var city = ee.options[ee.selectedIndex].value;
                ajaxCity(city.trim());
            }

            if(inputNewpostType == "department" && shipping_custom_address) {
                var ee = document.getElementById('select-np-city');
                var city = ee.options[ee.selectedIndex].value;

                //ajaxCity(city.trim());
                $.ajax({
                    url: 'index.php?route=checkout/shipping_method/newPostWarehous',
                    type: 'post',
                    data: {
                        city_name: city.trim()

                    },
                    dataType: 'json',
                    success: function (json) {
                        addToDepartmentNew(json, shipping_custom_address);
                    }
                });
            }


            var fShippingCode = function () {
                console.log($(this).val());
                if ($(this).val() === "new_post") {
                    new_post.style.display = 'block';
                } else {
                    new_post.style.display = 'none';
                }
            };

            var fSelectNpCity = function () {
                $('select#select-np-city, select#shipping_custom_address').select2();

             //   $('#shipping_code').on('selectric-select', fShippingCode);
              //  if ($(window).width() < 1030) {
                    $('#shipping_code').on('change', fShippingCode);
              //  }
                var selectedCity = $(this).val().trim();
                $('input#input-payment-address-1').val(selectedCity);
                ajaxCity(selectedCity);

            };

           // $('select#select-np-city').on('selectric-select', fSelectNpCity); // Город
          //  if ($(window).width() < 1030) {
                $('select#select-np-city').on('change', fSelectNpCity);
          //  }

            var shipping_code = "<?php echo $shipping_code ?>";
            var new_post = document.getElementById('new_post');
            if (shipping_code === "new_post") {
                new_post.style.display = 'block';
            }

            var shippingCustoAdress2 = document.getElementById('shipping_custom_address2');


            $('#shipping_code').on('selectric-change', fShippingCode); // Новая почта
            if ($(window).width() < 1030) {
                $('#shipping_code').on('change', fShippingCode);
            }
            var department = document.getElementById('shipping_custom_address_wrap');
            var byAddress = document.getElementById('shipping_custom_address2_wrap');
            if (inputNewpostType === "by-address") {
                byAddress.style.display = 'block';
                department.style.display = 'none';

            } else {
                byAddress.style.display = 'none';
                department.style.display = 'block';
            }



            var fInputNewpostType = function () {
                console.log($(this).val());
                if ($(this).val() === "by-address") {
                    byAddress.style.display = 'block';
                    department.style.display = 'none';
                    shippingCustoAdress2.setAttribute("value", "");

                } else {
                    byAddress.style.display = 'none';
                    department.style.display = 'block';
                    var e = document.getElementById('select-np-city');
                    var city = e.options[e.selectedIndex].value;
                    ajaxCity(city.trim());
                }
            };

            $('#input-newpost-type').on('selectric-select', fInputNewpostType); // на склад, курьером
            if ($(window).width() < 1030) {
                $('#input-newpost-type').on('change', fInputNewpostType);
            }

            function ajaxCity(selectedCity) {
                console.log("ajaxCity", selectedCity);
                $.ajax({
                    url: 'index.php?route=checkout/shipping_method/newPostWarehous',
                    type: 'post',
                    data: {
                        city_name: selectedCity

                    },
                    dataType: 'json',
                    success: function (json) {
                        addToDepartment(json);
                    }
                });
            }

            function addToDepartment(departments) {
                var depSelect = $(document).find('select#shipping_custom_address');
                var options = '';
                $.each(departments, function () {
                    options += '<option value = "' + this.address + '">' + this.address + '</option>';
                });
                $(depSelect).html(options).selectric('refresh');
                $(depSelect).html(options).select2();

                var Selectric2 = $('select#shipping_custom_address').data('selectric');
                Selectric2.destroy();
                $('select#shipping_custom_address').select2();
            }

            function addToDepartmentNew(departments, custom_address){
                var depSelect = $(document).find('select#shipping_custom_address');
                var options = '';
                $.each(departments, function(){
                    if(custom_address === this.address) {
                        options += '<option selected="selected" value = "' + this.address + '">' + this.address + '</option>';
                    } else {
                        options += '<option value = "' + this.address + '">' + this.address + '</option>';
                    }
                });
                $(depSelect).html(options).selectric('refresh');
                $(depSelect).html(options).select2();
                // $('input#input-payment-address-1').val($(depSelect).val());

                var Selectric2 = $('select#shipping_custom_address').data('selectric');
                Selectric2.destroy();
                $('select#shipping_custom_address').select2();

            }
        });
    </script>
	<?php if($has_error): ?>
		<script>
		$(document).ready(function() {
			$('html, body').animate({
                    scrollTop: $('.input-holder.has-error').first().offset().top - 250
                }, 'slow');
		});
		</script>
	<?php endif; ?>
    <?php echo $footer; ?>
