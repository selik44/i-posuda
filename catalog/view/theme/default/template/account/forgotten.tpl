<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class="">
                           <div class="content-holder">
                                <!-- heading -->
                                <div class="heading clearfix">
      <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                           		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
</div>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
      <p><?php echo $text_email; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-forgot">
        <fieldset>
          <div class="form-group required">
           <div class="input-holder">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"> </div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
