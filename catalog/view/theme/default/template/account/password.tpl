<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class="">
                           <div class="content-holder">
                                <!-- heading -->
                                <div class="heading clearfix">
      <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                           		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
</div>
  <div class="row">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_password; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-10">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
