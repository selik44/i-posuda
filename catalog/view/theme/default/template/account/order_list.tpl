<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class=""><?php echo $content_top; ?>
                           <div class="content-holder">
                                <!-- heading -->
                                <div class="heading clearfix">
      <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                           		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
</div>
                <div class="clearfix">
                    <!-- sidebar -->
                    <aside class="sidebar">
                        <nav class="sidebar-nav clearfix">
                            <ul>
                                <li><a href="/my-account/">Личные данные</a></li>
                                <li><a class="active" href="#">История заказов</a></li>
                                <li><a href="/discounts/">Скидки</a></li>
                            </ul>
                        </nav>
                    </aside>
      <?php if ($orders) { ?>
                    <!-- content -->
                    <div class="content history">
                        <div class="content-holder">
                            <h1 class="tablet-show">История заказов</h1>
            <?php foreach ($orders as $order) { ?>
                            <div class="open-close">
                                <div class="history-order-heading clearfix">
                                    <span class="num text-red">№ <?php echo $order['order_id']; ?></span>
                                    <div class="text-left">
                                        <span class="history-date"><?php echo $order['date_added']; ?></span>
                                        <span class="sum">Сумма заказа <span class="text-red"><?php echo $order['total']; ?></span></span>
                                    </div>
                                    <div class="text-right">
                                        <div class="top-row clearfix">
<?php if ($order['status']=="В обработке") { ?>
                                            <button class="button" type="button" onclick="status_change('<?php echo $order['order_id']; ?>',$(this) );">отменить</button>
                                            <span class="text">Заказ находится в обработке и Вы можете его отменить</span>
<?php } ?>
                                        </div>
                                        <div class="bottom-row clearfix">
                                            <span class="status <?php if ($order['status']=="Отменен") { ?>text-red<?php } elseif ($order['status']=="Выполнен") { ?>text-green<?php } else { ?>text-orange<?php } ?>"><?php echo $order['status']; ?></span>
                                            <span class="slide-opener link-right">
                                                <span class="text-show">Показать подробности</span>
                                                <span class="text-hide">Скрыть подробности</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide-block">
                                    <div class="columns-history clearfix">
                                        <ul class="column">
            <?php if ($order['shipping_address']) { ?>
            <li><?php echo $order['shipping_address']; ?></li>
            <?php } ?>

                                        </ul>
                                        <ul class="column">
                                            <li><span class="text-red">Способ доставки:</span> <span class="text"><?php echo $order['shipping_method']; ?></span></li>
                                            <li><span class="text-red">Способ оплаты:</span> <span class="text"><?php echo $order['payment_method']; ?></span></li>
      <?php if ($order['comment']) { ?>
                                            <li><span class="text-red">Комментарий:</span> <span class="text"><?php echo $order['comment']; ?></span></li>
<?php } ?>
                                        </ul>
                                    </div>
                                    <h3 class="text-red">Ваш заказ</h3>
                                    <div class="history-table">
                                        <div class="history-table-heading clearfix mobile-hidden">
                                            <span class="cell visual">Фотография</span>
                                            <span class="cell text-block">Наименование</span>
                                            <span class="cell">Количество</span>
                                            <span class="cell">Цена</span>
                                            <span class="cell">Сумма</span>
                                        </div>
            <?php foreach ($order['products'] as $product ) { ?>
                                        <div class="history-table-row clearfix">
                                            <div class="cell visual"><img width="80" height="81" src="<?php echo $product['thumb']; ?>" alt="image"/></div>
                                            <div class="cell text-block">
                                                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                                <span class="article">Артикул:	<span class="text-red"><?php echo $product['model']; ?></span></span>
                                            </div>
                                            <div class="cell qty-cell">
                                                <span class="mobile-show">Количество</span>
                                                <span class="qty"><?php echo $product['quantity']; ?></span>
                <?php foreach ($product['option'] as $option) { ?>
                                                <span class="text"><?php echo $option['value']; ?>:</span> <span class="text-red"><?php echo $option['name']; ?> шт.</span>
		<?php } ?>
                                            </div>
                                            <div class="cell"><span class="mobile-show">Цена</span> <div class="inner"><strong class="sum"><?php echo $product['price']; ?></strong></div></div>
                                            <div class="cell"><span class="mobile-show">Сумма</span> <div class="inner"><strong class="sum"><?php echo $product['total']; ?></strong></div></div>
                                        </div>
<?php } ?>
                                        <div class="clearfix">
                                            <div class="history-total">
                                                <span class="title">Общая сумма:</span>
                                                <strong class="sum"><?php echo $order['total']; ?></strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            <?php } ?>
<script type="text/javascript">
function status_change(order_id, button) {
		
        $.ajax({ //Process the form using $.ajax()
            type      : 'POST', //Method type
            url       : 'index.php?route=account/order/status',
	    data: '&order_id='+order_id,
	    dataType: 'json',
			success: function(json) {
				if (json['success']) {
					//$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' </div>');
					
					$(button).closest('.top-row').find('.text').text(json['success']);
					$(button).closest('.history-order-heading').find('.status').text('Отменен').attr('class','status text-red');
					$(button).remove();
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
	}); 


}
</script>
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
	</div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
