<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                           <div class="content-holder">
                                <!-- heading -->
                                <div class="heading clearfix">
      <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                           		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
</div>

            <form class="form-login" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <div class="box-login">
										  <?php if ($success) { ?>
  <div class="text-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="text-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
                            <div class="input-holder email">
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>
                            <div class="input-holder password">
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" /></div>

              <input class="button"  type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" /> 
               <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
                        <a class="text-red" href="<?php echo $register; ?>"><?php echo $text_register; ?></a>
                        </div>
            </form>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
</div>
<?php echo $footer; ?>
