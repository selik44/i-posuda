<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class=""><?php echo $content_top; ?>
                           <div class="content-holder">
                                <!-- heading -->
                                <div class="heading clearfix">
      <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                           		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
</div>
                <div class="clearfix">
                    <!-- sidebar -->
                    <aside class="sidebar">
                        <nav class="sidebar-nav clearfix">
                            <ul>
                                <li><a href="<?php echo $account; ?>">Личные данные</a></li>
                                <li><a href="<?php echo $orders; ?>">История заказов</a></li>
                                <li><a class="active" href="#">Скидки</a></li>
                            </ul>
                        </nav>
                    </aside>
                    <!-- content -->
                    <div class="content">
                        <div class="content-holder">
                            <h1 class="tablet-show">Скидки</h1>
                            <h2>Ваша текущая скидка:&nbsp;<span class="text-red"><?php echo $discounts['discount'].'%'; ?></span></h2>
                            <div class="text-discount clearfix">
                                <div class="alignleft">Общая сумма заказов: <strong class="text-red"><?php echo $discounts['total_orders']; ?></strong></div>
                                <div class="alignright">До следующей скидки осталось: <strong class="text-red"><?php echo $discounts['next']; ?></strong></div>
                            </div>
            <?php if (count($discounts['discounts_table'])) { ?>
                            <ul class="list-discount">
                            <?php foreach ($discounts['discounts_table'] as $d) { ?>
                                <li><span class="text-left"><?php echo $text_from; ?>  <span class="text-red"><?php echo $d['summ_str']; ?></span> скидка</span> <strong class="text-red"><?php echo $d['value']; ?> %</strong></li>
                                <?php } ?>
                            </ul>
            <?php } else { ?>
               <?php echo $text_empty; ?>
            <?php } ?>
                        </div>
                    </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
