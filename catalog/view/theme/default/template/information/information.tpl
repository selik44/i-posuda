<?php echo $header; ?>
            <div class="row main"><?php echo $content_top; ?>
                <!-- heading -->
                <div class="heading clearfix tablet-hidden">
      <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
                </div>
                <div class="clearfix">
<?php if ($customer) { ?>

                    <!-- sidebar -->
                    <aside class="sidebar">
                        <nav class="sidebar-nav clearfix">
                            <ul>
<?php foreach ($informations as $information) { ?>
                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
<?php } ?>
                            </ul>
                        </nav>
                    </aside>
<?php } ?>
    <div id="content" class="<?php if ($customer) { ?>content <?php } ?> text-section">
      <?php echo $description; ?><?php echo $content_bottom; ?></div>
</div>
</div>
<?php echo $footer; ?>
