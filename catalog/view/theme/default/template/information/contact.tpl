<?php echo $header; ?>
<div class="row main contact-page"><?php echo $content_top; ?>
	<div class="heading clearfix">
		<h1><?php echo $heading_title; ?></h1>
		<ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
			<?php $i = 1; ?>
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li itemprop="itemListElement" itemscope
					itemtype="http://schema.org/ListItem">
						<?php if ($i < count($breadcrumbs)) { ?>
						<a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
						<?php } ?>
						<span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
						<?php if ($i < count($breadcrumbs)) { ?>
						</a>
					<?php } ?>
					<meta itemprop="position" content="<?php echo $i ?>" />
				</li>
				<?php $i++; ?>
			<?php } ?>
		</ol>
	</div>
	<div class="clearfix">
		<section class="section-contact-icons clearfix">
			<div class="column">
				<h3>Связаться с нами</h3>
				<ul class="phones">
					<?php if(!empty($telephone)): 
					$telephone_parts = explode(' ',$telephone);
						if(count($telephone_parts) > 2): ?>
						<li>
							<a class="ks" href="tel:<?php echo str_replace(Array('(',')',' '),'',$telephone); ?>">
								<span class="text-gray"><?php echo array_shift($telephone_parts); ?></span> 
								<span class="text-red"><?php  echo array_shift($telephone_parts); ?></span> 
								<?php echo implode(' ',$telephone_parts); ?>
							</a>
						</li>
						<?php endif; ?>
					<?php endif; ?>
					<?php if(!empty($telephone_mts)): 
					$telephone_parts = explode(' ',$telephone_mts);
						if(count($telephone_parts) > 2): ?>
						<li>
							<a class="mts" href="tel:<?php echo str_replace(Array('(',')',' '),'',$telephone_mts); ?>">
								<span class="text-gray"><?php echo array_shift($telephone_parts); ?></span> 
								<span class="text-red"><?php  echo array_shift($telephone_parts); ?></span> 
								<?php echo implode(' ',$telephone_parts); ?>
							</a>
						</li>
						<?php endif; ?>
					<?php endif; ?>
				</ul> 
			</div>
			<div class="column">
				<h3>Cоциальные сети</h3>
				<ul class="social">
					<?php if(isset($config_facebook) && !empty($config_facebook)): ?>
						<li><a class="fb" target="_blank" href="<?php echo $config_facebook ?>">Facebook</a></li>
					<?php endif; ?>
					<?php if(isset($config_instagram) && !empty($config_instagram)): ?>
						<li><a class="insta" target="_blank" href="<?php echo $config_instagram ?>">Instagram</a></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="column">
				<h3>Мессенджеры</h3>
				<ul class="messengers">
					<?php if(isset($config_telegram) && !empty($config_telegram)): ?>
						<li class="telegram"><span class="text">Telegram</span> <a href="https://telegram.me/@<?php echo $config_telegram; ?>">@<?php echo $config_telegram; ?></a></li>
					<?php endif; ?>
					<?php if(isset($config_skype) && !empty($config_skype)): ?>
						<li class="skype"><span class="text">Skype</span> <a href="skype:<?php echo $config_skype; ?>?chat"><?php echo $config_skype; ?></a></li>
					<?php endif; ?>
					<?php if(isset($config_viber) && !empty($config_viber)): ?>
							<li class="viber"><span class="text">Viber</span> 
								<a  href="viber://chat?number=<?php echo str_replace(Array('(',')',' '),'',$config_viber); ?>">
									<?php echo $config_viber; ?>
								</a>
							</li>
					<?php endif; ?>
				</ul>
			</div>
		</section> 
		<div class="section-contact-info clearfix">
			<div class="column">
				
				<strong class="title">Общие вопросы</strong>
				<!-- <a class="text-red" href="mailto:<?php echo $config_email; ?>"><?php echo $config_email; ?></a> -->
				<a class="text-red" href="mailto:info@iposuda.com.ua">info@iposuda.com.ua</a>
				<?php if(isset($config_order_email) && !empty($config_order_email)): ?>
					<strong class="title">Заказы обрабатываются через почту</strong>
					<a class="text-red" href="mailto:<?php echo $config_order_email; ?>"><?php echo $config_order_email; ?></a>
				<?php endif; ?>
			</div>
			<div class="column">
				<?php if(isset($config_cooperation_email) && !empty($config_cooperation_email)): ?>
					<strong class="title">Сотрудничество</strong>
					<a class="text-red" href="mailto:<?php echo $config_cooperation_email; ?>"><?php echo $config_cooperation_email; ?></a>
				<?php endif; ?>
				<?php if(isset($config_callback_email) && !empty($config_callback_email)): ?>
					<strong class="title">Мы всегда рады услышать рекомендации по улучшению нашего сервиса или работе сайта, а также жалобы</strong>
					<a class="text-red" href="mailto:<?php echo $config_callback_email; ?>"><?php echo $config_callback_email; ?></a>
				<?php endif; ?>
			</div>
		</div>
		<section class="section-map">
			<h3>Наш адрес</h3>
			<address><?php echo $address; ?></address>
			<div id="map">
				<?php echo $geocode; ?>
			</div>
		</section>
	</div>

</div>
<?php echo $footer; ?>
