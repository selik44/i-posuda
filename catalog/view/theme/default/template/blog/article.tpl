<?php echo $header; ?>
            <div class="row main"><?php echo $content_top; ?>
                <!-- heading -->
                <div class="heading clearfix tablet-hidden">      <h1>Новости</h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<?php $i=1; ?>
    <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
		<?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
		<?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<?php if($i<count($breadcrumbs)) { ?>
			    </a>
		<?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
<?php $i++; ?>
    <?php } ?>
                    </ol>
                </div>
                <div class="clearfix">
                    <!-- sidebar -->
                    <aside class="sidebar">
                        <nav class="sidebar-nav clearfix">
                            <ul>
                                <li><a class="active">Новости</a></li>
                                <li><a href="/articles">Статьи</a></li>
                            </ul>
                        </nav>
                    </aside>
    <div class="content">
                        <div class="content-holder">
                           <!-- news-article -->
                            <article>
                <h2><?php echo $heading_title; ?></h2>
                                <span class="date"><?php echo $date; ?></span>
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                <!-- AddThis Button END -->
				<div class="text-section">
					<?php echo $description; ?>
				</div>
                           </article>
                        </div>
            </div>
        </div>
      <?php echo $content_bottom; ?></div>
</div>
<?php echo $footer; ?>
