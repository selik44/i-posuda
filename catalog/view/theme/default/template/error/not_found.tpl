<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
               <div class="block-404">
                    <div class="picture">
                        <img src="image/giphy.gif" alt="not found"/>
                        <strong class="text">404</strong>
                    </div>
                    <h2>Неправильно набран адрес или страница больше не существует</h2>
                    <a class="button" href="/">На главную</a>
                </div>
      <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
