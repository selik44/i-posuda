// page init
jQuery(function(){
    jQuery('input, textarea').placeholder();
    initSameHeight();
    initOpenClose();
    initPopups();
    initRating();
    jcf.customForms.replaceAll();
    $("[rel='lightbox']").fancybox({
        theme : 'light'
    });
    $(".lightbox-open").fancybox({
        theme : 'light'
    });
    initDropDownClasses();

$('select').selectric({
    onChange: function() {
        $('selectric').addClass('change');
    }
});

    $('.slider-product-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        fade: true,
        asNavFor: '.slider-product-nav'
    });
    $('.slider-product-nav').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-product-for',
        dots: false,
        focusOnSelect: true
    });

    $(window).resize(initSameHeight);
    $('.intro-slider').slick({
		autoplay: true,
		autoplaySpeed: 3500,
        infinite: true,
        slidesToShow: 1,
        dots: true,
        arrows: true,
        speed: 400,
        slidesToScroll: 1
    });

    jQuery('.spinner').spinner({
        create: function (event, ui) {
            if (jQuery(this).spinner("value") == 1) {
                jQuery(this).siblings('.ui-spinner-down').addClass('disabled');
            }
            else{
                jQuery(this).siblings('.ui-spinner-down').removeClass('disabled');
            }
        },
        spin: function (event, ui) {
            if (ui.value <= 1) {
                jQuery(this).spinner("value", 1);
                jQuery(this).siblings('.ui-spinner-down').addClass('disabled');
                return false;
            }
            else{
                $(this).siblings('.ui-spinner-down').removeClass('disabled');
            }

        }
    });
    var mobile_nav = $('.header .mobile-nav');
    function setPopupHeight(){
        mobile_nav.outerHeight($(window).height());
    }
    setPopupHeight();
    $(window).resize(setPopupHeight);

    var mobile_filter = $('.catalog-popup');
    function setFilterHeight(){
        // because mobile layout
        if($(".tablet-show").css("display") == "block"){
            mobile_filter.outerHeight($(window).height() - $('.header').outerHeight() -40);
        }
    }
    setFilterHeight();
    $(window).resize(setFilterHeight);

    var header = $('.header').offset().top;
    $(window).scroll(function(){
        if( $(window).scrollTop() >  0) {
            $('.header').addClass("sticky");
            $('body').addClass("body-sticky");
        } else {
            $('.header').removeClass("sticky");
            $('body').removeClass("body-sticky");
        }
    })

    // tabbed content

    $(".tab_content").hide();
    $(".tab_content:first").show();

    /* if in tab mode */
    $("ul.tabset-product li").click(function() {

        $(".tab_content").hide();
        var activeTab = $(this).attr("rel");
        $("#"+activeTab).fadeIn();

        $("ul.tabset-product li").removeClass("active");
        $(this).addClass("active");

        $(".tab_drawer_heading").removeClass("d_active");
        $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

    });
    /* if in drawer mode */
    $(".tab_drawer_heading").click(function() {

        $(".tab_content").hide();
        var d_activeTab = $(this).attr("rel");
        $("#"+d_activeTab).fadeIn();

        $(".tab_drawer_heading").removeClass("d_active");
        $(this).addClass("d_active");

        $("ul.tabset-product li").removeClass("active");
        $("ul.tabset-product li[rel^='"+d_activeTab+"']").addClass("active");
    });


    /* Extra class "tab_last"
     to add border to right side
     of last tab */
    $('ul.tabs li').last().addClass("tab_last");

    $('.block-advantages .button').click(function( e ) {
        e.preventDefault();
        $('.block-advantages .button').hide();

        $(".block-advantages li[class*='hidden']").removeClass(function (index, classNames) {
            var current_classes = classNames.split(" "),
                classes_to_remove = [];

            $.each(current_classes, function (index, class_name) {
                if (/.*hidden.*/.test(class_name)) {
                    classes_to_remove.push(class_name);
                }
            });
            return classes_to_remove.join(" ");
        });
    });

});


// align blocks height
function initSameHeight() {
    jQuery('ul.list-products').sameHeight({
        elements: 'li',
        multiLine: true
    });
    jQuery('ul.list-testimonials').sameHeight({
        elements: '.text-holder',
        multiLine: true
    });
}

// star rating init
function initRating() {
    lib.each(lib.queryElementsBySelector('ul.star-rating'), function(){
        new StarRating({
            element:this,
            onselect:function(num) {
                // rating setted event
            }
        });
    });
}

// open-close init
function initOpenClose() {
    jQuery('.open-close').openClose({
        activeClass: 'active',
        opener: '.slide-opener',
        slider: '.slide-block',
        animSpeed: 400,
        effect: 'slide'
    });
    jQuery('.open-close-inner').openClose({
        activeClass: 'active',
        opener: '.slide-opener-inner',
        slider: '.slide-block-inner',
        animSpeed: 400,
        effect: 'slide'
    });
}


// popups init
function initPopups() {
    jQuery('.popup-holder').contentPopup({
        mode: 'click',
        popup: '.popup',
        btnOpen: '.opener',
        btnClose: '.popup a.close-link'
    });
    jQuery('.catalog-popup-holder').contentPopup({
        mode: 'click',
        popup: '.catalog-popup',
        btnOpen: '.catalog-open',
        btnClose: '.popup a.close-link'
    });
}
// add classes if item has dropdown
function initDropDownClasses() {
    jQuery('.catalog-block .drop-main li').each(function() {
        var item = jQuery(this);
        var drop = item.find('div');
        var link = item.find('a').eq(0);
        if(drop.length) {
            item.addClass('has-drop-down');
            if(link.length) link.addClass('has-drop-down-a');
        }
    });
}

//mask and change focus for telephone fields
$(document).ready(function(){
	
	$('.buy-click #phone').mask("+38 (999) 999-99-99");
	
	$('.telephone_inputs input#telephone_operator_code').mask("(999)",
	{ 
		completed: function () { $(this).next('input').trigger('focus'); } 
	});
	$('.telephone_inputs input#telephone_main_part').mask("999-99-99");
	$('.telephone_inputs input#telephone_code').on('focus',function(){
		$(this).next('input').trigger('focus');
	});
});
//don't clost popup cart on click in itself
$(document).on("click.bs.dropdown.data-api", ".no-close", function (e) { e.stopPropagation() }); 

//checked wishlist buttons
$(document).on('click','a.link-favorite, a.button-favorite', function(e){
	if(!$(this).hasClass('active'))
	{
		$(this).addClass('active');
		var product_id = $(this).attr('data-id');
		$(this).attr('onclick',"wishlist.remove('"+product_id+"');")
	}
	else
	{
		$(this).removeClass('active');
		var product_id = $(this).attr('data-id');
		$(this).attr('onclick',"wishlist.add('"+product_id+"');")
	}
});

//search form submit
$(document).on('submit','form.search_form',function(){
	var search_field = $(this).find('input[name="search"]').val();
	
	var url = $('base').attr('href') + 'index.php?route=product/search';

	var value = $(this).find('input[name="search"]').val();

	if (value.length > 2) {
		url += '&search=' + encodeURIComponent(value);
		location = url;
	}

	return false;
});

function callFastPopup(popup_content){
	$('.mfp-fast-popup .mfp-fast-popup-content').html(popup_content);
	
	$.magnificPopup.open({
		mainClass: 'mfp-fade',
		items: {
		  src: '.mfp-fast-popup'
		},
		type: 'inline',
		closeBtnInside: true,
		callbacks: {
		open: function() {
				setTimeout(function () {
					$('.mfp-fast-popup .mfp-close').trigger('click');
					$('.mfp-fast-popup .mfp-fast-popup-content').html('');
				}, 3000);
			}
		},
		removalDelay: 300
	}, 0);
}