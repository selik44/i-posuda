function getURLVar(key) {
	var value = [];

	var query = document.location.search.split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

$(document).ready(function() {
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#form-currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#form-currency input[name=\'code\']').val($(this).attr('name'));

		$('#form-currency').submit();
	});

	// Language
	$('#form-language .language-select').on('click', function(e) {
		e.preventDefault();

		$('#form-language input[name=\'code\']').val($(this).attr('name'));

		$('#form-language').submit();
	});

	/* Search */
	$('.js-search  input[name=\'search\']').parent().find('button').on('click', function() {
		var url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('header .js-search  input[name=\'search\']').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('.js-search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header .js-search input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 10) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
		$('#grid-view').removeClass('active');
		$('#list-view').addClass('active');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		var cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		$('#list-view').removeClass('active');
		$('#grid-view').addClass('active');

		localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
		$('#list-view').addClass('active');
	} else {
		$('#grid-view').trigger('click');
		$('#grid-view').addClass('active');
	}

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body',trigger: 'hover'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});
});
var oneclick = {
	'add': function(product_id, phone) {
		$.ajax({
			url: 'index.php?route=checkout/cart/oneclick',
			type: 'post',
			data: 'product_id=' + product_id + '&phone=' + phone,
			dataType: 'json',
			success: function(json) {
			if (json['error']) {
				$('.box-fail').text(json['error']);
				$('.box-fail').animate({opacity:1}, 300);
				$('.box-fail').delay( 1000 ).animate({opacity:0}, 300);
			} else {
				$('.box-success').text('Спасибо, заказ принят');
				$('.box-success').animate({opacity:1}, 300);
				$('.box-success').delay( 1000 ).animate({opacity:0}, 300);
                
                if(json['transaction']){
				    //var dataLayer = dataLayer || [];
				    dataLayer.push(
					{
					    "transactionId": json['transaction']['id'],
					    "transactionAffiliation": json['transaction']['affiliation'],
					    "transactionTotal": json['transaction']['revenue'],
					    "transactionTax": 0,
					    "transactionShipping": 0,
					    "transactionProducts": [{
						"sku": json['transaction']['product']['sku'],
						"name": json['transaction']['product']['name'],
						"category": json['transaction']['product']['category'],
						"price": json['transaction']['product']['price'],
						"quantity": json['transaction']['product']['quantity'],
					    }]
					});
                    
                    dataLayer.push({ "event": "addTransaction" });
                    //console.log(dataLayer);
				}
			}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}
// Cart add remove functions
var cart = {
	'add': function(product_id, quantity, option_name, option) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: option_name+'='+option+'&product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > a').button('loading');
			},
			complete: function() {
				$('#cart > a').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					/*$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					$('#product').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
					
					callFastPopup('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					
					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart > a').html('<span id="cart-total">' + json['quant'] + '</span>');
					}, 100);
					
					//$('html, body').animate({ scrollTop: 0 }, 'slow');
					
					$('#cart > a').addClass('active');
					$('#cart > .dd-menu').load('index.php?route=common/cart/info .dd-menu .dd-menu__content');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'add2': function(product_id) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: $('#product'+product_id+' input[type=\'hidden\'], #product'+product_id+' input[type=\'radio\']:checked'),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > a').button('loading');
			},
			complete: function() {
				$('#cart > a').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					/*$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
					callFastPopup('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart > a').html('<span id="cart-total">' + json['quant'] + '</span>');
					}, 100);

					//$('html, body').animate({ scrollTop: 0 }, 'slow');

					$('#cart > .dd-menu').load('index.php?route=common/cart/info .dd-menu .dd-menu__content');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > a').button('loading');
			},
			complete: function() {
				$('#cart > a').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > a').html('<span id="cart-total">' + json['quant'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > .dd-menu').load('index.php?route=common/cart/info .dd-menu .dd-menu__content');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > a').button('loading');
			},
			complete: function() {
				$('#cart > a').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > a').html('<span id="cart-total">' + json['quant'] + '</span>');
				}, 100);
				$('#cart > a').removeClass('active');
				var now_location = String(document.location.pathname);

				if ((now_location == '/cart/') || (now_location == '/checkout/') || (getURLVar('route') == 'checkout/cart') || (getURLVar('route') == 'checkout/checkout')) {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > .dd-menu').load('index.php?route=common/cart/info .dd-menu .dd-menu__content');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > a').button('loading');
			},
			complete: function() {
				$('#cart > a').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > a').html('<span id="cart-total">' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > .dd-menu').load('index.php?route=common/cart/info .dd-menu .dd-menu__content');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					/*$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
				}
				
				$('#wishlist-total.favorite').html(json['quant']);
				$('#wishlist-total.favorite').attr('title', json['total']);

				//$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function(product_id) {
		
		$.ajax({
			url: 'index.php?route=account/wishlist/remove',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				$('#wishlist-total.favorite').html(json['quant']);
				$('#wishlist-total.favorite').attr('title', json['total']);

			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			}

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			}

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	}

		let GLOBAL_TITLE_STATE = document.title.trim();
    if(GLOBAL_TITLE_STATE.indexOf("Страница") === 0) {
        GLOBAL_TITLE_STATE = GLOBAL_TITLE_STATE.replace(/Страница \d+ - /, "");
    }
//AJAX LOAD content---------------------------------
    function ajaxContent(url, target, scroll_to = '', elem_type ,type = 'GET') {
      const $this = $(this);
      const $pagination = $('ul.pagination');
      const $pagination_loader = $('.lds-spinner');
      const $load_more_arrow = $('.list-products li.next-page-link .box .center .text');
      const $load_more_btn = $('[data-load-more="btn"]');
      if ( $this.data('requestRunning') ) {
        return;
      }
      $this.data('requestRunning', true);
      window.history.pushState('', document.title, url);

      let url2 = new URL(url),
				page_num = url2.searchParams.get("page"),
				old_title = document.title;
      	document.title = page_num == null ? GLOBAL_TITLE_STATE : 'Страница ' + page_num + ' - ' + GLOBAL_TITLE_STATE;

        $.ajax({
            type: type,
            url: url,
            beforeSend: function (data) {
            	if (elem_type === 'next-page') {
                $load_more_arrow.addClass('loading');
                $load_more_btn.addClass('item-disabled');
              } else {
                $pagination.css('opacity', '.4');
                $pagination_loader.css('display', 'block');
							}
            },
            success: function (data) {
                $(target).replaceWith(function () {
                    return $(data).find(`${target}`);
                });
                jcf.customForms.replaceAll();
                $('.link-cart .text-red').each(function () {
                    var default_price_value = $(this).closest('.box').find('.jcf-label-active .price .price-new').html();
                    $(this).html(default_price_value);
                });
                initSpiner();
              if (elem_type === 'next-page') {
                $load_more_arrow.removeClass('loading');
                $load_more_btn.removeClass('item-disabled');
              } else {
                $pagination.css('opacity', '1');
                $pagination_loader.css('display', 'none');
              }
              $('html, body').animate({scrollTop: $(scroll_to).offset().top}, 1000);
              window.dispatchEvent(new Event('resize'));

            },
						complete: function() {
              $this.data('requestRunning', false);
						},
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        });
    }

////

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        ajaxContent($(this).attr('href'), '[data-ajax-content="container"]', '[data-ajax-content="scroll"]');
    });

		// $(document).on('click', '[data-load-next-page]', function (e) {
    //     e.preventDefault();
    //     ajaxContent($(this).attr('data-load-more-href'), '[data-ajax-content="container"]', '[data-ajax-content="scroll"]', 'next-page');
    // });

    $(document).on('click', '[data-load-more="btn"]', function (e) {
        e.preventDefault();
        const $target = $(this);
        const $container = $('[data-load-more="container"]')
				const $load_more_arrow = $('.list-products li.next-page-link .box .center .text');
      	const $load_more_btn = $('[data-load-more="btn"]');

				if ( $target.data('requestRunning') ) {
					return;
				}
      	$target.data('requestRunning', true);
     		var url = $target.attr('data-href');
      	window.history.pushState('', document.title, url);
        $.ajax({
            type: 'GET',
            url: $target.data('load-more-href'),
            beforeSend: function () {
              $load_more_arrow.addClass('loading');
              $load_more_btn.addClass('item-disabled');
            },
            success: function (data) {
              	$load_more_arrow.removeClass('loading');
              	$load_more_btn.removeClass('item-disabled');
                let $new_container = $(data).find('[data-ajax-content="container"]');
                console.log($new_container);
                const $scroll_to = $new_container.find('.list-products li:first');
                $new_container.find('[data-load-more="container"]').prepend($container.find(`.${$target.data('load-more-find')}`));
                $('[data-ajax-content="container"]').replaceWith(function () {
                    return $new_container;
                });
                jcf.customForms.replaceAll();
                $('.link-cart .text-red').each(function () {
                    var default_price_value = $(this).closest('.box').find('.jcf-label-active .price .price-new').html();
                    $(this).html(default_price_value);
                });
                initSpiner();
                window.dispatchEvent(new Event('resize'));
            },
						complete: function() {
              $target.data('requestRunning', false);
						},
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        });
    });

    function initSpiner() {
        jQuery('.spinner').spinner({
            create: function (event, ui) {
                if (jQuery(this).spinner("value") == 1) {
                    jQuery(this).siblings('.ui-spinner-down').addClass('disabled');
                }
                else{
                    jQuery(this).siblings('.ui-spinner-down').removeClass('disabled');
                }
            },
            spin: function (event, ui) {
                if (ui.value <= 1) {
                    jQuery(this).spinner("value", 1);
                    jQuery(this).siblings('.ui-spinner-down').addClass('disabled');
                    return false;
                }
                else{
                    $(this).siblings('.ui-spinner-down').removeClass('disabled');
                }

            }
        });
    }

  function showTooltip() {
    setTimeout(function () {
      $('.tooltip-item-btn').addClass('active');
    }, 500);
    setTimeout(function () {
      $('.tooltip-item-btn').removeClass('active');
    }, 5000);
  };

  $(document).on('click', '#button-account-noreg, #button-account', function () {
    setTimeout(function () {
      $('.tooltip-item-register').addClass('active');
    }, 500);
    setTimeout(function () {
      $('.tooltip-item-register').removeClass('active');
    }, 5000);
  });

  $(document).on('focus', '#input-payment-surname', function () {
    $('.tooltip-item-register').addClass('active');
  });

  $(document).on('blur', '#input-payment-surname', function () {
    $('.tooltip-item-register').removeClass('active');
  });

  showTooltip();

})(window.jQuery);
